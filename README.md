# FCalPulse

Repository for software related to the FCalPulse test beam project.

## Building the code:

The DAQ program we will use is called FCP_DAQ.  To download it to your working area, do the following:

```
kinit <username>@CERN.CH    // then enter CERN password when prompted
git clone https://:@gitlab.cern.ch:8443/evarnes/fcalpulse.git
```

To build it, go to the FCP_DAQ/src directory and type 

```
make
```

## Running the code:

*NOTE*:  The USB driver for DT5730 may need to be re-installed every time there’s a kernel update.  To check, do

```
lsmod | grep CAEN
```

If the driver is there, you should see a line that starts with CAENUSBdrvB.  This means you're all set to run the code.  So go to the FCP_DAQ/src directory and type

```
./beam_signal &
./FCP_DAQ
```

(The first program above emulates the signal we will get when a spill is in progress.  You can run them from separate terminal windows on the same computer, which would avoid the need to put beam_signal into the background.)

If the digitizer is communicating properly with the PC, you’ll see:


```
**************************************************************
                        FCP_DAQ 1.0.0                      
**************************************************************
Connected to CAEN Digitizer Model DT5730B
ROC FPGA Release is 04.16 - Build 1720
AMC FPGA Release is 00.07 - Build 1901

```

With similar information for any other digitizers that are connected to the computer.

A GUI will appear with hopefully intuitive controls for starting/stopping the data acquisition.

## Output data

The date will appear in files names FCP_data_[date/time].dat.  A new file is opened for every run, and also when the file size exceeds a pre-set size limit (currently set to 2GB).

There are two data output formats, a root ntuple and a binary format.  For the root ntuple, information is stored in the tree "Digiziter_event".

The binary stream is formatted as follows:

First a run header is written.  This consists of the version of FCP_DAQ used to write the file, then information about how the digitizers were configured, how much data was recorded for each channel for each trigger, and the numbers of HV supplies and cryostats in the system:


| Start byte    |  Quantity      | Type        | Size     |
| :----------: | -------------- | ----------- | :------: |
| 0             | Data format version  | uint32_t    |  4       |
| 4             | Bytes per channel  | uint32_t    |  4       |
| 8             | No. of events to record | uint32_t | 4    |
| 12            | Samples per event      | uint32_t | 4 |
| 16            | Trigger position in sample window | uint32_t | 4 |
| 20            | External trigger logic (NIM or TTL) | uint32_t  | 4 |
| 24            | Number of digitizers (NDIG) | uint32_t | 4 |
| 28            | Digitizer serial numbers | uint32_t | 4*NDIG |
| 28 + 4*NDIG   | External trigger mode for each digitizer  | uint32_t | 4*NDIG |
| 28 + 8*NDIG   | Channel enable mask for each digitizer | unit32_t | 4*NDIG |
| 28 + 12*NDIG  | DCoffset for each channel | uint32_t | 32*NDIG |
| 28 + 44*NDIG  | Channel trigger mode for each channel | uint32_t | 32*NDIG |
| 28 + 76*NDIG  | Channel trigger polarity for each channel | uint32_t | 32*NDIG |
| 28 + 108*NDIG | Number of general writes to registers (NGW) | uint32_t | 4 | 
| 32 + 108*NDIG | Address of register writes | uint32_t | 4*NGW |
| 32 + 108*NDIG + 4*NGW | Data for register writes |  uint32_t |  4*NGW |
| 32 + 108*NDIG + 8*NGW | Mask for register writes |  uint32_t |  4*NGW |
| 32 + 108*NDIG + 12*NGW   | No. of HV supplies (NHV) | uint32_t  |  4           |
| 36 + 108*NDIG + 12*NGW   | No. of cryostats (NC) | uint32_t | 4            |
| 36 + 108*NDIG + 12*NGW + 4*NC  | Scintillators used (0=small, 1=large) | uint16_t | 2            |

For every spill that was recorded, there is then a spill header:

| Start byte  |  Quantity          | Type      | Size         |
|:----------:|--------------------|-----------|:------------:|
| 0           | Start start time   | time_t    |  8           |
| 8           | Spill number       | uint32_t  |  4           |
| 12          | Digitizer failure bits | uint32_t | 4*NDIG    |
| 12+4*NDIG          | HV values          | float     |  4*NHV |
| 12+4*NDIG+4*NC | Pressure in cryostat | float | 4*NC |
| 12+4*NDIG+4*NHV+4*NC | Temp. in cryostat | float |  4*NC |
| 12+4*NDIG+4*NHV + 8*NC | Beam status flag | uint32_t |  4 |

After this header, all the data recorded in the spill is written.  Each data packet has a header:

| Start byte  |  Quantity          | Type      | Size         |
|:----------:|--------------------|-----------|:------------:|
| 0          | Channel number     | uint32_t  |  4           |
| 4          | 0xa0000000 &#124; Event counter | uint32_t | 4    |
| 8          | Trigger timestamp  | uint32_t  |  4           |

Note that the 0xa in the upper nibble of the event counter is there to allow a data integrity check.

The data payload for the channel follows, consisting of ADC values (uint16_t) for the number of samples the channel was configured to record.

After all of the data recorded for the spill has been written, an "end-of-spill" record is written.  This is a uint32_t set to 0x80000000 (the MSB being set flags the end of spill).

An example ROOT macro that reads in this format is available in analysis/readData.C.

 ## Getting help

If you have trouble with any of the above, please contact [Erich Varnes](mailto:varnes@physics.arizona.edu)