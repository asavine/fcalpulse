//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Aug 13 05:23:08 2022 by ROOT version 6.14/06
// from TTree merged_digitizer_BPC/merged_digitizer_BPC
// found on file: run733.root
//////////////////////////////////////////////////////////

#ifndef merged_digitizer_BPC_h
#define merged_digitizer_BPC_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TDatime.h"
#include "vector"

using namespace std;

class merged_digitizer_BPC {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          dataFormatVersion;
   UInt_t          channelDataSize;
   UInt_t          RecordLength;
   UInt_t          PostTrigger;
   UInt_t          FPIOtype;
   UInt_t          NumDigitizers;
   UInt_t          SerialNumber[4];
   UInt_t          ExtTriggerMode[4];
   UInt_t          EnableMask[4];
   UInt_t          DCoffset[4][8];
   UInt_t          ChannelTriggerMode[4][8];
   UInt_t          PulsePolarity[4][8];
   UInt_t          GWn;
   UInt_t          GWaddr[20];
   UInt_t          GWdata[20];
   UInt_t          GWmask[20];
   UInt_t          nCryostats;
   UShort_t        scintUsed;
   UShort_t        includeTailVeto;
   UShort_t        sourceInBeam;
   UInt_t          runNumber;
   UInt_t          spillNumber;
   TDatime         *spillStartTime;
   UInt_t          BeamStatus;
   UInt_t          digitizerFailBits[4];
   Float_t         SCal_HV_set;
   Float_t         SCal_HV_read;
   Float_t         FCal_inner_HV_set;
   Float_t         FCal_tubeA_HV_read;
   Float_t         FCal_outer_HV_set;
   Float_t         FCal_tubeB_HV_read;
   Float_t         FCal_tubeC_HV_read;
   UInt_t          NumEventsMax;
   UInt_t          NumEventsInSpill;
   UInt_t          nchannels;
   UInt_t          eventCounter;
   UInt_t          triggerTimeTag[4];
   vector<unsigned short> *FCal_rod_front_source_data;
   vector<unsigned short> *FCal_rod_mid_source_data;
   vector<unsigned short> *FCal_rod_rear_source_data;
   vector<unsigned short> *ShCal_front_source_quad_1_data;
   vector<unsigned short> *ShCal_front_source_quad_2_data;
   vector<unsigned short> *ShCal_front_source_quad_3_data;
   vector<unsigned short> *ShCal_front_source_quad_4_data;
   vector<unsigned short> *ShCal_rear_source_quad_1_data;
   vector<unsigned short> *ShCal_rear_source_quad_2_data;
   vector<unsigned short> *ShCal_rear_source_quad_3_data;
   vector<unsigned short> *ShCal_rear_source_quad_4_data;
   vector<unsigned short> *FCal_rod_front_nosource_data;
   vector<unsigned short> *FCal_rod_mid_nosource_data;
   vector<unsigned short> *FCal_rod_rear_nosource_data;
   vector<unsigned short> *ShCal_front_nosource_quad_1_data;
   vector<unsigned short> *ShCal_front_nosource_quad_2_data;
   vector<unsigned short> *ShCal_front_nosource_quad_3_data;
   vector<unsigned short> *ShCal_front_nosource_quad_4_data;
   vector<unsigned short> *ShCal_rear_nosource_quad_1_data;
   vector<unsigned short> *ShCal_rear_nosource_quad_2_data;
   vector<unsigned short> *ShCal_rear_nosource_quad_3_data;
   vector<unsigned short> *ShCal_rear_nosource_quad_4_data;
   vector<unsigned short> *S1_counter_data;
   vector<unsigned short> *S2_counter_data;
   vector<unsigned short> *S3_counter_data;
   vector<unsigned short> *S4_counter_data;
   vector<unsigned short> *Leakage_counter_data;
   vector<unsigned short> *Halo_counter_data;
   vector<unsigned short> *Digitizer_1_trigger_data;
   vector<unsigned short> *Digitizer_2_trigger_data;
   vector<unsigned short> *Digitizer_3_trigger_data;
   vector<unsigned short> *Digitizer_4_trigger_data;
   UShort_t        FCal_rod_front_source_digitizer_SN;
   UShort_t        FCal_rod_front_source_digitizer_handle;
   UShort_t        FCal_rod_front_source_channel;
   Float_t         FCal_rod_front_source_vrange;
   UShort_t        FCal_rod_mid_source_digitizer_SN;
   UShort_t        FCal_rod_mid_source_digitizer_handle;
   UShort_t        FCal_rod_mid_source_channel;
   Float_t         FCal_rod_mid_source_vrange;
   UShort_t        FCal_rod_rear_source_digitizer_SN;
   UShort_t        FCal_rod_rear_source_digitizer_handle;
   UShort_t        FCal_rod_rear_source_channel;
   Float_t         FCal_rod_rear_source_vrange;
   UShort_t        ShCal_front_source_quad_1_digitizer_SN;
   UShort_t        ShCal_front_source_quad_1_digitizer_handle;
   UShort_t        ShCal_front_source_quad_1_channel;
   Float_t         ShCal_front_source_quad_1_vrange;
   UShort_t        ShCal_front_source_quad_2_digitizer_SN;
   UShort_t        ShCal_front_source_quad_2_digitizer_handle;
   UShort_t        ShCal_front_source_quad_2_channel;
   Float_t         ShCal_front_source_quad_2_vrange;
   UShort_t        ShCal_front_source_quad_3_digitizer_SN;
   UShort_t        ShCal_front_source_quad_3_digitizer_handle;
   UShort_t        ShCal_front_source_quad_3_channel;
   Float_t         ShCal_front_source_quad_3_vrange;
   UShort_t        ShCal_front_source_quad_4_digitizer_SN;
   UShort_t        ShCal_front_source_quad_4_digitizer_handle;
   UShort_t        ShCal_front_source_quad_4_channel;
   Float_t         ShCal_front_source_quad_4_vrange;
   UShort_t        ShCal_rear_source_quad_1_digitizer_SN;
   UShort_t        ShCal_rear_source_quad_1_digitizer_handle;
   UShort_t        ShCal_rear_source_quad_1_channel;
   Float_t         ShCal_rear_source_quad_1_vrange;
   UShort_t        ShCal_rear_source_quad_2_digitizer_SN;
   UShort_t        ShCal_rear_source_quad_2_digitizer_handle;
   UShort_t        ShCal_rear_source_quad_2_channel;
   Float_t         ShCal_rear_source_quad_2_vrange;
   UShort_t        ShCal_rear_source_quad_3_digitizer_SN;
   UShort_t        ShCal_rear_source_quad_3_digitizer_handle;
   UShort_t        ShCal_rear_source_quad_3_channel;
   Float_t         ShCal_rear_source_quad_3_vrange;
   UShort_t        ShCal_rear_source_quad_4_digitizer_SN;
   UShort_t        ShCal_rear_source_quad_4_digitizer_handle;
   UShort_t        ShCal_rear_source_quad_4_channel;
   Float_t         ShCal_rear_source_quad_4_vrange;
   UShort_t        FCal_rod_front_nosource_digitizer_SN;
   UShort_t        FCal_rod_front_nosource_digitizer_handle;
   UShort_t        FCal_rod_front_nosource_channel;
   Float_t         FCal_rod_front_nosource_vrange;
   UShort_t        FCal_rod_mid_nosource_digitizer_SN;
   UShort_t        FCal_rod_mid_nosource_digitizer_handle;
   UShort_t        FCal_rod_mid_nosource_channel;
   Float_t         FCal_rod_mid_nosource_vrange;
   UShort_t        FCal_rod_rear_nosource_digitizer_SN;
   UShort_t        FCal_rod_rear_nosource_digitizer_handle;
   UShort_t        FCal_rod_rear_nosource_channel;
   Float_t         FCal_rod_rear_nosource_vrange;
   UShort_t        ShCal_front_nosource_quad_1_digitizer_SN;
   UShort_t        ShCal_front_nosource_quad_1_digitizer_handle;
   UShort_t        ShCal_front_nosource_quad_1_channel;
   Float_t         ShCal_front_nosource_quad_1_vrange;
   UShort_t        ShCal_front_nosource_quad_2_digitizer_SN;
   UShort_t        ShCal_front_nosource_quad_2_digitizer_handle;
   UShort_t        ShCal_front_nosource_quad_2_channel;
   Float_t         ShCal_front_nosource_quad_2_vrange;
   UShort_t        ShCal_front_nosource_quad_3_digitizer_SN;
   UShort_t        ShCal_front_nosource_quad_3_digitizer_handle;
   UShort_t        ShCal_front_nosource_quad_3_channel;
   Float_t         ShCal_front_nosource_quad_3_vrange;
   UShort_t        ShCal_front_nosource_quad_4_digitizer_SN;
   UShort_t        ShCal_front_nosource_quad_4_digitizer_handle;
   UShort_t        ShCal_front_nosource_quad_4_channel;
   Float_t         ShCal_front_nosource_quad_4_vrange;
   UShort_t        ShCal_rear_nosource_quad_1_digitizer_SN;
   UShort_t        ShCal_rear_nosource_quad_1_digitizer_handle;
   UShort_t        ShCal_rear_nosource_quad_1_channel;
   Float_t         ShCal_rear_nosource_quad_1_vrange;
   UShort_t        ShCal_rear_nosource_quad_2_digitizer_SN;
   UShort_t        ShCal_rear_nosource_quad_2_digitizer_handle;
   UShort_t        ShCal_rear_nosource_quad_2_channel;
   Float_t         ShCal_rear_nosource_quad_2_vrange;
   UShort_t        ShCal_rear_nosource_quad_3_digitizer_SN;
   UShort_t        ShCal_rear_nosource_quad_3_digitizer_handle;
   UShort_t        ShCal_rear_nosource_quad_3_channel;
   Float_t         ShCal_rear_nosource_quad_3_vrange;
   UShort_t        ShCal_rear_nosource_quad_4_digitizer_SN;
   UShort_t        ShCal_rear_nosource_quad_4_digitizer_handle;
   UShort_t        ShCal_rear_nosource_quad_4_channel;
   Float_t         ShCal_rear_nosource_quad_4_vrange;
   UShort_t        S1_counter_digitizer_SN;
   UShort_t        S1_counter_digitizer_handle;
   UShort_t        S1_counter_channel;
   Float_t         S1_counter_vrange;
   UShort_t        S2_counter_digitizer_SN;
   UShort_t        S2_counter_digitizer_handle;
   UShort_t        S2_counter_channel;
   Float_t         S2_counter_vrange;
   UShort_t        S3_counter_digitizer_SN;
   UShort_t        S3_counter_digitizer_handle;
   UShort_t        S3_counter_channel;
   Float_t         S3_counter_vrange;
   UShort_t        S4_counter_digitizer_SN;
   UShort_t        Leakage_counter_digitizer_handle;
   UShort_t        S4_counter_channel;
   Float_t         S4_counter_vrange;
   UShort_t        Leakage_counter_digitizer_SN;
   UShort_t        Leakage_counter_channel;
   Float_t         Leakage_counter_vrange;
   UShort_t        Halo_counter_digitizer_SN;
   UShort_t        Halo_counter_digitizer_handle;
   UShort_t        Halo_counter_channel;
   Float_t         Halo_counter_vrange;
   UShort_t        Digitizer_1_trigger_digitizer_SN;
   UShort_t        Digitizer_1_trigger_digitizer_handle;
   UShort_t        Digitizer_1_trigger_channel;
   Float_t         Digitizer_1_trigger_vrange;
   UShort_t        Digitizer_2_trigger_digitizer_SN;
   UShort_t        Digitizer_2_trigger_digitizer_handle;
   UShort_t        Digitizer_2_trigger_channel;
   Float_t         Digitizer_2_trigger_vrange;
   UShort_t        Digitizer_3_trigger_digitizer_SN;
   UShort_t        Digitizer_3_trigger_digitizer_handle;
   UShort_t        Digitizer_3_trigger_channel;
   Float_t         Digitizer_3_trigger_vrange;
   UShort_t        Digitizer_4_trigger_digitizer_SN;
   UShort_t        Digitizer_4_trigger_digitizer_handle;
   UShort_t        Digitizer_4_trigger_channel;
   Float_t         Digitizer_4_trigger_vrange;
   Bool_t          good_BPC_data;
   UShort_t        BPC_tag;
   UShort_t        BPC_runNumber;
   UShort_t        BPC_spillNumber;
   UShort_t        BPC_triggerNumber;
   UInt_t          BPC_accum_time;
   UInt_t          BPC_wall_time;
   Float_t         BPC_x0;
   Float_t         BPC_y0;
   Float_t         BPC_x1;
   Float_t         BPC_y1;
   Float_t         BPC_x2;
   Float_t         BPC_y2;
   Float_t         BPC_x3;
   Float_t         BPC_y3;

   // List of branches
   TBranch        *b_dataFormatVersion;   //!
   TBranch        *b_channelDataSize;   //!
   TBranch        *b_RecordLength;   //!
   TBranch        *b_PostTrigger;   //!
   TBranch        *b_FPIOtype;   //!
   TBranch        *b_NumDigitizers;   //!
   TBranch        *b_SerialNumber;   //!
   TBranch        *b_ExtTriggerMode;   //!
   TBranch        *b_EnableMask;   //!
   TBranch        *b_DCoffset;   //!
   TBranch        *b_ChannelTriggerMode;   //!
   TBranch        *b_PulsePolarity;   //!
   TBranch        *b_GWn;   //!
   TBranch        *b_GWaddr;   //!
   TBranch        *b_GWdata;   //!
   TBranch        *b_GWmask;   //!
   TBranch        *b_nCryostats;   //!
   TBranch        *b_scintUsed;   //!
   TBranch        *b_includeTailVeto;   //!
   TBranch        *b_sourceInBeam;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_spillNumber;   //!
   TBranch        *b_spillStartTime;   //!
   TBranch        *b_BeamStatus;   //!
   TBranch        *b_boardFailBits;   //!
   TBranch        *b_SCal_HV_set;   //!
   TBranch        *b_SCal_HV_read;   //!
   TBranch        *b_FCal_inner_HV_set;   //!
   TBranch        *b_FCal_tubeA_HV_read;   //!
   TBranch        *b_FCal_outer_HV_set;   //!
   TBranch        *b_FCal_tubeB_HV_read;   //!
   TBranch        *b_FCal_tubeC_HV_read;   //!
   TBranch        *b_NumEventsMax;   //!
   TBranch        *b_NumEventsInSpill;   //!
   TBranch        *b_nchannels;   //!
   TBranch        *b_eventCounter;   //!
   TBranch        *b_triggerTimeTag;   //!
   TBranch        *b_FCal_rod_front_source_data;   //!
   TBranch        *b_FCal_rod_mid_source_data;   //!
   TBranch        *b_FCal_rod_rear_source_data;   //!
   TBranch        *b_ShCal_front_source_quad_1_data;   //!
   TBranch        *b_ShCal_front_source_quad_2_data;   //!
   TBranch        *b_ShCal_front_source_quad_3_data;   //!
   TBranch        *b_ShCal_front_source_quad_4_data;   //!
   TBranch        *b_ShCal_rear_source_quad_1_data;   //!
   TBranch        *b_ShCal_rear_source_quad_2_data;   //!
   TBranch        *b_ShCal_rear_source_quad_3_data;   //!
   TBranch        *b_ShCal_rear_source_quad_4_data;   //!
   TBranch        *b_FCal_rod_front_nosource_data;   //!
   TBranch        *b_FCal_rod_mid_nosource_data;   //!
   TBranch        *b_FCal_rod_rear_nosource_data;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_data;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_data;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_data;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_data;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_data;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_data;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_data;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_data;   //!
   TBranch        *b_S1_counter_data;   //!
   TBranch        *b_S2_counter_data;   //!
   TBranch        *b_S3_counter_data;   //!
   TBranch        *b_S4_counter_data;   //!
   TBranch        *b_Leakage_counter_data;   //!
   TBranch        *b_Halo_counter_data;   //!
   TBranch        *b_Digitizer_1_trigger_data;   //!
   TBranch        *b_Digitizer_2_trigger_data;   //!
   TBranch        *b_Digitizer_3_trigger_data;   //!
   TBranch        *b_Digitizer_4_trigger_data;   //!
   TBranch        *b_FCal_rod_front_source_digitizer_SN;   //!
   TBranch        *b_FCal_rod_front_source_digitizer_handle;   //!
   TBranch        *b_FCal_rod_front_source_channel;   //!
   TBranch        *b_FCal_rod_front_source_vrange;   //!
   TBranch        *b_FCal_rod_mid_source_digitizer_SN;   //!
   TBranch        *b_FCal_rod_mid_source_digitizer_handle;   //!
   TBranch        *b_FCal_rod_mid_source_channel;   //!
   TBranch        *b_FCal_rod_mid_source_vrange;   //!
   TBranch        *b_FCal_rod_rear_source_digitizer_SN;   //!
   TBranch        *b_FCal_rod_rear_source_digitizer_handle;   //!
   TBranch        *b_FCal_rod_rear_source_channel;   //!
   TBranch        *b_FCal_rod_rear_source_vrange;   //!
   TBranch        *b_ShCal_front_source_quad_1_digitizer_SN;   //!
   TBranch        *b_ShCal_front_source_quad_1_digitizer_handle;   //!
   TBranch        *b_ShCal_front_source_quad_1_channel;   //!
   TBranch        *b_ShCal_front_source_quad_1_vrange;   //!
   TBranch        *b_ShCal_front_source_quad_2_digitizer_SN;   //!
   TBranch        *b_ShCal_front_source_quad_2_digitizer_handle;   //!
   TBranch        *b_ShCal_front_source_quad_2_channel;   //!
   TBranch        *b_ShCal_front_source_quad_2_vrange;   //!
   TBranch        *b_ShCal_front_source_quad_3_digitizer_SN;   //!
   TBranch        *b_ShCal_front_source_quad_3_digitizer_handle;   //!
   TBranch        *b_ShCal_front_source_quad_3_channel;   //!
   TBranch        *b_ShCal_front_source_quad_3_vrange;   //!
   TBranch        *b_ShCal_front_source_quad_4_digitizer_SN;   //!
   TBranch        *b_ShCal_front_source_quad_4_digitizer_handle;   //!
   TBranch        *b_ShCal_front_source_quad_4_channel;   //!
   TBranch        *b_ShCal_front_source_quad_4_vrange;   //!
   TBranch        *b_ShCal_rear_source_quad_1_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_source_quad_1_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_source_quad_1_channel;   //!
   TBranch        *b_ShCal_rear_source_quad_1_vrange;   //!
   TBranch        *b_ShCal_rear_source_quad_2_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_source_quad_2_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_source_quad_2_channel;   //!
   TBranch        *b_ShCal_rear_source_quad_2_vrange;   //!
   TBranch        *b_ShCal_rear_source_quad_3_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_source_quad_3_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_source_quad_3_channel;   //!
   TBranch        *b_ShCal_rear_source_quad_3_vrange;   //!
   TBranch        *b_ShCal_rear_source_quad_4_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_source_quad_4_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_source_quad_4_channel;   //!
   TBranch        *b_ShCal_rear_source_quad_4_vrange;   //!
   TBranch        *b_FCal_rod_front_nosource_digitizer_SN;   //!
   TBranch        *b_FCal_rod_front_nosource_digitizer_handle;   //!
   TBranch        *b_FCal_rod_front_nosource_channel;   //!
   TBranch        *b_FCal_rod_front_nosource_vrange;   //!
   TBranch        *b_FCal_rod_mid_nosource_digitizer_SN;   //!
   TBranch        *b_FCal_rod_mid_nosource_digitizer_handle;   //!
   TBranch        *b_FCal_rod_mid_nosource_channel;   //!
   TBranch        *b_FCal_rod_mid_nosource_vrange;   //!
   TBranch        *b_FCal_rod_rear_nosource_digitizer_SN;   //!
   TBranch        *b_FCal_rod_rear_nosource_digitizer_handle;   //!
   TBranch        *b_FCal_rod_rear_nosource_channel;   //!
   TBranch        *b_FCal_rod_rear_nosource_vrange;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_digitizer_SN;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_digitizer_handle;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_channel;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_vrange;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_digitizer_SN;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_digitizer_handle;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_channel;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_vrange;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_digitizer_SN;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_digitizer_handle;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_channel;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_vrange;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_digitizer_SN;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_digitizer_handle;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_channel;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_vrange;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_channel;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_vrange;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_channel;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_vrange;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_channel;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_vrange;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_channel;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_vrange;   //!
   TBranch        *b_S1_counter_digitizer_SN;   //!
   TBranch        *b_S1_counter_digitizer_handle;   //!
   TBranch        *b_S1_counter_channel;   //!
   TBranch        *b_S1_counter_vrange;   //!
   TBranch        *b_S2_counter_digitizer_SN;   //!
   TBranch        *b_S2_counter_digitizer_handle;   //!
   TBranch        *b_S2_counter_channel;   //!
   TBranch        *b_S2_counter_vrange;   //!
   TBranch        *b_S3_counter_digitizer_SN;   //!
   TBranch        *b_S3_counter_digitizer_handle;   //!
   TBranch        *b_S3_counter_channel;   //!
   TBranch        *b_S3_counter_vrange;   //!
   TBranch        *b_S4_counter_digitizer_SN;   //!
   TBranch        *b_Leakage_counter_digitizer_handle;   //!
   TBranch        *b_S4_counter_channel;   //!
   TBranch        *b_S4_counter_vrange;   //!
   TBranch        *b_Leakage_counter_digitizer_SN;   //!
   TBranch        *b_Leakage_counter_channel;   //!
   TBranch        *b_Leakage_counter_vrange;   //!
   TBranch        *b_Halo_counter_digitizer_SN;   //!
   TBranch        *b_Halo_counter_digitizer_handle;   //!
   TBranch        *b_Halo_counter_channel;   //!
   TBranch        *b_Halo_counter_vrange;   //!
   TBranch        *b_Digitizer_1_trigger_digitizer_SN;   //!
   TBranch        *b_Digitizer_1_trigger_digitizer_handle;   //!
   TBranch        *b_Digitizer_1_trigger_channel;   //!
   TBranch        *b_Digitizer_1_trigger_vrange;   //!
   TBranch        *b_Digitizer_2_trigger_digitizer_SN;   //!
   TBranch        *b_Digitizer_2_trigger_digitizer_handle;   //!
   TBranch        *b_Digitizer_2_trigger_channel;   //!
   TBranch        *b_Digitizer_2_trigger_vrange;   //!
   TBranch        *b_Digitizer_3_trigger_digitizer_SN;   //!
   TBranch        *b_Digitizer_3_trigger_digitizer_handle;   //!
   TBranch        *b_Digitizer_3_trigger_channel;   //!
   TBranch        *b_Digitizer_3_trigger_vrange;   //!
   TBranch        *b_Digitizer_4_trigger_digitizer_SN;   //!
   TBranch        *b_Digitizer_4_trigger_digitizer_handle;   //!
   TBranch        *b_Digitizer_4_trigger_channel;   //!
   TBranch        *b_Digitizer_4_trigger_vrange;   //!
   TBranch        *b_good_BPC_data;   //!
   TBranch        *b_BPC_tag;   //!
   TBranch        *b_BPC_runNumber;   //!
   TBranch        *b_BPC_spillNumber;   //!
   TBranch        *b_BPC_triggerNumber;   //!
   TBranch        *b_BPC_accum_time;   //!
   TBranch        *b_BPC_wall_time;   //!
   TBranch        *b_BPC_x0;   //!
   TBranch        *b_BPC_y0;   //!
   TBranch        *b_BPC_x1;   //!
   TBranch        *b_BPC_y1;   //!
   TBranch        *b_BPC_x2;   //!
   TBranch        *b_BPC_y2;   //!
   TBranch        *b_BPC_x3;   //!
   TBranch        *b_BPC_y3;   //!

   merged_digitizer_BPC(TTree *tree=0);
   virtual ~merged_digitizer_BPC();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(Bool_t Sr90in);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef merged_digitizer_BPC_cxx
merged_digitizer_BPC::merged_digitizer_BPC(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("run1025.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("run1025.root");
      }
      f->GetObject("merged_digitizer_BPC",tree);

   }
   Init(tree);
}

merged_digitizer_BPC::~merged_digitizer_BPC()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t merged_digitizer_BPC::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t merged_digitizer_BPC::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void merged_digitizer_BPC::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   spillStartTime = 0;
   FCal_rod_front_source_data = 0;
   FCal_rod_mid_source_data = 0;
   FCal_rod_rear_source_data = 0;
   ShCal_front_source_quad_1_data = 0;
   ShCal_front_source_quad_2_data = 0;
   ShCal_front_source_quad_3_data = 0;
   ShCal_front_source_quad_4_data = 0;
   ShCal_rear_source_quad_1_data = 0;
   ShCal_rear_source_quad_2_data = 0;
   ShCal_rear_source_quad_3_data = 0;
   ShCal_rear_source_quad_4_data = 0;
   FCal_rod_front_nosource_data = 0;
   FCal_rod_mid_nosource_data = 0;
   FCal_rod_rear_nosource_data = 0;
   ShCal_front_nosource_quad_1_data = 0;
   ShCal_front_nosource_quad_2_data = 0;
   ShCal_front_nosource_quad_3_data = 0;
   ShCal_front_nosource_quad_4_data = 0;
   ShCal_rear_nosource_quad_1_data = 0;
   ShCal_rear_nosource_quad_2_data = 0;
   ShCal_rear_nosource_quad_3_data = 0;
   ShCal_rear_nosource_quad_4_data = 0;
   S1_counter_data = 0;
   S2_counter_data = 0;
   S3_counter_data = 0;
   S4_counter_data = 0;
   Leakage_counter_data = 0;
   Halo_counter_data = 0;
   Digitizer_1_trigger_data = 0;
   Digitizer_2_trigger_data = 0;
   Digitizer_3_trigger_data = 0;
   Digitizer_4_trigger_data = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("dataFormatVersion", &dataFormatVersion, &b_dataFormatVersion);
   fChain->SetBranchAddress("channelDataSize", &channelDataSize, &b_channelDataSize);
   fChain->SetBranchAddress("RecordLength", &RecordLength, &b_RecordLength);
   fChain->SetBranchAddress("PostTrigger", &PostTrigger, &b_PostTrigger);
   fChain->SetBranchAddress("FPIOtype", &FPIOtype, &b_FPIOtype);
   fChain->SetBranchAddress("NumDigitizers", &NumDigitizers, &b_NumDigitizers);
   fChain->SetBranchAddress("SerialNumber", SerialNumber, &b_SerialNumber);
   fChain->SetBranchAddress("ExtTriggerMode", ExtTriggerMode, &b_ExtTriggerMode);
   fChain->SetBranchAddress("EnableMask", EnableMask, &b_EnableMask);
   fChain->SetBranchAddress("DCoffset", DCoffset, &b_DCoffset);
   fChain->SetBranchAddress("ChannelTriggerMode", ChannelTriggerMode, &b_ChannelTriggerMode);
   fChain->SetBranchAddress("PulsePolarity", PulsePolarity, &b_PulsePolarity);
   fChain->SetBranchAddress("GWn", &GWn, &b_GWn);
   fChain->SetBranchAddress("GWaddr", GWaddr, &b_GWaddr);
   fChain->SetBranchAddress("GWdata", GWdata, &b_GWdata);
   fChain->SetBranchAddress("GWmask", GWmask, &b_GWmask);
   fChain->SetBranchAddress("nCryostats", &nCryostats, &b_nCryostats);
   fChain->SetBranchAddress("scintUsed", &scintUsed, &b_scintUsed);
   fChain->SetBranchAddress("includeTailVeto", &includeTailVeto, &b_includeTailVeto);
   fChain->SetBranchAddress("sourceInBeam", &sourceInBeam, &b_sourceInBeam);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("spillNumber", &spillNumber, &b_spillNumber);
   fChain->SetBranchAddress("spillStartTime", &spillStartTime, &b_spillStartTime);
   fChain->SetBranchAddress("BeamStatus", &BeamStatus, &b_BeamStatus);
   fChain->SetBranchAddress("digitizerFailBits", digitizerFailBits, &b_boardFailBits);
   fChain->SetBranchAddress("SCal_HV_set", &SCal_HV_set, &b_SCal_HV_set);
   fChain->SetBranchAddress("SCal_HV_read", &SCal_HV_read, &b_SCal_HV_read);
   fChain->SetBranchAddress("FCal_inner_HV_set", &FCal_inner_HV_set, &b_FCal_inner_HV_set);
   fChain->SetBranchAddress("FCal_tubeA_HV_read", &FCal_tubeA_HV_read, &b_FCal_tubeA_HV_read);
   fChain->SetBranchAddress("FCal_outer_HV_set", &FCal_outer_HV_set, &b_FCal_outer_HV_set);
   fChain->SetBranchAddress("FCal_tubeB_HV_read", &FCal_tubeB_HV_read, &b_FCal_tubeB_HV_read);
   fChain->SetBranchAddress("FCal_tubeC_HV_read", &FCal_tubeC_HV_read, &b_FCal_tubeC_HV_read);
   fChain->SetBranchAddress("NumEventsMax", &NumEventsMax, &b_NumEventsMax);
   fChain->SetBranchAddress("NumEventsInSpill", &NumEventsInSpill, &b_NumEventsInSpill);
   fChain->SetBranchAddress("nchannels", &nchannels, &b_nchannels);
   fChain->SetBranchAddress("eventCounter", &eventCounter, &b_eventCounter);
   fChain->SetBranchAddress("triggerTimeTag", triggerTimeTag, &b_triggerTimeTag);
   fChain->SetBranchAddress("FCal_rod_front_source_data", &FCal_rod_front_source_data, &b_FCal_rod_front_source_data);
   fChain->SetBranchAddress("FCal_rod_mid_source_data", &FCal_rod_mid_source_data, &b_FCal_rod_mid_source_data);
   fChain->SetBranchAddress("FCal_rod_rear_source_data", &FCal_rod_rear_source_data, &b_FCal_rod_rear_source_data);
   fChain->SetBranchAddress("ShCal_front_source_quad_1_data", &ShCal_front_source_quad_1_data, &b_ShCal_front_source_quad_1_data);
   fChain->SetBranchAddress("ShCal_front_source_quad_2_data", &ShCal_front_source_quad_2_data, &b_ShCal_front_source_quad_2_data);
   fChain->SetBranchAddress("ShCal_front_source_quad_3_data", &ShCal_front_source_quad_3_data, &b_ShCal_front_source_quad_3_data);
   fChain->SetBranchAddress("ShCal_front_source_quad_4_data", &ShCal_front_source_quad_4_data, &b_ShCal_front_source_quad_4_data);
   fChain->SetBranchAddress("ShCal_rear_source_quad_1_data", &ShCal_rear_source_quad_1_data, &b_ShCal_rear_source_quad_1_data);
   fChain->SetBranchAddress("ShCal_rear_source_quad_2_data", &ShCal_rear_source_quad_2_data, &b_ShCal_rear_source_quad_2_data);
   fChain->SetBranchAddress("ShCal_rear_source_quad_3_data", &ShCal_rear_source_quad_3_data, &b_ShCal_rear_source_quad_3_data);
   fChain->SetBranchAddress("ShCal_rear_source_quad_4_data", &ShCal_rear_source_quad_4_data, &b_ShCal_rear_source_quad_4_data);
   fChain->SetBranchAddress("FCal_rod_front_nosource_data", &FCal_rod_front_nosource_data, &b_FCal_rod_front_nosource_data);
   fChain->SetBranchAddress("FCal_rod_mid_nosource_data", &FCal_rod_mid_nosource_data, &b_FCal_rod_mid_nosource_data);
   fChain->SetBranchAddress("FCal_rod_rear_nosource_data", &FCal_rod_rear_nosource_data, &b_FCal_rod_rear_nosource_data);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_1_data", &ShCal_front_nosource_quad_1_data, &b_ShCal_front_nosource_quad_1_data);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_2_data", &ShCal_front_nosource_quad_2_data, &b_ShCal_front_nosource_quad_2_data);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_3_data", &ShCal_front_nosource_quad_3_data, &b_ShCal_front_nosource_quad_3_data);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_4_data", &ShCal_front_nosource_quad_4_data, &b_ShCal_front_nosource_quad_4_data);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_1_data", &ShCal_rear_nosource_quad_1_data, &b_ShCal_rear_nosource_quad_1_data);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_2_data", &ShCal_rear_nosource_quad_2_data, &b_ShCal_rear_nosource_quad_2_data);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_3_data", &ShCal_rear_nosource_quad_3_data, &b_ShCal_rear_nosource_quad_3_data);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_4_data", &ShCal_rear_nosource_quad_4_data, &b_ShCal_rear_nosource_quad_4_data);
   fChain->SetBranchAddress("S1_counter_data", &S1_counter_data, &b_S1_counter_data);
   fChain->SetBranchAddress("S2_counter_data", &S2_counter_data, &b_S2_counter_data);
   fChain->SetBranchAddress("S3_counter_data", &S3_counter_data, &b_S3_counter_data);
   fChain->SetBranchAddress("S4_counter_data", &S4_counter_data, &b_S4_counter_data);
   fChain->SetBranchAddress("Leakage_counter_data", &Leakage_counter_data, &b_Leakage_counter_data);
   fChain->SetBranchAddress("Halo_counter_data", &Halo_counter_data, &b_Halo_counter_data);
   fChain->SetBranchAddress("Digitizer_1_trigger_data", &Digitizer_1_trigger_data, &b_Digitizer_1_trigger_data);
   fChain->SetBranchAddress("Digitizer_2_trigger_data", &Digitizer_2_trigger_data, &b_Digitizer_2_trigger_data);
   fChain->SetBranchAddress("Digitizer_3_trigger_data", &Digitizer_3_trigger_data, &b_Digitizer_3_trigger_data);
   fChain->SetBranchAddress("Digitizer_4_trigger_data", &Digitizer_4_trigger_data, &b_Digitizer_4_trigger_data);
   fChain->SetBranchAddress("FCal_rod_front_source_digitizer_SN", &FCal_rod_front_source_digitizer_SN, &b_FCal_rod_front_source_digitizer_SN);
   fChain->SetBranchAddress("FCal_rod_front_source_digitizer_handle", &FCal_rod_front_source_digitizer_handle, &b_FCal_rod_front_source_digitizer_handle);
   fChain->SetBranchAddress("FCal_rod_front_source_channel", &FCal_rod_front_source_channel, &b_FCal_rod_front_source_channel);
   fChain->SetBranchAddress("FCal_rod_front_source_vrange", &FCal_rod_front_source_vrange, &b_FCal_rod_front_source_vrange);
   fChain->SetBranchAddress("FCal_rod_mid_source_digitizer_SN", &FCal_rod_mid_source_digitizer_SN, &b_FCal_rod_mid_source_digitizer_SN);
   fChain->SetBranchAddress("FCal_rod_mid_source_digitizer_handle", &FCal_rod_mid_source_digitizer_handle, &b_FCal_rod_mid_source_digitizer_handle);
   fChain->SetBranchAddress("FCal_rod_mid_source_channel", &FCal_rod_mid_source_channel, &b_FCal_rod_mid_source_channel);
   fChain->SetBranchAddress("FCal_rod_mid_source_vrange", &FCal_rod_mid_source_vrange, &b_FCal_rod_mid_source_vrange);
   fChain->SetBranchAddress("FCal_rod_rear_source_digitizer_SN", &FCal_rod_rear_source_digitizer_SN, &b_FCal_rod_rear_source_digitizer_SN);
   fChain->SetBranchAddress("FCal_rod_rear_source_digitizer_handle", &FCal_rod_rear_source_digitizer_handle, &b_FCal_rod_rear_source_digitizer_handle);
   fChain->SetBranchAddress("FCal_rod_rear_source_channel", &FCal_rod_rear_source_channel, &b_FCal_rod_rear_source_channel);
   fChain->SetBranchAddress("FCal_rod_rear_source_vrange", &FCal_rod_rear_source_vrange, &b_FCal_rod_rear_source_vrange);
   fChain->SetBranchAddress("ShCal_front_source_quad_1_digitizer_SN", &ShCal_front_source_quad_1_digitizer_SN, &b_ShCal_front_source_quad_1_digitizer_SN);
   fChain->SetBranchAddress("ShCal_front_source_quad_1_digitizer_handle", &ShCal_front_source_quad_1_digitizer_handle, &b_ShCal_front_source_quad_1_digitizer_handle);
   fChain->SetBranchAddress("ShCal_front_source_quad_1_channel", &ShCal_front_source_quad_1_channel, &b_ShCal_front_source_quad_1_channel);
   fChain->SetBranchAddress("ShCal_front_source_quad_1_vrange", &ShCal_front_source_quad_1_vrange, &b_ShCal_front_source_quad_1_vrange);
   fChain->SetBranchAddress("ShCal_front_source_quad_2_digitizer_SN", &ShCal_front_source_quad_2_digitizer_SN, &b_ShCal_front_source_quad_2_digitizer_SN);
   fChain->SetBranchAddress("ShCal_front_source_quad_2_digitizer_handle", &ShCal_front_source_quad_2_digitizer_handle, &b_ShCal_front_source_quad_2_digitizer_handle);
   fChain->SetBranchAddress("ShCal_front_source_quad_2_channel", &ShCal_front_source_quad_2_channel, &b_ShCal_front_source_quad_2_channel);
   fChain->SetBranchAddress("ShCal_front_source_quad_2_vrange", &ShCal_front_source_quad_2_vrange, &b_ShCal_front_source_quad_2_vrange);
   fChain->SetBranchAddress("ShCal_front_source_quad_3_digitizer_SN", &ShCal_front_source_quad_3_digitizer_SN, &b_ShCal_front_source_quad_3_digitizer_SN);
   fChain->SetBranchAddress("ShCal_front_source_quad_3_digitizer_handle", &ShCal_front_source_quad_3_digitizer_handle, &b_ShCal_front_source_quad_3_digitizer_handle);
   fChain->SetBranchAddress("ShCal_front_source_quad_3_channel", &ShCal_front_source_quad_3_channel, &b_ShCal_front_source_quad_3_channel);
   fChain->SetBranchAddress("ShCal_front_source_quad_3_vrange", &ShCal_front_source_quad_3_vrange, &b_ShCal_front_source_quad_3_vrange);
   fChain->SetBranchAddress("ShCal_front_source_quad_4_digitizer_SN", &ShCal_front_source_quad_4_digitizer_SN, &b_ShCal_front_source_quad_4_digitizer_SN);
   fChain->SetBranchAddress("ShCal_front_source_quad_4_digitizer_handle", &ShCal_front_source_quad_4_digitizer_handle, &b_ShCal_front_source_quad_4_digitizer_handle);
   fChain->SetBranchAddress("ShCal_front_source_quad_4_channel", &ShCal_front_source_quad_4_channel, &b_ShCal_front_source_quad_4_channel);
   fChain->SetBranchAddress("ShCal_front_source_quad_4_vrange", &ShCal_front_source_quad_4_vrange, &b_ShCal_front_source_quad_4_vrange);
   fChain->SetBranchAddress("ShCal_rear_source_quad_1_digitizer_SN", &ShCal_rear_source_quad_1_digitizer_SN, &b_ShCal_rear_source_quad_1_digitizer_SN);
   fChain->SetBranchAddress("ShCal_rear_source_quad_1_digitizer_handle", &ShCal_rear_source_quad_1_digitizer_handle, &b_ShCal_rear_source_quad_1_digitizer_handle);
   fChain->SetBranchAddress("ShCal_rear_source_quad_1_channel", &ShCal_rear_source_quad_1_channel, &b_ShCal_rear_source_quad_1_channel);
   fChain->SetBranchAddress("ShCal_rear_source_quad_1_vrange", &ShCal_rear_source_quad_1_vrange, &b_ShCal_rear_source_quad_1_vrange);
   fChain->SetBranchAddress("ShCal_rear_source_quad_2_digitizer_SN", &ShCal_rear_source_quad_2_digitizer_SN, &b_ShCal_rear_source_quad_2_digitizer_SN);
   fChain->SetBranchAddress("ShCal_rear_source_quad_2_digitizer_handle", &ShCal_rear_source_quad_2_digitizer_handle, &b_ShCal_rear_source_quad_2_digitizer_handle);
   fChain->SetBranchAddress("ShCal_rear_source_quad_2_channel", &ShCal_rear_source_quad_2_channel, &b_ShCal_rear_source_quad_2_channel);
   fChain->SetBranchAddress("ShCal_rear_source_quad_2_vrange", &ShCal_rear_source_quad_2_vrange, &b_ShCal_rear_source_quad_2_vrange);
   fChain->SetBranchAddress("ShCal_rear_source_quad_3_digitizer_SN", &ShCal_rear_source_quad_3_digitizer_SN, &b_ShCal_rear_source_quad_3_digitizer_SN);
   fChain->SetBranchAddress("ShCal_rear_source_quad_3_digitizer_handle", &ShCal_rear_source_quad_3_digitizer_handle, &b_ShCal_rear_source_quad_3_digitizer_handle);
   fChain->SetBranchAddress("ShCal_rear_source_quad_3_channel", &ShCal_rear_source_quad_3_channel, &b_ShCal_rear_source_quad_3_channel);
   fChain->SetBranchAddress("ShCal_rear_source_quad_3_vrange", &ShCal_rear_source_quad_3_vrange, &b_ShCal_rear_source_quad_3_vrange);
   fChain->SetBranchAddress("ShCal_rear_source_quad_4_digitizer_SN", &ShCal_rear_source_quad_4_digitizer_SN, &b_ShCal_rear_source_quad_4_digitizer_SN);
   fChain->SetBranchAddress("ShCal_rear_source_quad_4_digitizer_handle", &ShCal_rear_source_quad_4_digitizer_handle, &b_ShCal_rear_source_quad_4_digitizer_handle);
   fChain->SetBranchAddress("ShCal_rear_source_quad_4_channel", &ShCal_rear_source_quad_4_channel, &b_ShCal_rear_source_quad_4_channel);
   fChain->SetBranchAddress("ShCal_rear_source_quad_4_vrange", &ShCal_rear_source_quad_4_vrange, &b_ShCal_rear_source_quad_4_vrange);
   fChain->SetBranchAddress("FCal_rod_front_nosource_digitizer_SN", &FCal_rod_front_nosource_digitizer_SN, &b_FCal_rod_front_nosource_digitizer_SN);
   fChain->SetBranchAddress("FCal_rod_front_nosource_digitizer_handle", &FCal_rod_front_nosource_digitizer_handle, &b_FCal_rod_front_nosource_digitizer_handle);
   fChain->SetBranchAddress("FCal_rod_front_nosource_channel", &FCal_rod_front_nosource_channel, &b_FCal_rod_front_nosource_channel);
   fChain->SetBranchAddress("FCal_rod_front_nosource_vrange", &FCal_rod_front_nosource_vrange, &b_FCal_rod_front_nosource_vrange);
   fChain->SetBranchAddress("FCal_rod_mid_nosource_digitizer_SN", &FCal_rod_mid_nosource_digitizer_SN, &b_FCal_rod_mid_nosource_digitizer_SN);
   fChain->SetBranchAddress("FCal_rod_mid_nosource_digitizer_handle", &FCal_rod_mid_nosource_digitizer_handle, &b_FCal_rod_mid_nosource_digitizer_handle);
   fChain->SetBranchAddress("FCal_rod_mid_nosource_channel", &FCal_rod_mid_nosource_channel, &b_FCal_rod_mid_nosource_channel);
   fChain->SetBranchAddress("FCal_rod_mid_nosource_vrange", &FCal_rod_mid_nosource_vrange, &b_FCal_rod_mid_nosource_vrange);
   fChain->SetBranchAddress("FCal_rod_rear_nosource_digitizer_SN", &FCal_rod_rear_nosource_digitizer_SN, &b_FCal_rod_rear_nosource_digitizer_SN);
   fChain->SetBranchAddress("FCal_rod_rear_nosource_digitizer_handle", &FCal_rod_rear_nosource_digitizer_handle, &b_FCal_rod_rear_nosource_digitizer_handle);
   fChain->SetBranchAddress("FCal_rod_rear_nosource_channel", &FCal_rod_rear_nosource_channel, &b_FCal_rod_rear_nosource_channel);
   fChain->SetBranchAddress("FCal_rod_rear_nosource_vrange", &FCal_rod_rear_nosource_vrange, &b_FCal_rod_rear_nosource_vrange);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_1_digitizer_SN", &ShCal_front_nosource_quad_1_digitizer_SN, &b_ShCal_front_nosource_quad_1_digitizer_SN);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_1_digitizer_handle", &ShCal_front_nosource_quad_1_digitizer_handle, &b_ShCal_front_nosource_quad_1_digitizer_handle);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_1_channel", &ShCal_front_nosource_quad_1_channel, &b_ShCal_front_nosource_quad_1_channel);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_1_vrange", &ShCal_front_nosource_quad_1_vrange, &b_ShCal_front_nosource_quad_1_vrange);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_2_digitizer_SN", &ShCal_front_nosource_quad_2_digitizer_SN, &b_ShCal_front_nosource_quad_2_digitizer_SN);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_2_digitizer_handle", &ShCal_front_nosource_quad_2_digitizer_handle, &b_ShCal_front_nosource_quad_2_digitizer_handle);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_2_channel", &ShCal_front_nosource_quad_2_channel, &b_ShCal_front_nosource_quad_2_channel);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_2_vrange", &ShCal_front_nosource_quad_2_vrange, &b_ShCal_front_nosource_quad_2_vrange);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_3_digitizer_SN", &ShCal_front_nosource_quad_3_digitizer_SN, &b_ShCal_front_nosource_quad_3_digitizer_SN);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_3_digitizer_handle", &ShCal_front_nosource_quad_3_digitizer_handle, &b_ShCal_front_nosource_quad_3_digitizer_handle);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_3_channel", &ShCal_front_nosource_quad_3_channel, &b_ShCal_front_nosource_quad_3_channel);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_3_vrange", &ShCal_front_nosource_quad_3_vrange, &b_ShCal_front_nosource_quad_3_vrange);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_4_digitizer_SN", &ShCal_front_nosource_quad_4_digitizer_SN, &b_ShCal_front_nosource_quad_4_digitizer_SN);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_4_digitizer_handle", &ShCal_front_nosource_quad_4_digitizer_handle, &b_ShCal_front_nosource_quad_4_digitizer_handle);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_4_channel", &ShCal_front_nosource_quad_4_channel, &b_ShCal_front_nosource_quad_4_channel);
   fChain->SetBranchAddress("ShCal_front_nosource_quad_4_vrange", &ShCal_front_nosource_quad_4_vrange, &b_ShCal_front_nosource_quad_4_vrange);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_1_digitizer_SN", &ShCal_rear_nosource_quad_1_digitizer_SN, &b_ShCal_rear_nosource_quad_1_digitizer_SN);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_1_digitizer_handle", &ShCal_rear_nosource_quad_1_digitizer_handle, &b_ShCal_rear_nosource_quad_1_digitizer_handle);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_1_channel", &ShCal_rear_nosource_quad_1_channel, &b_ShCal_rear_nosource_quad_1_channel);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_1_vrange", &ShCal_rear_nosource_quad_1_vrange, &b_ShCal_rear_nosource_quad_1_vrange);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_2_digitizer_SN", &ShCal_rear_nosource_quad_2_digitizer_SN, &b_ShCal_rear_nosource_quad_2_digitizer_SN);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_2_digitizer_handle", &ShCal_rear_nosource_quad_2_digitizer_handle, &b_ShCal_rear_nosource_quad_2_digitizer_handle);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_2_channel", &ShCal_rear_nosource_quad_2_channel, &b_ShCal_rear_nosource_quad_2_channel);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_2_vrange", &ShCal_rear_nosource_quad_2_vrange, &b_ShCal_rear_nosource_quad_2_vrange);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_3_digitizer_SN", &ShCal_rear_nosource_quad_3_digitizer_SN, &b_ShCal_rear_nosource_quad_3_digitizer_SN);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_3_digitizer_handle", &ShCal_rear_nosource_quad_3_digitizer_handle, &b_ShCal_rear_nosource_quad_3_digitizer_handle);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_3_channel", &ShCal_rear_nosource_quad_3_channel, &b_ShCal_rear_nosource_quad_3_channel);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_3_vrange", &ShCal_rear_nosource_quad_3_vrange, &b_ShCal_rear_nosource_quad_3_vrange);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_4_digitizer_SN", &ShCal_rear_nosource_quad_4_digitizer_SN, &b_ShCal_rear_nosource_quad_4_digitizer_SN);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_4_digitizer_handle", &ShCal_rear_nosource_quad_4_digitizer_handle, &b_ShCal_rear_nosource_quad_4_digitizer_handle);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_4_channel", &ShCal_rear_nosource_quad_4_channel, &b_ShCal_rear_nosource_quad_4_channel);
   fChain->SetBranchAddress("ShCal_rear_nosource_quad_4_vrange", &ShCal_rear_nosource_quad_4_vrange, &b_ShCal_rear_nosource_quad_4_vrange);
   fChain->SetBranchAddress("S1_counter_digitizer_SN", &S1_counter_digitizer_SN, &b_S1_counter_digitizer_SN);
   fChain->SetBranchAddress("S1_counter_digitizer_handle", &S1_counter_digitizer_handle, &b_S1_counter_digitizer_handle);
   fChain->SetBranchAddress("S1_counter_channel", &S1_counter_channel, &b_S1_counter_channel);
   fChain->SetBranchAddress("S1_counter_vrange", &S1_counter_vrange, &b_S1_counter_vrange);
   fChain->SetBranchAddress("S2_counter_digitizer_SN", &S2_counter_digitizer_SN, &b_S2_counter_digitizer_SN);
   fChain->SetBranchAddress("S2_counter_digitizer_handle", &S2_counter_digitizer_handle, &b_S2_counter_digitizer_handle);
   fChain->SetBranchAddress("S2_counter_channel", &S2_counter_channel, &b_S2_counter_channel);
   fChain->SetBranchAddress("S2_counter_vrange", &S2_counter_vrange, &b_S2_counter_vrange);
   fChain->SetBranchAddress("S3_counter_digitizer_SN", &S3_counter_digitizer_SN, &b_S3_counter_digitizer_SN);
   fChain->SetBranchAddress("S3_counter_digitizer_handle", &S3_counter_digitizer_handle, &b_S3_counter_digitizer_handle);
   fChain->SetBranchAddress("S3_counter_channel", &S3_counter_channel, &b_S3_counter_channel);
   fChain->SetBranchAddress("S3_counter_vrange", &S3_counter_vrange, &b_S3_counter_vrange);
   fChain->SetBranchAddress("S4_counter_digitizer_SN", &S4_counter_digitizer_SN, &b_S4_counter_digitizer_SN);
   fChain->SetBranchAddress("Leakage_counter_digitizer_handle", &Leakage_counter_digitizer_handle, &b_Leakage_counter_digitizer_handle);
   fChain->SetBranchAddress("S4_counter_channel", &S4_counter_channel, &b_S4_counter_channel);
   fChain->SetBranchAddress("S4_counter_vrange", &S4_counter_vrange, &b_S4_counter_vrange);
   fChain->SetBranchAddress("Leakage_counter_digitizer_SN", &Leakage_counter_digitizer_SN, &b_Leakage_counter_digitizer_SN);
   fChain->SetBranchAddress("Leakage_counter_channel", &Leakage_counter_channel, &b_Leakage_counter_channel);
   fChain->SetBranchAddress("Leakage_counter_vrange", &Leakage_counter_vrange, &b_Leakage_counter_vrange);
   fChain->SetBranchAddress("Halo_counter_digitizer_SN", &Halo_counter_digitizer_SN, &b_Halo_counter_digitizer_SN);
   fChain->SetBranchAddress("Halo_counter_digitizer_handle", &Halo_counter_digitizer_handle, &b_Halo_counter_digitizer_handle);
   fChain->SetBranchAddress("Halo_counter_channel", &Halo_counter_channel, &b_Halo_counter_channel);
   fChain->SetBranchAddress("Halo_counter_vrange", &Halo_counter_vrange, &b_Halo_counter_vrange);
   fChain->SetBranchAddress("Digitizer_1_trigger_digitizer_SN", &Digitizer_1_trigger_digitizer_SN, &b_Digitizer_1_trigger_digitizer_SN);
   fChain->SetBranchAddress("Digitizer_1_trigger_digitizer_handle", &Digitizer_1_trigger_digitizer_handle, &b_Digitizer_1_trigger_digitizer_handle);
   fChain->SetBranchAddress("Digitizer_1_trigger_channel", &Digitizer_1_trigger_channel, &b_Digitizer_1_trigger_channel);
   fChain->SetBranchAddress("Digitizer_1_trigger_vrange", &Digitizer_1_trigger_vrange, &b_Digitizer_1_trigger_vrange);
   fChain->SetBranchAddress("Digitizer_2_trigger_digitizer_SN", &Digitizer_2_trigger_digitizer_SN, &b_Digitizer_2_trigger_digitizer_SN);
   fChain->SetBranchAddress("Digitizer_2_trigger_digitizer_handle", &Digitizer_2_trigger_digitizer_handle, &b_Digitizer_2_trigger_digitizer_handle);
   fChain->SetBranchAddress("Digitizer_2_trigger_channel", &Digitizer_2_trigger_channel, &b_Digitizer_2_trigger_channel);
   fChain->SetBranchAddress("Digitizer_2_trigger_vrange", &Digitizer_2_trigger_vrange, &b_Digitizer_2_trigger_vrange);
   fChain->SetBranchAddress("Digitizer_3_trigger_digitizer_SN", &Digitizer_3_trigger_digitizer_SN, &b_Digitizer_3_trigger_digitizer_SN);
   fChain->SetBranchAddress("Digitizer_3_trigger_digitizer_handle", &Digitizer_3_trigger_digitizer_handle, &b_Digitizer_3_trigger_digitizer_handle);
   fChain->SetBranchAddress("Digitizer_3_trigger_channel", &Digitizer_3_trigger_channel, &b_Digitizer_3_trigger_channel);
   fChain->SetBranchAddress("Digitizer_3_trigger_vrange", &Digitizer_3_trigger_vrange, &b_Digitizer_3_trigger_vrange);
   fChain->SetBranchAddress("Digitizer_4_trigger_digitizer_SN", &Digitizer_4_trigger_digitizer_SN, &b_Digitizer_4_trigger_digitizer_SN);
   fChain->SetBranchAddress("Digitizer_4_trigger_digitizer_handle", &Digitizer_4_trigger_digitizer_handle, &b_Digitizer_4_trigger_digitizer_handle);
   fChain->SetBranchAddress("Digitizer_4_trigger_channel", &Digitizer_4_trigger_channel, &b_Digitizer_4_trigger_channel);
   fChain->SetBranchAddress("Digitizer_4_trigger_vrange", &Digitizer_4_trigger_vrange, &b_Digitizer_4_trigger_vrange);
   fChain->SetBranchAddress("good_BPC_data", &good_BPC_data, &b_good_BPC_data);
   fChain->SetBranchAddress("BPC_tag", &BPC_tag, &b_BPC_tag);
   fChain->SetBranchAddress("BPC_runNumber", &BPC_runNumber, &b_BPC_runNumber);
   fChain->SetBranchAddress("BPC_spillNumber", &BPC_spillNumber, &b_BPC_spillNumber);
   fChain->SetBranchAddress("BPC_triggerNumber", &BPC_triggerNumber, &b_BPC_triggerNumber);
   fChain->SetBranchAddress("BPC_accum_time", &BPC_accum_time, &b_BPC_accum_time);
   fChain->SetBranchAddress("BPC_wall_time", &BPC_wall_time, &b_BPC_wall_time);
   fChain->SetBranchAddress("BPC_x0", &BPC_x0, &b_BPC_x0);
   fChain->SetBranchAddress("BPC_y0", &BPC_y0, &b_BPC_y0);
   fChain->SetBranchAddress("BPC_x1", &BPC_x1, &b_BPC_x1);
   fChain->SetBranchAddress("BPC_y1", &BPC_y1, &b_BPC_y1);
   fChain->SetBranchAddress("BPC_x2", &BPC_x2, &b_BPC_x2);
   fChain->SetBranchAddress("BPC_y2", &BPC_y2, &b_BPC_y2);
   fChain->SetBranchAddress("BPC_x3", &BPC_x3, &b_BPC_x3);
   fChain->SetBranchAddress("BPC_y3", &BPC_y3, &b_BPC_y3);
   Notify();
}

Bool_t merged_digitizer_BPC::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void merged_digitizer_BPC::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t merged_digitizer_BPC::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef merged_digitizer_BPC_cxx
