#!/bin/bash

runDir=/home/varnes/FCalPulse/2022/production/fcalpulse/

PID=`ps -ef | grep ${runDir}/FCP_DAQ/src/FCP_DAQ | grep -v grep | awk '{print $2}'`
if [[ $PID -ne "" ]]
then
    kill $PID
fi


