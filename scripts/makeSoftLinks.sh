#!/bin/bash

dataDir=/home/varnes/FCalPulse/data/2022 

runNumber=$1

echo $runNumber

for  file in `find $dataDir -name "run${runNumber}*.root" | grep -v all`
do
   echo $file
   shortfile=`echo $file | awk -F/ '{print $NF;}'`
   echo $shortfile
   ln -sr $file ${dataDir}/all/${shortFile}
done
