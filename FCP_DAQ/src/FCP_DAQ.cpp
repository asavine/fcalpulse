/******************************************************************************
 *
 * Description: 
 *
-----------------------------------------------------------------------------
* This is the DAQ program for the FCalPulse testbeam.  It is based on
CAEN's * WaveDump utility, but modified for the specific needs of
FCalPulse
 *
 * Contact person: Erich Varnes (varnes@physics.arizona.edu)
 *  ----------------------------------------------------------------------------
 *  Syntax: FCP_DAQ
 ******************************************************************************/

#define FCP_DAQ_MajorVersion      2
#define FCP_DAQ_MinorVersion      2
#define FCP_DAQ_Revision          2
#define FCP_DAQ_Release_Date   "Aug 15 2022"
#define FCP_DAQ_Data_Format_Version 4 // 2000 samples recorded
#define DBG_TIME
#define OUTPUT_TOPLEVEL_DIRECTORY "/home/varnes/FCalPulse/data/2022"  // "/data1/testData"
#define RUNDIR "/home/varnes/FCalPulse/2022/production/fcalpulse/"

#define DIGITIZER_1_SN 1502  // the "TRIUMF" digitizer
#define DIGITIZER_2_SN 1756  // the "Arizona" digitizer    
#define DIGITIZER_3_SN 14028    // one of the "CERN" digitizers
#define DIGITIZER_4_SN 14018    // one of the "CERN" digitizers 

#define FCAL_ROD_FRONT_NOSOURCE_DIGITIZER_SN DIGITIZER_4_SN
#define FCAL_ROD_FRONT_NOSOURCE_DIGITIZER_HANDLE DIGITIZER_4_HANDLE
#define FCAL_ROD_FRONT_NOSOURCE_CHANNEL 1
#define FCAL_ROD_MID_NOSOURCE_DIGITIZER_SN DIGITIZER_4_SN
#define FCAL_ROD_MID_NOSOURCE_DIGITIZER_HANDLE DIGITIZER_4_HANDLE
#define FCAL_ROD_MID_NOSOURCE_CHANNEL 0
#define FCAL_ROD_REAR_NOSOURCE_DIGITIZER_SN DIGITIZER_4_SN
#define FCAL_ROD_REAR_NOSOURCE_DIGITIZER_HANDLE DIGITIZER_4_HANDLE
#define FCAL_ROD_REAR_NOSOURCE_CHANNEL 2
#define SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_SN DIGITIZER_3_SN
#define SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_HANDLE DIGITIZER_3_HANDLE
#define SHCAL_FRONT_SOURCE_QUAD_1_CHANNEL 1
#define SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_SN DIGITIZER_3_SN
#define SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE DIGITIZER_3_HANDLE
#define SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL 0
#define SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_SN DIGITIZER_3_SN
#define SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_HANDLE DIGITIZER_3_HANDLE
#define SHCAL_FRONT_SOURCE_QUAD_3_CHANNEL 3
#define SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_SN DIGITIZER_3_SN
#define SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_HANDLE DIGITIZER_3_HANDLE
#define SHCAL_FRONT_SOURCE_QUAD_4_CHANNEL 2
#define SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_SN DIGITIZER_4_SN
#define SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_HANDLE DIGITIZER_4_HANDLE
#define SHCAL_REAR_SOURCE_QUAD_1_CHANNEL 4
#define SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_SN DIGITIZER_4_SN
#define SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_HANDLE DIGITIZER_4_HANDLE
#define SHCAL_REAR_SOURCE_QUAD_2_CHANNEL 3
#define SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_SN DIGITIZER_4_SN
#define SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_HANDLE DIGITIZER_4_HANDLE
#define SHCAL_REAR_SOURCE_QUAD_3_CHANNEL 6
#define SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_SN DIGITIZER_4_SN
#define SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_HANDLE DIGITIZER_4_HANDLE
#define SHCAL_REAR_SOURCE_QUAD_4_CHANNEL 5

#define FCAL_ROD_FRONT_SOURCE_DIGITIZER_SN DIGITIZER_2_SN
#define FCAL_ROD_FRONT_SOURCE_DIGITIZER_HANDLE DIGITIZER_2_HANDLE
#define FCAL_ROD_FRONT_SOURCE_CHANNEL 1
#define FCAL_ROD_MID_SOURCE_DIGITIZER_SN DIGITIZER_2_SN
#define FCAL_ROD_MID_SOURCE_DIGITIZER_HANDLE DIGITIZER_2_HANDLE
#define FCAL_ROD_MID_SOURCE_CHANNEL 2
#define FCAL_ROD_REAR_SOURCE_DIGITIZER_SN DIGITIZER_2_SN
#define FCAL_ROD_REAR_SOURCE_DIGITIZER_HANDLE DIGITIZER_2_HANDLE
#define FCAL_ROD_REAR_SOURCE_CHANNEL 0
#define SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_SN DIGITIZER_1_SN
#define SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_HANDLE DIGITIZER_1_HANDLE
#define SHCAL_FRONT_NOSOURCE_QUAD_1_CHANNEL 2
#define SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_SN DIGITIZER_1_SN
#define SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_HANDLE DIGITIZER_1_HANDLE
#define SHCAL_FRONT_NOSOURCE_QUAD_2_CHANNEL 3
#define SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_SN DIGITIZER_1_SN
#define SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_HANDLE DIGITIZER_1_HANDLE
#define SHCAL_FRONT_NOSOURCE_QUAD_3_CHANNEL 0
#define SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_SN DIGITIZER_1_SN
#define SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_HANDLE DIGITIZER_1_HANDLE
#define SHCAL_FRONT_NOSOURCE_QUAD_4_CHANNEL 1
#define SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_SN DIGITIZER_2_SN
#define SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_HANDLE DIGITIZER_2_HANDLE
#define SHCAL_REAR_NOSOURCE_QUAD_1_CHANNEL 5
#define SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_SN DIGITIZER_2_SN
#define SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_HANDLE DIGITIZER_2_HANDLE
#define SHCAL_REAR_NOSOURCE_QUAD_2_CHANNEL 6
#define SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_SN DIGITIZER_2_SN
#define SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_HANDLE DIGITIZER_2_HANDLE
#define SHCAL_REAR_NOSOURCE_QUAD_3_CHANNEL 3
#define SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_SN DIGITIZER_2_SN
#define SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_HANDLE DIGITIZER_2_HANDLE
#define SHCAL_REAR_NOSOURCE_QUAD_4_CHANNEL 4

#define S1_COUNTER_DIGITIZER_SN DIGITIZER_3_SN
#define S1_COUNTER_DIGITIZER_HANDLE DIGITIZER_3_HANDLE
#define S1_COUNTER_CHANNEL 4
#define S2_COUNTER_DIGITIZER_SN DIGITIZER_3_SN
#define S2_COUNTER_DIGITIZER_HANDLE DIGITIZER_3_HANDLE
#define S2_COUNTER_CHANNEL 5
#define S3_COUNTER_DIGITIZER_SN DIGITIZER_3_SN
#define S3_COUNTER_DIGITIZER_HANDLE DIGITIZER_3_HANDLE
#define S3_COUNTER_CHANNEL 6
#define S4_COUNTER_DIGITIZER_SN DIGITIZER_1_SN
#define S4_COUNTER_DIGITIZER_HANDLE DIGITIZER_1_HANDLE
#define S4_COUNTER_CHANNEL 4
#define LEAKAGE_COUNTER_DIGITIZER_SN DIGITIZER_1_SN
#define LEAKAGE_COUNTER_DIGITIZER_HANDLE DIGITIZER_1_HANDLE
#define LEAKAGE_COUNTER_CHANNEL 6
#define HALO_COUNTER_DIGITIZER_SN DIGITIZER_1_SN
#define HALO_COUNTER_DIGITIZER_HANDLE DIGITIZER_1_HANDLE
#define HALO_COUNTER_CHANNEL 5

#define DIGITIZER_1_TRIGGER_DIGITIZER_SN DIGITIZER_1_SN
#define DIGITIZER_1_TRIGGER_DIGITIZER_HANDLE DIGITIZER_1_HANDLE
#define DIGITIZER_1_TRIGGER_CHANNEL 7
#define DIGITIZER_2_TRIGGER_DIGITIZER_SN DIGITIZER_2_SN
#define DIGITIZER_2_TRIGGER_DIGITIZER_HANDLE DIGITIZER_2_HANDLE
#define DIGITIZER_2_TRIGGER_CHANNEL 7
#define DIGITIZER_3_TRIGGER_DIGITIZER_SN DIGITIZER_3_SN
#define DIGITIZER_3_TRIGGER_DIGITIZER_HANDLE DIGITIZER_3_HANDLE
#define DIGITIZER_3_TRIGGER_CHANNEL 7
#define DIGITIZER_4_TRIGGER_DIGITIZER_SN DIGITIZER_4_SN
#define DIGITIZER_4_TRIGGER_DIGITIZER_HANDLE DIGITIZER_4_HANDLE
#define DIGITIZER_4_TRIGGER_CHANNEL 7

#define GIGABYTE 1073741824

#include <CAENDigitizer.h>
#include <CAENVMElib.h>
#include "FCP_DAQ.h"
#include "portnumber.h"
#include <time.h>

#include <dispatch.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <fstream>
#include <errno.h>
#include <sys/statvfs.h>
 
// ROOT stuff
#include "TTree.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TStopwatch.h"

/* Error messages */
typedef enum  {
  ERR_NONE= 0,
  ERR_DGZ_OPEN,
  ERR_DIGITIZER_INFO_READ,
  ERR_INVALID_DIGITIZER_TYPE,
  ERR_DGZ_PROGRAM,
  ERR_MALLOC,
  ERR_CLEAR,
  ERR_RESTART,
  ERR_READOUT,
  ERR_EVENT_BUILD,
  ERR_UNHANDLED_DIGITIZER,
  ERR_OUTFILE_WRITE,
  ERR_OVERTEMP,
  ERR_DIGITIZER_FAILURE,
  ERR_PLL_LOCK,
  ERR_ADC,
  ERR_DATA_WRITE,
  ERR_SR90_POSITION,
  ERR_DUMMY_LAST
} ERROR_CODES;

static char ErrMsg[ERR_DUMMY_LAST][100] = {
  "No Error",                                         /* ERR_NONE */
  "Can't open the digitizer",                         /* ERR_DGZ_OPEN */
  "Can't read the Digitizer Info",                        /* ERR_DIGITIZER_INFO_READ */
  "Can't run FCP_DAQ for this digitizer",            /* ERR_INVALID_DIGITIZER_TYPE */
  "Can't program the digitizer",                      /* ERR_DGZ_PROGRAM */
  "Can't allocate the memory for the readout buffer", /* ERR_MALLOC */
  "Error clearing data",                              /* ERR_CLEAR */    
  "Restarting Error",                                 /* ERR_RESTART */
  "Readout Error",                                    /* ERR_READOUT */
  "Event Build Error",                                /* ERR_EVENT_BUILD */
  "Unhandled digitizer type",                             /* ERR_UNHANDLED_DIGITIZER */
  "Output file write error",                          /* ERR_OUTFILE_WRITE */
  "Over Temperature",			              /* ERR_OVERTEMP */
  "Digitizer Failure",				      /* ERR_DIGITIZER_FAILURE */
  "PLL Lock Failure",                                 /* ERR_PLL_LOCK */
  "ADC Error",                                         /* ERR_ADC */
  "Error writing data file",                           /* ERR_DATA_WRITE */
  "Sr90 position changed during run"                  /* ERR_SR90_POSITION */     
};

int sim_mode = 0;  // Allows code to run without hardware, for testing purposes
int teststand_mode = 0; // For running in the lab at UA
int merge_BPC_data = 1;  //expect per-spill data from the BPCs
int use_ext_trigger = 1; // 1 for data-taking, 0 for channel self-trigger
int channel_trigger_threshold = 10000; //6000; 
int do_analysis = 0;  // invoke simple analysis on spill by spill basis.  
                      // probably not needed since we have data quality 
                      // monitoring

// initialize to default values so that even if some are disconnected,
// arrays won't be overwritten.  This is to prevent crashes during testing;
// we wouldn't want to really run this way
uint16_t DIGITIZER_1_HANDLE = 0;
//#define DIGITIZER_2_HANDLE DIGITIZER_1_HANDLE
//#define DIGITIZER_3_HANDLE DIGITIZER_1_HANDLE
//#define DIGITIZER_4_HANDLE DIGITIZER_1_HANDLE

uint16_t DIGITIZER_2_HANDLE = 0;
uint16_t DIGITIZER_3_HANDLE = 0;
uint16_t DIGITIZER_4_HANDLE = 0;
//#define DIGITIZER_3_HANDLE DIGITIZER_1_HANDLE
//#define DIGITIZER_4_HANDLE DIGITIZER_2_HANDLE

// log file
FILE* g_logfile = 0;


// buffer for data from the BPC, and one for unexpected data as well
char BPC_data_buf[1000000];
char BPC_data_buf_junk[1000000];

// keep track of time since last attempted socket write (for heartbeat check)
time_t g_last_socket_write_time = 0;

// ControlHost variables
char ch_tag[200] = {"a ACK a BPCDATA"};
//char ch_host[200] = {"128.141.151.231"};
char ch_host[200] = {"local"};

uint32_t ShCalPed_HV_pos = 0xE000;
uint32_t ShCalPed_HV_neg = 0x2000;

//Ntuple branches
TBranch* b_dataFormatVersion = 0;
TBranch* b_channelDataSize = 0;
TBranch* b_NumEventsMax = 0;
TBranch* b_NumEventsInSpill = 0;
TBranch* b_RecordLength = 0;  
TBranch* b_PostTrigger = 0;
TBranch* b_FPIOtype = 0;
TBranch* b_NumDigitizers = 0;
TBranch* b_SerialNumber = 0;
TBranch* b_ExtTriggerMode = 0;
TBranch* b_EnableMask = 0;
TBranch* b_DCoffset = 0;
TBranch* b_ChannelTriggerMode = 0;
TBranch* b_PulsePolarity = 0;
TBranch* b_GWn = 0; 
TBranch* b_GWaddr = 0;
TBranch* b_GWdata = 0;
TBranch* b_GWmask = 0;
TBranch* b_nCryostats = 0;
TBranch* b_scintUsed = 0;
TBranch* b_tailVeto = 0;
TBranch* b_sourceInBeam = 0;
TBranch* b_runNumber = 0;
TBranch* b_spillNumber = 0;
TBranch* b_spillStartTime = 0;
TBranch* b_BeamStatus = 0;
TBranch* b_digitizerFailBits = 0;
TBranch* b_SCal_HV_set = 0;
TBranch* b_SCal_HV_read = 0;
TBranch* b_FCal_inner_HV_set = 0;
TBranch* b_FCal_tubeA_HV_read = 0;
TBranch* b_FCal_outer_HV_set = 0;
TBranch* b_FCal_tubeB_HV_read = 0;
TBranch* b_FCal_tubeC_HV_read = 0;
TBranch* b_eventNum = 0;
TBranch* b_triggerTimeTag = 0;
TBranch* b_nchannels = 0;
TBranch* b_channel = 0;   // needed?
TBranch* b_eventCounter = 0; // needed?
//TBranch* b_ADC_data[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
// intuitive channel names for actual data-taking
TBranch* b_FCal_rod_front_source_data = 0;
TBranch* b_FCal_rod_front_source_digitizer_SN = 0;
TBranch* b_FCal_rod_front_source_digitizer_handle = 0;
TBranch* b_FCal_rod_front_source_channel = 0;
TBranch* b_FCal_rod_front_source_vrange = 0;
TBranch* b_FCal_rod_mid_source_data = 0;
TBranch* b_FCal_rod_mid_source_digitizer_SN = 0;
TBranch* b_FCal_rod_mid_source_digitizer_handle = 0;
TBranch* b_FCal_rod_mid_source_channel = 0;
TBranch* b_FCal_rod_mid_source_vrange = 0;
TBranch* b_FCal_rod_rear_source_data = 0;
TBranch* b_FCal_rod_rear_source_digitizer_SN = 0;
TBranch* b_FCal_rod_rear_source_digitizer_handle = 0;
TBranch* b_FCal_rod_rear_source_channel = 0;
TBranch* b_FCal_rod_rear_source_vrange = 0;
TBranch* b_ShCal_front_source_quad_1_data = 0;
TBranch* b_ShCal_front_source_quad_1_digitizer_SN = 0;
TBranch* b_ShCal_front_source_quad_1_digitizer_handle = 0;
TBranch* b_ShCal_front_source_quad_1_channel = 0;
TBranch* b_ShCal_front_source_quad_1_vrange = 0;
TBranch* b_ShCal_front_source_quad_2_data = 0;
TBranch* b_ShCal_front_source_quad_2_digitizer_SN = 0;
TBranch* b_ShCal_front_source_quad_2_digitizer_handle = 0;
TBranch* b_ShCal_front_source_quad_2_channel = 0;
TBranch* b_ShCal_front_source_quad_2_vrange = 0;
TBranch* b_ShCal_front_source_quad_3_data = 0;
TBranch* b_ShCal_front_source_quad_3_digitizer_SN = 0;
TBranch* b_ShCal_front_source_quad_3_digitizer_handle = 0;
TBranch* b_ShCal_front_source_quad_3_channel = 0;
TBranch* b_ShCal_front_source_quad_3_vrange = 0;
TBranch* b_ShCal_front_source_quad_4_data = 0;
TBranch* b_ShCal_front_source_quad_4_digitizer_SN = 0;
TBranch* b_ShCal_front_source_quad_4_digitizer_handle = 0;
TBranch* b_ShCal_front_source_quad_4_channel = 0;
TBranch* b_ShCal_front_source_quad_4_vrange = 0;
TBranch* b_ShCal_rear_source_quad_1_data = 0;
TBranch* b_ShCal_rear_source_quad_1_digitizer_SN = 0;
TBranch* b_ShCal_rear_source_quad_1_digitizer_handle = 0;
TBranch* b_ShCal_rear_source_quad_1_channel = 0;
TBranch* b_ShCal_rear_source_quad_1_vrange = 0;
TBranch* b_ShCal_rear_source_quad_2_data = 0;
TBranch* b_ShCal_rear_source_quad_2_digitizer_SN = 0;
TBranch* b_ShCal_rear_source_quad_2_digitizer_handle = 0;
TBranch* b_ShCal_rear_source_quad_2_channel = 0;
TBranch* b_ShCal_rear_source_quad_2_vrange = 0;
TBranch* b_ShCal_rear_source_quad_3_data = 0;
TBranch* b_ShCal_rear_source_quad_3_digitizer_SN = 0;
TBranch* b_ShCal_rear_source_quad_3_digitizer_handle = 0;
TBranch* b_ShCal_rear_source_quad_3_channel = 0;
TBranch* b_ShCal_rear_source_quad_3_vrange = 0;
TBranch* b_ShCal_rear_source_quad_4_data = 0;
TBranch* b_ShCal_rear_source_quad_4_digitizer_SN = 0;
TBranch* b_ShCal_rear_source_quad_4_digitizer_handle = 0;
TBranch* b_ShCal_rear_source_quad_4_channel = 0;
TBranch* b_ShCal_rear_source_quad_4_vrange = 0;
TBranch* b_FCal_rod_front_nosource_data = 0;
TBranch* b_FCal_rod_front_nosource_digitizer_SN = 0;
TBranch* b_FCal_rod_front_nosource_digitizer_handle = 0;
TBranch* b_FCal_rod_front_nosource_channel = 0;
TBranch* b_FCal_rod_front_nosource_vrange = 0;
TBranch* b_FCal_rod_mid_nosource_data = 0;
TBranch* b_FCal_rod_mid_nosource_digitizer_SN = 0;
TBranch* b_FCal_rod_mid_nosource_digitizer_handle = 0;
TBranch* b_FCal_rod_mid_nosource_channel = 0;
TBranch* b_FCal_rod_mid_nosource_vrange = 0;
TBranch* b_FCal_rod_rear_nosource_data = 0;
TBranch* b_FCal_rod_rear_nosource_digitizer_SN = 0;
TBranch* b_FCal_rod_rear_nosource_digitizer_handle = 0;
TBranch* b_FCal_rod_rear_nosource_channel = 0;
TBranch* b_FCal_rod_rear_nosource_vrange = 0;
TBranch* b_ShCal_front_nosource_quad_1_data = 0;
TBranch* b_ShCal_front_nosource_quad_1_digitizer_SN = 0;
TBranch* b_ShCal_front_nosource_quad_1_digitizer_handle = 0;
TBranch* b_ShCal_front_nosource_quad_1_channel = 0;
TBranch* b_ShCal_front_nosource_quad_1_vrange = 0;
TBranch* b_ShCal_front_nosource_quad_2_data = 0;
TBranch* b_ShCal_front_nosource_quad_2_digitizer_SN = 0;
TBranch* b_ShCal_front_nosource_quad_2_digitizer_handle = 0;
TBranch* b_ShCal_front_nosource_quad_2_channel = 0;
TBranch* b_ShCal_front_nosource_quad_2_vrange = 0;
TBranch* b_ShCal_front_nosource_quad_3_data = 0;
TBranch* b_ShCal_front_nosource_quad_3_digitizer_SN = 0;
TBranch* b_ShCal_front_nosource_quad_3_digitizer_handle = 0;
TBranch* b_ShCal_front_nosource_quad_3_channel = 0;
TBranch* b_ShCal_front_nosource_quad_3_vrange = 0;
TBranch* b_ShCal_front_nosource_quad_4_data = 0;
TBranch* b_ShCal_front_nosource_quad_4_digitizer_SN = 0;
TBranch* b_ShCal_front_nosource_quad_4_digitizer_handle = 0;
TBranch* b_ShCal_front_nosource_quad_4_channel = 0;
TBranch* b_ShCal_front_nosource_quad_4_vrange = 0;
TBranch* b_ShCal_rear_nosource_quad_1_data = 0;
TBranch* b_ShCal_rear_nosource_quad_1_digitizer_SN = 0;
TBranch* b_ShCal_rear_nosource_quad_1_digitizer_handle = 0;
TBranch* b_ShCal_rear_nosource_quad_1_channel = 0;
TBranch* b_ShCal_rear_nosource_quad_1_vrange = 0;
TBranch* b_ShCal_rear_nosource_quad_2_data = 0;
TBranch* b_ShCal_rear_nosource_quad_2_digitizer_SN = 0;
TBranch* b_ShCal_rear_nosource_quad_2_digitizer_handle = 0;
TBranch* b_ShCal_rear_nosource_quad_2_channel = 0;
TBranch* b_ShCal_rear_nosource_quad_2_vrange = 0;
TBranch* b_ShCal_rear_nosource_quad_3_data = 0;
TBranch* b_ShCal_rear_nosource_quad_3_digitizer_SN = 0;
TBranch* b_ShCal_rear_nosource_quad_3_digitizer_handle = 0;
TBranch* b_ShCal_rear_nosource_quad_3_channel = 0;
TBranch* b_ShCal_rear_nosource_quad_3_vrange = 0;
TBranch* b_ShCal_rear_nosource_quad_4_data = 0;
TBranch* b_ShCal_rear_nosource_quad_4_digitizer_SN = 0;
TBranch* b_ShCal_rear_nosource_quad_4_digitizer_handle = 0;
TBranch* b_ShCal_rear_nosource_quad_4_channel = 0;
TBranch* b_ShCal_rear_nosource_quad_4_vrange = 0;
TBranch* b_S1_counter_data = 0;
TBranch* b_S1_counter_digitizer_SN = 0;
TBranch* b_S1_counter_digitizer_handle = 0;
TBranch* b_S1_counter_channel = 0;
TBranch* b_S1_counter_vrange = 0;
TBranch* b_S2_counter_data = 0;
TBranch* b_S2_counter_digitizer_SN = 0;
TBranch* b_S2_counter_digitizer_handle = 0;
TBranch* b_S2_counter_channel = 0;
TBranch* b_S2_counter_vrange = 0;
TBranch* b_S3_counter_data = 0;
TBranch* b_S3_counter_digitizer_SN = 0;
TBranch* b_S3_counter_digitizer_handle = 0;
TBranch* b_S3_counter_channel = 0;
TBranch* b_S3_counter_vrange = 0;
TBranch* b_S4_counter_data = 0;
TBranch* b_S4_counter_digitizer_SN = 0;
TBranch* b_S4_counter_digitizer_handle = 0;
TBranch* b_S4_counter_channel = 0;
TBranch* b_S4_counter_vrange = 0;
TBranch* b_Leakage_counter_data = 0;
TBranch* b_Leakage_counter_digitizer_SN = 0;
TBranch* b_Leakage_counter_digitizer_handle = 0;
TBranch* b_Leakage_counter_channel = 0;
TBranch* b_Leakage_counter_vrange = 0;
TBranch* b_Halo_counter_data = 0;
TBranch* b_Halo_counter_digitizer_SN = 0;
TBranch* b_Halo_counter_digitizer_handle = 0;
TBranch* b_Halo_counter_channel = 0;
TBranch* b_Halo_counter_vrange = 0;
TBranch* b_Digitizer_1_trigger_data = 0;
TBranch* b_Digitizer_1_trigger_digitizer_SN = 0;
TBranch* b_Digitizer_1_trigger_digitizer_handle = 0;
TBranch* b_Digitizer_1_trigger_channel = 0;
TBranch* b_Digitizer_1_trigger_vrange = 0;
TBranch* b_Digitizer_2_trigger_data = 0;
TBranch* b_Digitizer_2_trigger_digitizer_SN = 0;
TBranch* b_Digitizer_2_trigger_digitizer_handle = 0;
TBranch* b_Digitizer_2_trigger_channel = 0;
TBranch* b_Digitizer_2_trigger_vrange = 0;
TBranch* b_Digitizer_3_trigger_data = 0;
TBranch* b_Digitizer_3_trigger_digitizer_SN = 0;
TBranch* b_Digitizer_3_trigger_digitizer_handle = 0;
TBranch* b_Digitizer_3_trigger_channel = 0;
TBranch* b_Digitizer_3_trigger_vrange = 0;
TBranch* b_Digitizer_4_trigger_data = 0;
TBranch* b_Digitizer_4_trigger_digitizer_SN = 0;
TBranch* b_Digitizer_4_trigger_digitizer_handle = 0;
TBranch* b_Digitizer_4_trigger_channel = 0;
TBranch* b_Digitizer_4_trigger_vrange = 0;

//basic branch names for teststand datataking
TBranch* b_DIGITIZER_1_channel_0_data = 0;
TBranch* b_DIGITIZER_1_channel_1_data = 0;
TBranch* b_DIGITIZER_1_channel_2_data = 0;
TBranch* b_DIGITIZER_1_channel_3_data = 0;
TBranch* b_DIGITIZER_1_channel_4_data = 0;
TBranch* b_DIGITIZER_1_channel_5_data = 0;
TBranch* b_DIGITIZER_1_channel_6_data = 0;
TBranch* b_DIGITIZER_1_channel_7_data = 0;
TBranch* b_DIGITIZER_2_channel_0_data = 0;
TBranch* b_DIGITIZER_2_channel_1_data = 0;
TBranch* b_DIGITIZER_2_channel_2_data = 0;
TBranch* b_DIGITIZER_2_channel_3_data = 0;
TBranch* b_DIGITIZER_2_channel_4_data = 0;
TBranch* b_DIGITIZER_2_channel_5_data = 0;
TBranch* b_DIGITIZER_2_channel_6_data = 0;
TBranch* b_DIGITIZER_2_channel_7_data = 0;
TBranch* b_DIGITIZER_1_channel_0_vrange = 0;
TBranch* b_DIGITIZER_1_channel_1_vrange = 0;
TBranch* b_DIGITIZER_1_channel_2_vrange = 0;
TBranch* b_DIGITIZER_1_channel_3_vrange = 0;
TBranch* b_DIGITIZER_1_channel_4_vrange = 0;
TBranch* b_DIGITIZER_1_channel_5_vrange = 0;
TBranch* b_DIGITIZER_1_channel_6_vrange = 0;
TBranch* b_DIGITIZER_1_channel_7_vrange = 0;
TBranch* b_DIGITIZER_2_channel_0_vrange = 0;
TBranch* b_DIGITIZER_2_channel_1_vrange = 0;
TBranch* b_DIGITIZER_2_channel_2_vrange = 0;
TBranch* b_DIGITIZER_2_channel_3_vrange = 0;
TBranch* b_DIGITIZER_2_channel_4_vrange = 0;
TBranch* b_DIGITIZER_2_channel_5_vrange = 0;
TBranch* b_DIGITIZER_2_channel_6_vrange = 0;
TBranch* b_DIGITIZER_2_channel_7_vrange = 0;
  // that's it  -- we don't have the CERN digitizer in AZ

TBranch* b_good_BPC_data = 0;
TBranch* b_BPC_tag = 0;
TBranch* b_BPC_runNumber = 0;
TBranch* b_BPC_spillNumber = 0;
TBranch* b_BPC_triggerNumber = 0;
TBranch* b_BPC_accum_time = 0;
TBranch* b_BPC_wall_time = 0;
TBranch* b_BPC_x[NUM_BPCS] = {NUM_BPCS*0};
TBranch* b_BPC_y[NUM_BPCS] = {NUM_BPCS*0};

// hardware wiring
char digitizerName[NUM_ADC_BOARDS][100];

// keep track of file names
char bytestream_fname[200], root_fname[200], root_simplefname[200];

int updateConditions(FCP_DAQ_Info_t* info, bool startOfRun = false);

//TBranch* b_ADC_data = 0;

// need to define the bitwise or operator to use enums as flags
inline CAEN_DGTZ_ErrorCode operator| (CAEN_DGTZ_ErrorCode a, CAEN_DGTZ_ErrorCode b)
{
  return static_cast<CAEN_DGTZ_ErrorCode>(static_cast<int>(a) | static_cast<int> (b));
}

gboolean mainDAQLoop(FCP_DAQ_Info_t* info);
CAEN_DGTZ_ErrorCode initializeDigitizers(FCP_DAQ_Info_t* info);
int openOutputFile(FCP_DAQ_Info_t* info);
int openLogFile();
int writeRunHeader(FCP_DAQ_Info_t* info);
int readoutData(FCP_DAQ_Info_t* info, int iBoard);
int writeData(FCP_DAQ_Info_t* info);
int writeEndOfSpillRecord(FCP_DAQ_Info_t* info);
void quitProgram(FCP_DAQ_Info_t* info);
int openErrorWindow(FCP_DAQ_Info_t* info, const char *message, int severity = 0);
CAEN_DGTZ_ErrorCode resetShCalDCoffsets(FCP_DAQ_Config_t* FCP_DAQcfg, unsigned value);
void addError(FCP_DAQ_Run_t* run, int bdNum, ERROR_CODES code);
static void writeToSocket(const char* msg);
float freeDiskSpace();

TFile *f_root;
//TTree* t_run;
//TTree* t_spill;
TTree* t_event = 0;

/* ###########################################################################
 *  Functions
 *  ########################################################################### */

/* open a socket for comunicating with the arduino
 */
int sockfd = -1;


int printToLogfile(const char* string) {
  if (g_logfile == NULL) {
    printf("Error: log file not available\n");
    return -1;
  }
  
  time_t rawtime;
  time(&rawtime);
  fprintf(g_logfile, "%s %s\n", ctime(&rawtime), string);

  return 0;
}

int openSocket(FCP_DAQ_Info_t* info) {
  /* fd for the socket */
  //printf("sockfd = %d\n", sockfd);
  if (sockfd > -1) {
    shutdown(sockfd, SHUT_RDWR);
    close(sockfd);
    //printf("Hey I closed it...\n");
  }

  sockfd = socket(AF_INET,      /* versus AF_LOCAL */
		  SOCK_STREAM,  /* reliable, bidirectional */
		  0);           /* system picks protocol (TCP) */
  if (sockfd < 0) {
    printf("Error opening socket\n");
    printToLogfile("Error opening socket");
    sprintf(info->beam_status,"Unknown");
    return -1;
  }

  /* get the address of the host */
  struct hostent* hptr = gethostbyname("127.0.0.1"); /* localhost: 127.0.0.1 */
  //struct hostent* hptr = gethostbyname("atlng01.physics.arizona.edu");
  if (!hptr) {
    printf("Error getting host\n");
    printToLogfile("Error getting host");
    sprintf(info->beam_status,"Unknown");
    return -1;
  }

  if (hptr->h_addrtype != AF_INET)  {     /* versus AF_LOCAL */
    printf("Error: bad address family\n");
    printToLogfile("Error: bad address family");
    sprintf(info->beam_status,"Unknown");
    return -1;
  }

  /* connect to the server: configure server's address 1st */
  struct sockaddr_in saddr;
  memset(&saddr, 0, sizeof(saddr));
  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr =
    ((struct in_addr*) hptr->h_addr_list[0])->s_addr;
  saddr.sin_port = htons(PortNumber); /* port number in big-endian */

  int err = connect(sockfd, (struct sockaddr*) &saddr, sizeof(saddr));
  if (err  < 0) {
    //    perror("Error: ");
    //    printf("Error connecting to socket (error = %d).  Setting beam status to Unknown.\n", err);
    sprintf(info->beam_status,"Unknown");
    return -1;
  }

  return 0;
}

void printFCP_DAQcfg(FCP_DAQ_Config_t FCP_DAQcfg) {
  printf("LinkType = %d\n", FCP_DAQcfg.LinkType);
  printf("LinkNum = %d\n", FCP_DAQcfg.LinkNum);
  printf("ConetNode = %d\n", FCP_DAQcfg.ConetNode);
  printf("Nch = %d\n", FCP_DAQcfg.Nch);
  printf("Nbit = %d\n", FCP_DAQcfg.Nbit);
  printf("Ts = %f\n", FCP_DAQcfg.Ts);
  printf("NumEvents = %d\n", FCP_DAQcfg.NumEvents);
  printf("RecordLength = %d\n", FCP_DAQcfg.RecordLength);
  printf("PostTrigger = %d\n", FCP_DAQcfg.PostTrigger);
  printf("TestPattern = %x\n", FCP_DAQcfg.TestPattern);
  //int TriggerEdge;
  printf("FPIOtype = %d\n", FCP_DAQcfg.FPIOtype);
  int iBoard;
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    printf("ExtTriggerMode[%d] = %d\n", iBoard, FCP_DAQcfg.ExtTriggerMode[iBoard]);
    printf("EnableMask = %x\n", FCP_DAQcfg.EnableMask[iBoard]);  
    int iChan;
    for (iChan = 0; iChan < NUM_ADC_CHANNELS; iChan++) {
      printf("ChannelTriggerMode[%d][%d] = %d\n", iBoard, iChan, FCP_DAQcfg.ChannelTriggerMode[iBoard][iChan]);
      printf("PulsePolarity[%d][%d] = %d\n", iBoard, iChan, FCP_DAQcfg.PulsePolarity[iBoard][iChan]);
      printf("DCoffset[%d][%d] = %d\n", iBoard, iChan, FCP_DAQcfg.DCoffset[iBoard][iChan]);
      printf("Vrange[%d][%d] = %5.2f\n", iBoard, iChan, FCP_DAQcfg.Vrange[iBoard][iChan]);
    }
  }
  
  printf("GWn = %d\n", FCP_DAQcfg.GWn);
  int iWrite;
  for (iWrite = 0; iWrite < FCP_DAQcfg.GWn; iWrite++) {
    printf("GWaddr[%d] = %x\n", iWrite, FCP_DAQcfg.GWaddr[iWrite]);
    printf("GWdata[%d] = %x\n", iWrite, FCP_DAQcfg.GWdata[iWrite]);
    printf("GWmask[%d] = %x\n", iWrite, FCP_DAQcfg.GWmask[iWrite]);
  }
  
}

/*! \fn      CAEN_DGTZ_ErrorCode WriteRegisterBitmask(int32_t handle, uint32_t address, uint32_t data, uint32_t mask)
 *   \brief   writes 'data' on register at 'address' using 'mask' as bitmask
 *
 *   \param   handle :   Digitizer handle
 *   \param   address:   Address of the Register to write
 *   \param   data   :   Data to Write on the Register
 *   \param   mask   :   Bitmask to use for data masking
 *   \return  0 = Success; negative numbers are error codes
 */
CAEN_DGTZ_ErrorCode WriteRegisterBitmask(int32_t handle, uint32_t address, uint32_t data, uint32_t mask) {

  // mask is the set of digitizers to send the command to
  if (mask & (0x1 << handle)) {
    CAEN_DGTZ_ErrorCode ret = CAEN_DGTZ_Success;
    uint32_t d32 = 0xFFFFFFFF;
    uint32_t datamask = 0xFFFFFFFF;
    
    ret = CAEN_DGTZ_ReadRegister(handle, address, &d32);
    if(ret != CAEN_DGTZ_Success)
      return ret;
    
    
    data &= datamask;
    d32 &= ~datamask;
    d32 |= data;
    ret = CAEN_DGTZ_WriteRegister(handle, address, d32);
    //    printf("Register write: handle %d, address %x, data %x\n", handle, address, d32);
    return ret;
  } else {
    return  CAEN_DGTZ_Success;
  }
}


/*! \fn      CAEN_DGTZ_ResetTimeStamp()
 *   \brief   writes 'data' on register at 'address' using 'mask' as bitmask
 *
 *   \param   handle :   Digitizer handle
 *
 *   \return  Error code
 */
CAEN_DGTZ_ErrorCode ResetTimeStamp(int32_t handle) {
  //printf("Resetting trigger time tag\n");
  CAEN_DGTZ_ErrorCode ret = CAEN_DGTZ_Success;
  uint32_t d32 = 0xFFFFFFFF;
  uint32_t mask = 0xFFFFFFFF;

  ret = WriteRegisterBitmask(handle , 0xEF28, d32, mask);

  return ret;
}

static CAEN_DGTZ_ErrorCode CheckDigitizerFailureStatus(FCP_DAQ_Info_t* info, int handle) {

  CAEN_DGTZ_ErrorCode ret;
  uint32_t status = 0;
  ret = CAEN_DGTZ_ReadRegister(handle, 0x8178, &status);
  if (ret != CAEN_DGTZ_Success) {
    printf("Error: Unable to read digitizer failure status.\n");
    printToLogfile("Error: Unable to read digitizer failure status.\n");
    return ret;
  }
#ifdef _WIN32
  Sleep(200);
#else
  usleep(200000);
#endif
  //read twice (first read clears the previous status)
  ret = CAEN_DGTZ_ReadRegister(handle, 0x8178, &status);
  if (ret != CAEN_DGTZ_Success) {
    printf("Error: Unable to read digitizer failure status.\n");
    printToLogfile("Error: Unable to read digitizer failure status.\n");
    return ret;
  }

  if(status & (1 << 4)) {
    printf("Digitizer error detected: PLL not locked.\n");
    printToLogfile("Digitizer error detected: PLL not locked.\n");
    addError(info->FCP_DAQrun, handle, ERR_PLL_LOCK);
    return CAEN_DGTZ_DigitizerNotReady;
  }

  if (status  & (1 << 5)) {
    printf("Digitizer over temperature detected.\n");
    printToLogfile("Digitizer over temperature detected.\n");
    addError(info->FCP_DAQrun, handle, ERR_OVERTEMP);
    return CAEN_DGTZ_DigitizerNotReady;
  }

  if (status  & (1 << 6)) {
    printf("Digitizer ADC power is down.\n");
    printToLogfile("Digitizer ADC power is down.\n");
    addError(info->FCP_DAQrun, handle, ERR_ADC);
    return CAEN_DGTZ_DigitizerNotReady; 
  }

  
  return ret;
}

/*! \fn      int SetupConfiguration(FCP_DAQ_Config_t *FCP_DAQcfg)
 *   \brief   Set configuration parameters for the digitizer (ProgramDigitizer
 *            does the actual writing to registers etc)
 *
 *   \param   FCP_DAQcfg:   FCP_DAQ_Config data structure
 *   \return  0 = Success; negative numbers are error codes
 */
int SetupConfiguration(FCP_DAQ_Config_t *FCP_DAQcfg) {
  int iChan;
  int iBoard;

  FCP_DAQcfg->Nbit = 14; 
  FCP_DAQcfg->Ts = 2.0; 
  FCP_DAQcfg->Nch = 8;
  FCP_DAQcfg->LinkType = CAEN_DGTZ_USB;
  FCP_DAQcfg->ConetNode = 0;
  FCP_DAQcfg->RecordLength = 2000;
  FCP_DAQcfg->PostTrigger = 50;
  //  FCP_DAQcfg->NumEvents = 1023;
  FCP_DAQcfg->NumEvents = 1024;

  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    FCP_DAQcfg->EnableMask[iBoard] = 0xff;  // all channels
  }
  FCP_DAQcfg->GWn = 0; 
    
 
  for(iChan=0; iChan<NUM_ADC_CHANNELS; iChan++) {
    for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
      if ( FCP_DAQcfg->EnableMask[iBoard] & (0x1 << iChan)) {
	FCP_DAQcfg->PulsePolarity[iBoard][iChan] = CAEN_DGTZ_PulsePolarityNegative;
	if (use_ext_trigger == 0) {
	  FCP_DAQcfg->ChannelTriggerMode[iBoard][iChan] = CAEN_DGTZ_TRGMODE_ACQ_ONLY;
	  //FCP_DAQcfg->ChannelTriggerMode[iBoard][iChan] = CAEN_DGTZ_TRGMODE_ACQ_AND_EXTOUT;

	} else {
	  FCP_DAQcfg->ChannelTriggerMode[iBoard][iChan] = CAEN_DGTZ_TRGMODE_DISABLED;
	}
      }
      FCP_DAQcfg->DCoffset[iBoard][iChan] = 0x8000; // half scale
    }
  }

  if (teststand_mode == 0) {
    //default assuming positive HV polarity to ShCal
    FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_1_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_3_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_4_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_1_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_2_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_3_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_4_CHANNEL] = ShCalPed_HV_pos;
    
    FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_1_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_2_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_3_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_4_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_1_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_2_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_3_CHANNEL] = ShCalPed_HV_pos;
    FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_4_CHANNEL] = ShCalPed_HV_pos;

    // high pedestal for scintillators and trigger signals
    uint16_t trigScintOffset = 0x2000;
    FCP_DAQcfg->DCoffset[S1_COUNTER_DIGITIZER_HANDLE][S1_COUNTER_CHANNEL] = trigScintOffset;
    FCP_DAQcfg->DCoffset[S2_COUNTER_DIGITIZER_HANDLE][S2_COUNTER_CHANNEL] = trigScintOffset;  
    FCP_DAQcfg->DCoffset[S3_COUNTER_DIGITIZER_HANDLE][S3_COUNTER_CHANNEL] = trigScintOffset;  
    FCP_DAQcfg->DCoffset[S4_COUNTER_DIGITIZER_HANDLE][S4_COUNTER_CHANNEL] = trigScintOffset;      
    FCP_DAQcfg->DCoffset[HALO_COUNTER_DIGITIZER_HANDLE][HALO_COUNTER_CHANNEL] = trigScintOffset;  
    FCP_DAQcfg->DCoffset[LEAKAGE_COUNTER_DIGITIZER_HANDLE][LEAKAGE_COUNTER_CHANNEL] = trigScintOffset;  
    FCP_DAQcfg->DCoffset[DIGITIZER_1_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_1_TRIGGER_CHANNEL] = trigScintOffset;  
    FCP_DAQcfg->DCoffset[DIGITIZER_2_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_2_TRIGGER_CHANNEL] = trigScintOffset; 
    FCP_DAQcfg->DCoffset[DIGITIZER_3_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_3_TRIGGER_CHANNEL] = trigScintOffset; 
    FCP_DAQcfg->DCoffset[DIGITIZER_4_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_4_TRIGGER_CHANNEL] = trigScintOffset; 
  } else {
    // any AZ teststand-specific configurations go here
    FCP_DAQcfg->DCoffset[DIGITIZER_1_HANDLE][7] = 0x2000;
    FCP_DAQcfg->DCoffset[DIGITIZER_2_HANDLE][7] =0x2000;
  }


  // DT5730-specific setup that involves writing to registers
  // these set  0.5V pp mode
  for(iChan=0; iChan<NUM_ADC_CHANNELS; iChan++) {
    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (iChan << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 1;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0xf;  // all four digitizers
    FCP_DAQcfg->GWn++;

    for (int iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++){
      FCP_DAQcfg->Vrange[iBoard][iChan] = 0.5;
    }
  }
  
 
  //here we set the digitizer channels used for the scintillators to 2V pp
  if (teststand_mode == 0) {
    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (S1_COUNTER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << S1_COUNTER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[S1_COUNTER_DIGITIZER_HANDLE][S1_COUNTER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (S2_COUNTER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << S2_COUNTER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[S2_COUNTER_DIGITIZER_HANDLE][S2_COUNTER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (S3_COUNTER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << S3_COUNTER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[S3_COUNTER_DIGITIZER_HANDLE][S3_COUNTER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (S4_COUNTER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << S4_COUNTER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[S4_COUNTER_DIGITIZER_HANDLE][S4_COUNTER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (LEAKAGE_COUNTER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << LEAKAGE_COUNTER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[LEAKAGE_COUNTER_DIGITIZER_HANDLE][LEAKAGE_COUNTER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (HALO_COUNTER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << HALO_COUNTER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[HALO_COUNTER_DIGITIZER_HANDLE][HALO_COUNTER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (DIGITIZER_1_TRIGGER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << DIGITIZER_1_TRIGGER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[DIGITIZER_1_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_1_TRIGGER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (DIGITIZER_2_TRIGGER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << DIGITIZER_2_TRIGGER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[DIGITIZER_2_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_2_TRIGGER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (DIGITIZER_3_TRIGGER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << DIGITIZER_3_TRIGGER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[DIGITIZER_3_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_3_TRIGGER_CHANNEL] = 2.0;

    if (FCP_DAQcfg->GWn == NUM_GW) {
      printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
    }
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (DIGITIZER_4_TRIGGER_CHANNEL << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << DIGITIZER_4_TRIGGER_DIGITIZER_HANDLE; 
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[DIGITIZER_4_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_4_TRIGGER_CHANNEL] = 2.0;
 
  } else {
    //in the teststand the trigger pulse is sent to channel 7 of each digitizer
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (7 << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << DIGITIZER_1_HANDLE;
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x1028 + (7 << 8);
    FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0;
    FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0x1 << DIGITIZER_2_HANDLE;
    FCP_DAQcfg->GWn++;
    FCP_DAQcfg->Vrange[DIGITIZER_1_HANDLE][7] = 2.0;
    FCP_DAQcfg->Vrange[DIGITIZER_2_HANDLE][7] = 2.0;
  }

  // to enable external clock
  if (FCP_DAQcfg->GWn == NUM_GW) {
    printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
  }
  FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x8100;
  FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0x1 << 6;  // 0x1 for external clock
  FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0xf; 
  FCP_DAQcfg->GWn++; 

  // to propagate BUSY on GP0
 if (FCP_DAQcfg->GWn == NUM_GW) {
    printf("Error:  trying to set up %d register writes, but only %d allowed.  Please modify parameter NUM_GW in FCP_DAQ.h\n", FCP_DAQcfg->GWn+1, NUM_GW);
  }
  FCP_DAQcfg->GWaddr[FCP_DAQcfg->GWn] = 0x811c;
  FCP_DAQcfg->GWdata[FCP_DAQcfg->GWn] = 0xD << 16; 
  FCP_DAQcfg->GWmask[FCP_DAQcfg->GWn] = 0xf; 
  FCP_DAQcfg->GWn++; 

 

  if (use_ext_trigger == 0) {
    for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
      FCP_DAQcfg->ExtTriggerMode[iBoard] = CAEN_DGTZ_TRGMODE_DISABLED;
    }
  } else {
   for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
     FCP_DAQcfg->ExtTriggerMode[iBoard] = CAEN_DGTZ_TRGMODE_ACQ_AND_EXTOUT;
   }
  }

  FCP_DAQcfg->TestPattern = 0;
  FCP_DAQcfg->FPIOtype = (CAEN_DGTZ_IOLevel_t) 0;   //NIM (1 = TTL)
                                                    // but may be overwritten
                                                    // by register 0x811c

  return 0;
}

CAEN_DGTZ_ErrorCode resetShCalDCoffsets(FCP_DAQ_Config_t* FCP_DAQcfg, unsigned value) {

  CAEN_DGTZ_ErrorCode ret = CAEN_DGTZ_Success;

  FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_1_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_3_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_4_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_1_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_2_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_3_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_4_CHANNEL] = value;

  FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_1_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_2_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_3_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_4_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_1_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_2_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_3_CHANNEL] = value;
  FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_4_CHANNEL] = value;

  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_HANDLE, SHCAL_FRONT_SOURCE_QUAD_1_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_1_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE, SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_HANDLE, SHCAL_FRONT_SOURCE_QUAD_3_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_3_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_HANDLE, SHCAL_FRONT_SOURCE_QUAD_4_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_4_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_HANDLE, SHCAL_REAR_SOURCE_QUAD_1_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_1_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_HANDLE, SHCAL_REAR_SOURCE_QUAD_2_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_2_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_HANDLE, SHCAL_REAR_SOURCE_QUAD_3_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_3_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_HANDLE, SHCAL_REAR_SOURCE_QUAD_4_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_4_CHANNEL]);

  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_HANDLE, SHCAL_FRONT_NOSOURCE_QUAD_1_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_1_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_HANDLE, SHCAL_FRONT_NOSOURCE_QUAD_2_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_2_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_HANDLE, SHCAL_FRONT_NOSOURCE_QUAD_3_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_3_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_HANDLE, SHCAL_FRONT_NOSOURCE_QUAD_4_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_4_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_HANDLE, SHCAL_REAR_NOSOURCE_QUAD_1_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_1_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_HANDLE, SHCAL_REAR_NOSOURCE_QUAD_2_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_2_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_HANDLE, SHCAL_REAR_NOSOURCE_QUAD_3_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_3_CHANNEL]);
  ret = ret | CAEN_DGTZ_SetChannelDCOffset(SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_HANDLE, SHCAL_REAR_NOSOURCE_QUAD_4_CHANNEL, FCP_DAQcfg->DCoffset[SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_4_CHANNEL]);

  return ret;
}
/*! \fn      CAEN_DGTZ_ErrorCode ProgramDigitizer(int iBoard, int handle, 
 *                 FCP_DAQ_Config_t FCP_DAQcfg)
 *   \brief   configure the digitizer according to the parameters read from
 *            the configuration file and saved in the FCP_DAQcfg data structure
 *
 *   \param   handle   Digitizer handle
 *   \param   FCP_DAQcfg:   FCP_DAQ_Config data structure
 *   \return  0 = Success; negative numbers are error codes
 */
CAEN_DGTZ_ErrorCode ProgramDigitizer(int iBoard, int handle, FCP_DAQ_Config_t FCP_DAQcfg, CAEN_DGTZ_BoardInfo_t BoardInfo)
{
  // printFCP_DAQcfg(FCP_DAQcfg);

  int iChan, iWrite = 0;
  CAEN_DGTZ_ErrorCode ret = CAEN_DGTZ_Success;
  char buf[200];

  /* reset the digitizer */
  ret = ret | CAEN_DGTZ_Reset(handle);
  sprintf(buf, "Digitizer %d reset with return code %d", handle, ret);
  printToLogfile(buf);
  if (ret != CAEN_DGTZ_Success) {
    printf("Error: Unable to reset digitizer.\nPlease reset digitizer manually then restart the program\n");
    printToLogfile("Error: Unable to reset digitizer.\nPlease reset digitizer manually then restart the program");
    return ret;
  }

 // This is a strange one: it seems that the clock mode has to be set to
  // external *before* the call to SetMaxNumEventsBLT, with some delay added,
  // or else the digitizers go into a strange state where further communications
  // lead to errors
  ret = ret | WriteRegisterBitmask(handle, 0x8100, 0x40, 0xf);
  if (ret != CAEN_DGTZ_Success) {
    sprintf(buf, "Error: Unable to set digitizer %d to use external clock; error code is %d.\n", handle, ret);
    printToLogfile(buf);
    return ret;
  }

  usleep(250000);


  ret = ret | CAEN_DGTZ_Calibrate(handle);
  if (ret != CAEN_DGTZ_Success) {
    sprintf(buf, "Error: Unable to calibrate digitizer %d; error code is %d.\n", handle, ret);
    printToLogfile(buf);
    return ret;
  }
  else {
    sprintf(buf, "Digitizer %d ADC calibration is successful.\n", handle);
    printToLogfile(buf);
  }


  // Set the waveform test bit for debugging
  if (FCP_DAQcfg.TestPattern) {
    ret = ret | CAEN_DGTZ_WriteRegister(handle, CAEN_DGTZ_BROAD_CH_CONFIGBIT_SET_ADD, 1<<3);
    sprintf(buf, "Digitizer %d test pattern sent, return code is %d\n", handle, ret);
    printToLogfile(buf);
  }
  ret = ret | CAEN_DGTZ_SetRecordLength(handle, FCP_DAQcfg.RecordLength);
  sprintf(buf, "Digitizer %d record length set to %d, return code is %d\n", handle, FCP_DAQcfg.RecordLength, ret);
  printToLogfile(buf);

  uint32_t rl;
  ret = ret | CAEN_DGTZ_GetRecordLength(handle, &rl);
  if (rl != FCP_DAQcfg.RecordLength) {
    printf("Warning: in digitizer %d, record length of %d requested, but %d actually set", handle, FCP_DAQcfg.RecordLength, rl);
  }
  FCP_DAQcfg.RecordLength = rl;
  sprintf(buf, "Digitizer %d record length read, return code is %d\n", handle, ret);
  printToLogfile(buf);

  ret = ret | CAEN_DGTZ_SetPostTriggerSize(handle, FCP_DAQcfg.PostTrigger);
  sprintf(buf, "Digitizer %d post trigger size set to %d, return code is %d\n", handle, FCP_DAQcfg.PostTrigger, ret);
  printToLogfile(buf);
  uint32_t pt;
  ret = ret | CAEN_DGTZ_GetPostTriggerSize(handle, &pt);
  if (pt != FCP_DAQcfg.PostTrigger) {
    printf("Warning: in digitizer %d, post trigger size of %d requested, but %d actually set", handle, FCP_DAQcfg.PostTrigger, pt);
  }

  FCP_DAQcfg.PostTrigger = pt;


  ret = ret | CAEN_DGTZ_SetIOLevel(handle, FCP_DAQcfg.FPIOtype);
  sprintf(buf, "Digitizer %d IO level set to %d, return code is %d\n", handle, FCP_DAQcfg.FPIOtype, ret);
  printToLogfile(buf);

  ret = ret | CAEN_DGTZ_SetMaxNumEventsBLT(handle, FCP_DAQcfg.NumEvents);
  sprintf(buf, "Digitizer %d max num events set to %d, return code is %d\n", handle, FCP_DAQcfg.NumEvents, ret);
  printToLogfile(buf);

	
  ret = ret |CAEN_DGTZ_SetAcquisitionMode(handle, CAEN_DGTZ_SW_CONTROLLED);
  sprintf(buf, "Digitizer %d acquisition mode set to %d, return code is %d\n", handle, CAEN_DGTZ_SW_CONTROLLED, ret);
  printToLogfile(buf);

  // below is possibly fragile -- will handle always enumerate digitizers?
  ret = ret | CAEN_DGTZ_SetExtTriggerInputMode(handle, FCP_DAQcfg.ExtTriggerMode[handle]);
  sprintf(buf, "Digitizer %d trigger input mode set to %d, return code is %d\n", handle, FCP_DAQcfg.ExtTriggerMode[handle], ret);
  printToLogfile(buf);

  ret = ret | CAEN_DGTZ_SetChannelEnableMask(handle, FCP_DAQcfg.EnableMask[iBoard]);
  sprintf(buf, "Digitizer %d enable mask set to %x, return code is %d\n", handle, FCP_DAQcfg.EnableMask[iBoard], ret);
  printToLogfile(buf);

  for (iChan = 0; iChan < FCP_DAQcfg.Nch; iChan++) {
    if (FCP_DAQcfg.EnableMask[iBoard] & (1<<iChan)) {
      ret = ret | CAEN_DGTZ_SetChannelDCOffset(handle, iChan, FCP_DAQcfg.DCoffset[iBoard][iChan]);
      sprintf(buf, "Digitizer %d channel %d, DC offset set to %d, return code is %d\n", handle, iChan, FCP_DAQcfg.DCoffset[iBoard][iChan], ret);
      printToLogfile(buf);
    }
  }

  // channel pair settings for x730 boards
  for (iChan = 0; iChan < FCP_DAQcfg.Nch; iChan += 2) {
    if (FCP_DAQcfg.EnableMask[iBoard] & (0x3 << iChan)) {
      CAEN_DGTZ_TriggerMode_t mode = FCP_DAQcfg.ChannelTriggerMode[iBoard][iChan];
      uint32_t pair_chmask = 0;
      
      // Build mode and relevant channelmask. The behaviour is that,
      // if the triggermode of one channel of the pair is DISABLED,
      // this channel doesn't take part to the trigger generation.
      // Otherwise, if both are different from DISABLED, the one of
      // the even channel is used.
      if (FCP_DAQcfg.ChannelTriggerMode[iBoard][iChan] != CAEN_DGTZ_TRGMODE_DISABLED) {
	if (FCP_DAQcfg.ChannelTriggerMode[iBoard][iChan + 1] == CAEN_DGTZ_TRGMODE_DISABLED)
	  pair_chmask = (0x1 << iChan);
	else
	  pair_chmask = (0x3 << iChan);
      }
      else {
	mode = FCP_DAQcfg.ChannelTriggerMode[iBoard][iChan + 1];
	pair_chmask = (0x2 << iChan);
      }
      
      pair_chmask &= FCP_DAQcfg.EnableMask[iBoard];
 
      ret = ret | CAEN_DGTZ_SetChannelSelfTrigger(handle, mode, pair_chmask);
      sprintf(buf, "Digitizer %d channel pair %d, trigger mode set to %d, return code is %d\n", handle, pair_chmask, mode, ret);
      printToLogfile(buf);
    }
  }

  // MAYBE REMOVE THIS?
  for (iChan = 0; iChan < FCP_DAQcfg.Nch; iChan ++) {
    ret = ret | CAEN_DGTZ_SetChannelTriggerThreshold(handle, iChan, channel_trigger_threshold);
    sprintf(buf, "Digitizer %d channel %d, self trigger threshold set to %d, return code is %d\n", handle, iChan, channel_trigger_threshold, ret);
    printToLogfile(buf);
  }

  /* execute generic write commands */
  for(iWrite=0; iWrite<FCP_DAQcfg.GWn; iWrite++) {
    //printf("%d %x %x %x\n", iWrite, FCP_DAQcfg.GWaddr[iWrite],  FCP_DAQcfg.GWdata[iWrite],  FCP_DAQcfg.GWmask[iWrite]);
    // don't set up external clock
    //if (FCP_DAQcfg.GWaddr[iWrite] == 0x8100) continue;
    ret = ret | WriteRegisterBitmask(handle, FCP_DAQcfg.GWaddr[iWrite], FCP_DAQcfg.GWdata[iWrite], FCP_DAQcfg.GWmask[iWrite]);
    sprintf(buf, "Register write:  handle %d, address %x, data %x, mask %x, return code %d\n", handle, FCP_DAQcfg.GWaddr[iWrite], FCP_DAQcfg.GWdata[iWrite], FCP_DAQcfg.GWmask[iWrite], ret);
    printToLogfile(buf);
    // give time for digitizer to settle down after enabling external clock
    if (FCP_DAQcfg.GWaddr[iWrite] == 0x8100) usleep(250000);
  }
 
  // reset timer
  ret = ret | CAEN_DGTZ_WriteRegister(handle,  0xEF28, 0x1);
  sprintf(buf, "Digitizer %d timer reset, return code is %d\n", handle, ret);
  printToLogfile(buf);

  return ret;
}


static gboolean startDAQLoop(gpointer gp) {
  
  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;
  //  if (strstr(info->beam_status, "spill") == NULL) {
  //  if ((info->FCP_DAQrun->PauseRun == 1) || (info->FCP_DAQrun->EndRun == 1)) {
  //     printf("killing the DAQ loop thread\n");
  //    return FALSE;
  //  }
  // }

  return mainDAQLoop(info);
}

void *analysisLoop(void *arg) {

  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)arg;
  CAEN_DGTZ_ErrorCode ret = CAEN_DGTZ_Success;

  float summedPulseHeight[NUM_ADC_BOARDS*NUM_ADC_CHANNELS] = {0.*NUM_ADC_BOARDS*NUM_ADC_CHANNELS};

  // create window for displaying pulse heights
  FILE* gnuplot = popen("gnuplot", "w");
  if (gnuplot != NULL) {
    fprintf(gnuplot, "set boxwidth 0.5\n");
    fprintf(gnuplot, "set style fill solid\n");
    fprintf(gnuplot, "set terminal x11 0 position 500,500\n");
    fprintf(gnuplot, "set xlabel \"channel\"\n");
    fprintf(gnuplot, "set ylabel \"summed pulse height (ADC counts)\"\n");    
  }
  // create file for storing pulse heights to be used by gnuplot
  FILE* pulseheightdata = fopen("pulseHeights.dat", "w");
  
  fflush(gnuplot);

  while (1) {
    sleep(1);
    if (info->FCP_DAQrun->NewDataToAnalyze == 1) { // start analysis!
      uint32_t NumEvents;
      int iBoard;
      
      if (info->FCP_DAQrun->NewDataToAnalyze != 0) {
	info->FCP_DAQrun->DataCopyInProgress = 1;
	for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
	  size_t bufSize = info->BufferSize_ADC[iBoard];
	  if (memcpy(info->analysisBuffer_ADC[iBoard], info->buffer_ADC[iBoard], bufSize) == NULL) {
	    printf("Error copying data into analysis buffer\n");
	    printToLogfile("Error copying data into analysis buffer\n");
	    //      return FALSE;
	  }
	  info->FCP_DAQrun->DataCopyInProgress = 0;
	  if (bufSize != 0) {
	    ret = CAEN_DGTZ_GetNumEvents(iBoard, info->analysisBuffer_ADC[iBoard], bufSize, &NumEvents);
	    if (ret != 0) {
	      printf("Error getting number of events\n");
	      printToLogfile("Error getting number of events");
	      //	return ret;
	    }
	  }
	}
      }

      int ichan;
      for (ichan = 0; ichan < NUM_ADC_BOARDS*NUM_ADC_CHANNELS; ichan++) {
	summedPulseHeight[ichan] = 0.;
      }

      // use samples well before the trigger to calculate pedestal
      uint32_t nPedSamples = info->FCP_DAQcfg->RecordLength*(100-info->FCP_DAQcfg->PostTrigger)/200;
      // printf("Using %d samples for pedestal\n", nPedSamples);
      char buf[200];
      sprintf(buf, "Using %d samples for pedestal", nPedSamples);
      printToLogfile(buf);
      if (nPedSamples < 100) {
	printf("Warning: only %d samples available for calculating pedestal.  Values may not be accurate\n", nPedSamples);
	sprintf(buf, "Warning: only %d samples available for calculating pedestal.  Values may not be accurate\n", nPedSamples);
	printToLogfile(buf);
      }
      
      uint32_t iEvt;
      for (iEvt = 0; iEvt < NumEvents; iEvt++) {
	
	for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
	  char* EventPtr = NULL;
	  CAEN_DGTZ_EventInfo_t       EventInfo;
	  
	  ret = CAEN_DGTZ_GetEventInfo(iBoard, info->analysisBuffer_ADC[iBoard], info->BufferSize_ADC[iBoard], iEvt, &EventInfo, &EventPtr); 
	  if (ret != 0) {
	    printf("Error getting event %d of %d from board %d: %d\n", iEvt, NumEvents, iBoard, ret);
	    char buf[200];
	    sprintf(buf, "Error getting event %d od %d from board %d: %d", iEvt, NumEvents, iBoard, ret);
	    printToLogfile(buf);
	    //	return ret;
	  }
	  ret = CAEN_DGTZ_DecodeEvent(iBoard, EventPtr, (void**)&info->Event16[iBoard]);
	  if (ret != 0) {
	    printf("Error decoding event\n");
	    printToLogfile("Error decoding event");
	    //	return ret;
	  }
	  int ch;
	  for (ch = 0; ch < info->FCP_DAQcfg->Nch; ch++) {
	    int Size = info->Event16[iBoard]->ChSize[ch];
	    if (Size <= 0) {
	      continue;
	    }
	    
	    uint32_t iSamp;
	    double sum = 0, sumsq = 0;
	    float ped, pedErr;
	    for (iSamp = 0; iSamp < nPedSamples; iSamp++) {
	      uint16_t dat =  info->Event16[iBoard]->DataChannel[ch][iSamp];
	      sum += dat;
	      sumsq += dat*dat;
	    }
	    ped  = sum/nPedSamples;
	    pedErr = sqrt((sumsq - sum*sum/nPedSamples)/nPedSamples);
	  
	    if (iEvt == 0) {
	      //printf("Pedestal for channel %d is %6.2f +/- %6.2f\n", ch + (iBoard << 3), ped, pedErr);
	      char buf[200];
	      sprintf(buf, "Pedestal for channel %d is %6.2f +/- %6.2f\n", ch + (iBoard << 3), ped, pedErr);
	      printToLogfile(buf);
	    }

	    //now that pedestals are available, calculate pulse heights
	    rewind(pulseheightdata);

	    for (iSamp = nPedSamples; iSamp < info->FCP_DAQcfg->RecordLength; iSamp++) {
	      uint16_t dat =  info->Event16[iBoard]->DataChannel[ch][iSamp];
	      if (fabs((dat-ped)/pedErr) > 5.) {
		summedPulseHeight[ch+(iBoard<<3)] += dat-ped;	
	      }
	    }
	  }
	}
      

	
      }
      
      for (ichan = 0; ichan < NUM_ADC_BOARDS*NUM_ADC_CHANNELS; ichan++) {
	//printf("Summed pulse height for channel %d is %6.2f\n", ichan, summedPulseHeight[ichan]);
	char buf[200];
	sprintf(buf, "Summed pulse height for channel %d is %6.2f\n", ichan, summedPulseHeight[ichan]);
	printToLogfile(buf);
	fprintf(pulseheightdata, "%d \"channel %d\" %f\n", ichan, ichan, summedPulseHeight[ichan]); 
      }
      fflush(pulseheightdata);
      fprintf(gnuplot, "plot 'pulseHeights.dat' using 1:3 with boxes  title \"\"\n");
      fflush(gnuplot);
      
      info->FCP_DAQrun->NewDataToAnalyze = 0;
    }
  //  return TRUE;
  }

  return 0;
}

void addError(FCP_DAQ_Run_t* run, int bdNum, ERROR_CODES code) {
  uint16_t errId = (bdNum << 8) + code;
  int iErr;
  int newErr = 1;
  for (iErr = 0; iErr < run->nErrors; iErr++ ) {
    if (errId == run->errors[iErr].id) {
      newErr = 0;
      break;
    }
  }
  
  char msg[200];
  if (bdNum > -1) {
    sprintf(msg, "Digitizer %s: %s", digitizerName[bdNum], ErrMsg[code]);
  } else {
    sprintf(msg, "%s", ErrMsg[code]);
  }
  if (newErr) {
    run->errors[run->nErrors].id = errId;
    sprintf(run->errors[run->nErrors].msg, "%s", msg);
    run->errors[run->nErrors].isAcknowledged = 0;
    run->nErrors++;
  }
}

static void writeToSocket(const char* msg) {
  //  printf("In writeToSocket rying to write %s (%d bytes) to sockfd %d\n", msg, strlen(msg), sockfd);
  char buf[200];
  sprintf(buf, "%s\n", msg);
  if (write(sockfd, buf, strlen(buf)) < 0) {
    printf("Error writing %s to socket\n", msg);
    sprintf(buf, "Error writing %s to socket\n", msg);
    printToLogfile(buf);
  } else {
    // write was OK, so reset heartbeat timeout to help avoid race condition
    time(&g_last_socket_write_time);
    sprintf(buf, "Wrote to socket: %s", msg);
    printToLogfile(buf);
  }
}


static gboolean readFromSocket(gpointer gp) {

  //printf("Reading from socket %d\n", sockfd);
  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;

  //is socket still available? Do this check periodically (set to 5 s for now)
  time_t curr_time;
  time(&curr_time);
  if (curr_time - g_last_socket_write_time > 5) {
    //    printf("Checking to see if socket is open...\n");
    //printf("Writing %s to sockfd %d\n", info->beam_status, sockfd);
    char buf[200];
    sprintf(buf, "%s\n", info->beam_status);
    if (write(sockfd, buf, strlen(buf)) < 0) {
      // hmm, try re-opening
      openSocket(info);
      //printf("sockfd is now %d\n", sockfd);
      // if that didn't work, give up for now
      if (write(sockfd, info->beam_status, strlen(info->beam_status)) < 0) {
	sprintf(info->beam_status, "Unknown");
	char labelTxt[20];
	sprintf(labelTxt, "Beam status: Unknown");
	gtk_label_set_text((GtkLabel*)info->beam_status_label, labelTxt);
	
	return TRUE;
      }
    }
    sprintf(buf, "Wrote to socket: %s", info->beam_status);
    printToLogfile(buf);
    g_last_socket_write_time = curr_time;
  }

  unsigned count;
  if (ioctl(sockfd, FIONREAD, &count) == 0) {
    if (count > 0) { // new data available!
      //printf("%d bytes to read from buffer\n", count);
      char inbuf[1024]; //make very large in case we missed some messages since the last read
      if (count > sizeof(inbuf) ) {
	char errbuf[200];
	sprintf(errbuf, "Error: read %u bytes from socket, but can only process %lu", count, sizeof(inbuf));
	//printf("%s\n", errbuf);
	printToLogfile(errbuf);
      }
      if (read(sockfd, inbuf, sizeof(inbuf)) > 0) {
	unsigned idx = 0;
    
	while (idx < count) {
	  char *substring = &inbuf[idx];
	  char buf[200];
	  sprintf(buf, "read %s from socket\n", substring);
	  printToLogfile(buf);
	  idx += strlen(substring)+1;
	  if (strstr(substring, "beam") != NULL) { // string contains beam status
	    if (strstr(substring, "spill") != NULL) {
	      if (info->FCP_DAQrun->InRun == 1) {
		info->beam_status[0] = '\0';
		char* token = strtok_r(substring, " ", &substring);
		while (token != NULL) {
		  token = strtok_r(substring, " ", &substring);
		  //printf("token is %s\n", token);
		  if (token != NULL) {
		    sprintf(info->beam_status, "%s %s", info->beam_status, token);
		    //printf("beam status is %s\n", info->beam_status);
		  }
		}
	      } else {
		sprintf(info->beam_status, "spill");
	      }
	      
	      //gtk_widget_set_sensitive (info->start, FALSE);
	      //gtk_widget_set_sensitive (info->pause, FALSE);
	      //gtk_widget_set_sensitive (info->stop, FALSE);
	      gtk_widget_show (info->start);
	      gtk_widget_show (info->stop);
	      gtk_widget_show (info->pause);

	    } else if (strstr(substring, "off") != NULL) {
	      sprintf(info->beam_status, "off");
	      // set buttons to active, as appropriate
	      if (info->FCP_DAQrun->InRun == 0 ) {
		gtk_widget_set_sensitive (info->start, TRUE);
		gtk_widget_set_sensitive (info->stop, FALSE);
		gtk_widget_set_sensitive (info->pause, FALSE);
		gtk_widget_set_sensitive (info->exit, TRUE);
	      } else if (info->FCP_DAQrun->PauseRun == 0 ) {
		gtk_widget_set_sensitive (info->start, FALSE);
		gtk_widget_set_sensitive (info->stop, TRUE);
		gtk_widget_set_sensitive (info->pause, TRUE);
		gtk_widget_set_sensitive (info->exit, FALSE);
	      } else {
		gtk_widget_set_sensitive (info->start, TRUE);
		gtk_widget_set_sensitive (info->stop, TRUE);
		gtk_widget_set_sensitive (info->pause, FALSE);
		gtk_widget_set_sensitive (info->exit, FALSE);
	      }
	      gtk_widget_show (info->start);
	      gtk_widget_show (info->stop);
	      gtk_widget_show (info->pause);
	      gtk_widget_show (info->exit);
	      
	    } else {
	      char errbuf[200];	    
	      sprintf(errbuf, "Error: unexpected message %s\n", substring);
	      printf(errbuf);
	      printToLogfile(errbuf);
	      sprintf(info->beam_status, "Unknown");
	    }
	    char labelTxt[200];
	    sprintf(labelTxt, "Beam status: %s", info->beam_status);
	    gtk_label_set_text((GtkLabel*)info->beam_status_label, labelTxt);
	  } else if (strstr(substring, "Sr90") != NULL) { // string tells whether Sr90 or control assembly is in beam
	    if (strstr(substring, "in") != NULL) {
	      if (info->FCP_DAQrun->InRun == 1 && info->FCP_DAQcfg->sourceInBeam == 0) { 
		printf("Error: Reported Sr90 position changed from out-of-beam to in-beam during run\n");
		printToLogfile("Reported Sr90 position changed from out-of-beam to in-beam during run");
		addError(info->FCP_DAQrun, 0, ERR_SR90_POSITION);
		openErrorWindow(info, "Reported Sr90 position changed from out-of-beam to in-beam during run", 1);
	      }
	      info->FCP_DAQcfg->sourceInBeam = 1;
	      gtk_label_set_text((GtkLabel*)info->assembly_in_beam_label,"Sr90 in beam:  yes");
	    } else if (strstr(substring, "out") != NULL) {
	      if (info->FCP_DAQrun->InRun == 1 && info->FCP_DAQcfg->sourceInBeam == 1) {
                printf("Error: Reported Sr90 position changed from in-beam to out-of-beam during run\n");
                printToLogfile("Reported Sr90 position changed from in-beam to out-of-beam during run");
		addError(info->FCP_DAQrun, 0, ERR_SR90_POSITION);
                openErrorWindow(info, "Reported Sr90 position changed from in-beam to out-of-beam during run", 1);
	      } 
	      info->FCP_DAQcfg->sourceInBeam = 0;
	      gtk_label_set_text((GtkLabel*)info->assembly_in_beam_label,"Sr90 in beam:  no");
	    } else {
	      char errbuf[200];	    
	      sprintf(errbuf, "Error: unexpected message %s\n", substring);
	      printf(errbuf);
	      printToLogfile(errbuf);
	    }
	  
	  } else {
	    char errbuf[200];	 
	    sprintf(errbuf, "Error: unexpected message from socket: %s\nPrevious message was %s\n", substring, info->prevSocketMessage);
	    printf(errbuf);
	    printToLogfile(errbuf);
	 
	  }
	  strcpy(info->prevSocketMessage, substring);      
	  

	}
	//input buffer should be empty now -- check!
	while (count >0) {
	  if (ioctl(sockfd, FIONREAD, &count) == 0) {
	    if (count > 0) {
	      printf("Still %d bytes in the socket buffer!\n", count);
	      read(sockfd, inbuf, sizeof(inbuf));
	    }
	  }
	}

      }
      
      /*	    
      if (read(sockfd, info->beam_status, sizeof(info->beam_status)) > 0) {
	//printf("Beam status = %s\n", info->beam_status);
	char labelTxt[20];
        sprintf(labelTxt, "Beam status: %s", info->beam_status);
	gtk_label_set_text((GtkLabel*)info->beam_status_label, labelTxt);
      } else {
	printf("Error getting beam status: wrong length\n");
	printToLogfile("Error getting beam status: wrong length");
	char labelTxt[20];
	sprintf(labelTxt, "Beam status: Unknown");
	gtk_label_set_text((GtkLabel*)info->beam_status_label, labelTxt);
      }
    } 
  } else {
    printf("Error getting beam status: no message\n");
    printToLogfile("Error getting beam status: no message");
    char labelTxt[20];
    sprintf(labelTxt, "Beam status: Unknown");
    gtk_label_set_text((GtkLabel*)info->beam_status_label, labelTxt);
  }
      */
    }
  }

  return TRUE;
}

void startRun(FCP_DAQ_Info_t *info) {

  //  info->FCP_DAQrun->AcqRun = 1;
  //info->FCP_DAQrun->EndRun = 0;

  // send start run signals to arduino and ControlHost
  const int buflen = 1024;
  char buf[buflen];
  if (info->FCP_DAQrun->PauseRun == 1) {
    sprintf(buf, "resume run %d", g_runHeader->runNumber);
    writeToSocket(buf);
    info->FCP_DAQrun->PauseRun = 0;
  } else {
    sprintf(buf, "start run %d", g_runHeader->runNumber);
    writeToSocket(buf);
    if (merge_BPC_data == 1) {
      sprintf(buf, "SoR %d",  g_runHeader->runNumber);
      int rc = put_fullstring("CMD", buf);
      if (rc < 0) {
	printf("Error writing to ControlHost: %d\n", rc);
      } else {
	char tag[TAGSIZE+1] = {"NULL"};
	int nb;
	int ntries = 0, maxtries = 500;
	int rc = 0;
	while ((ntries < maxtries) && (strcmp(tag, "ACK") != 0)) {
	  rc =check_head(tag, &nb);
	  if (rc < 0) {
	    printToLogfile("Error communicating with ControlHost when looking for SoR ACK");
	    break;
	  }
	  if (rc > 0) {
	    if (strcmp(tag, "ACK") != 0) {
	      char msgbuf[200];
	      sprintf(msgbuf, "Warning: unexpected tag when looking for EoR ACK: %s", tag);	 
	      printToLogfile(msgbuf);
	      get_string(buf, buflen-1);
	    }
	  } 
	  usleep(10000);
	  ntries++;
	}
	//      wait_head(tag, &nb);
	//      printf("ntries = %d\n", ntries);
	if (ntries >= maxtries || rc < 0) {
	  printf("No ACK received from BPC\n");
	  printToLogfile("No ACK received from BPC\n");
	} else {
	  get_string(buf, buflen-1);
	  char msgbuf[200];
	  sprintf(msgbuf, "ACK from BPC received: %s", buf);
	  printToLogfile(msgbuf);
	  printf("ACK from BPC received: %s\n", buf);
	}
      }
    }
  }

  // get ready to take data
  for (int iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    uint32_t ret;
    ret = CAEN_DGTZ_SWStartAcquisition(info->handle_ADC[iBoard]);
    if (ret != 0) {
      sprintf(buf, "Error starting data aqcuisition for handle %d: %d\n", info->handle_ADC[iBoard], ret);
      printToLogfile(buf);
    }
  }
}

/*! \fn      void GuiStart(FCP_DAQ_Run_t *FCP_DAQrun, GtkWidget *widget)
 *   \brief   start run 
 *
 *   \param   FCP_DAQrun:   Pointer to the FCP_DAQ_Run_t data structure
 *   \param   widget:  Gui wigdet for start button  
 */
void GuiStart(GtkButton* button, gpointer gp) {
  //printf("Start button pushed!\n");
  
  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;

  info->FCP_DAQrun->InRun = 1;

  if ( info->FCP_DAQrun->PauseRun == 0 ) {
    // read in and update run number
    std::ifstream runNumberFile("runNumber.txt");
    runNumberFile >> g_runHeader->runNumber;
    runNumberFile.close();
    std::ofstream runNumberFileNew("runNumber.txt");
    runNumberFileNew << g_runHeader->runNumber+1 << std::endl;
    runNumberFileNew.close();
    
    char buf[200];
    sprintf(buf, "Begin run %d", g_runHeader->runNumber);
    printToLogfile(buf);

    info->nTriggersInRun = 0;
    sprintf(buf, "Run %d in progress: %d triggers recorded", g_runHeader->runNumber, info->nTriggersInRun);
    sprintf(buf, "<span foreground=\"#ff0044\">Run %d in progress: %d triggers recorded</span>", g_runHeader->runNumber, info->nTriggersInRun);
    //gtk_label_set_text((GtkLabel*)info->run_status_label, buf);   
    gtk_label_set_markup((GtkLabel*)info->run_status_label, buf);
    gtk_widget_show(info->run_status_label);
    while(gtk_events_pending()) {
      gtk_main_iteration();
    }
    
    if (openOutputFile(info) != 0) {
      return;
    }
  }
 
  gtk_widget_set_sensitive (info->start, FALSE);
  gtk_widget_set_sensitive (info->pause, TRUE);
  gtk_widget_set_sensitive (info->stop, TRUE);
  gtk_widget_set_sensitive (info->exit, FALSE);
  gtk_widget_set_sensitive (info->scintChoice0, FALSE);
  gtk_widget_set_sensitive (info->scintChoice1, FALSE);
  gtk_widget_set_sensitive (info->ShCalPed0, FALSE);
  gtk_widget_set_sensitive (info->ShCalPed1, FALSE);
  gtk_widget_set_sensitive (info->assemblyInBeam0, FALSE);
  gtk_widget_set_sensitive (info->assemblyInBeam1, FALSE);
  gtk_widget_set_sensitive (info->includeTailVeto, FALSE);


  //  info->FCP_DAQrun->PauseRun = 0;
  info->FCP_DAQrun->EndRun = 0;

  //  printf("Acquisition started\n");
		
  //CAEN_DGTZ_SWStartAcquisition(info->handle);
  
  updateConditions(info, true);

  // don't start a run in the middle of a spill (might lead to some digitizers
  // being ready to respond to a trigger before other)
  // printf("beam_status before strstr is %s", info->beam_status);
  readFromSocket(info);
  if (strstr(info->beam_status, "spill") == NULL) {
    info->FCP_DAQrun->StartPushedDuringSpill = 0;
    startRun(info);
  } else {
   info->FCP_DAQrun->StartPushedDuringSpill = 1;
  }
  //  printf("beam_status after strstr is %s", info->beam_status);

  if (info->DAQ_thread_ID == 0) {
    info->DAQ_thread_ID = g_idle_add(startDAQLoop, gp);
  }


}

/*! \fn      void GuiScintChoice(FCP_DAQ_Run_t *FCP_DAQrun, GtkWidget *widget)
 *   \brief   start run 
 *
 *   \param   FCP_DAQrun:   Pointer to the FCP_DAQ_Run_t data structure
 *   \param   widget:  Gui wigdet for scintillator choice button  
 */
void GuiScintChoice(GtkButton* button, gpointer gp) {
  //printf("Scintillator choice button pushed!\n");
  
  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;
  if (button == (GtkButton*)info->scintChoice0) {
    if (gtk_toggle_button_get_active((GtkToggleButton*)button) ) {
      //printf("Will use small scintillators\n");
      printToLogfile("Will use small scintillators");
      info->FCP_DAQcfg->scintUsed = 0;
      //      writeToSocket("small scintillators");
      // hack due to wiring inversion in trigger
      writeToSocket("large scintillators");  
    }
  } else if (button == (GtkButton*)info->scintChoice1) {
    if (gtk_toggle_button_get_active((GtkToggleButton*)button) ) {
      //Printf("Will use large scintillators\n");
      printToLogfile("Will use large scintillators");
      info->FCP_DAQcfg->scintUsed = 1;
      //      writeToSocket("large scintillators");
      // hack due to wiring inversion in trigger                          
      writeToSocket("small scintillators");
    }
  }  else if (button == (GtkButton*)info->ShCalPed0) {
    if (gtk_toggle_button_get_active((GtkToggleButton*)button) ) {
      printf("Set ShCal pedestals high (I think)\n");
      resetShCalDCoffsets(info->FCP_DAQcfg, ShCalPed_HV_pos);
    }
  }  else if (button == (GtkButton*)info->ShCalPed1) {
    if (gtk_toggle_button_get_active((GtkToggleButton*)button) ) {
      printf("Set ShCal pedestals low (I think)\n");
      resetShCalDCoffsets(info->FCP_DAQcfg, ShCalPed_HV_neg);
    }
  }else {
    printf("Unknown button pressed\n");
    printToLogfile("Unknown button pressed");
  }
  
}

/*! \fn      void GuiAssemblyInBeam(FCP_DAQ_Run_t *FCP_DAQrun, GtkWidget *widget)
 *   \brief   start run 
 *
 *   \param   FCP_DAQrun:   Pointer to the FCP_DAQ_Run_t data structure
 *   \param   widget:  Gui wigdet for recording which assembly is in the beam  
 */
void GuiAssemblyInBeam(GtkButton* button, gpointer gp) {
  //printf("Assembly in beam button pushed!\n");
  
  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;
  if (button == (GtkButton*)info->assemblyInBeam0) {
    //printf("Upper assembly is in beam\n");
    printToLogfile("Upper assembly is in beam");
    //    info->FCP_DAQcfg->assemblyInBeam = 0;
  } else if (button == (GtkButton*)info->assemblyInBeam1) {
    // printf("Lower assembly is in beam\n");
    printToLogfile("Lower assembly is in beam");
    // info->FCP_DAQcfg->assemblyInBeam = 1;
  } else {
    printf("Unknown button pressed\n");
    printToLogfile("Unknown button pressed");
  }
  
}

/*! \fn      void GuiIncludeTailVeto(FCP_DAQ_Run_t *FCP_DAQrun, GtkWidget *widget)
 *   \brief   start run 
 *
 *   \param   FCP_DAQrun:   Pointer to the FCP_DAQ_Run_t data structure
 *   \param   widget:  Gui wigdet for choice on whether to include tail veto 
 *                     in trigger 
 */
void GuiIncludeTailVeto(GtkButton* button, gpointer gp) {
  //printf("Include tail veto button pushed!\n");
  
  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;

  if (gtk_toggle_button_get_active((GtkToggleButton*)button) ) {
    //printf("Yes, I will include the tail veto\n");
    printToLogfile("Halo and tail veto included in trigger");
    info->FCP_DAQcfg->includeTailVeto = 1;
    writeToSocket("halo trigger veto");
  } else {
    info->FCP_DAQcfg->includeTailVeto = 0;
    //printf("No, I will not include the tail veto\n");
    printToLogfile("Halo and tail veto not included in trigger");
    writeToSocket("no halo trigger veto");
  }
  
}

void requestComment(const char* prompt, const char* comment_prefix, const char* default_comment, FCP_DAQ_Info_t *info) {
  GtkWidget *dialog, *label, *content_area, *action_area, *txt_entry;
  GtkDialogFlags flags;
  
  // Create the widgets
  flags = GTK_DIALOG_DESTROY_WITH_PARENT;
  dialog = gtk_dialog_new_with_buttons ("Operator comment",
					(GtkWindow*)info->window,
					flags,
					"_Record comment",
					GTK_RESPONSE_NONE,
					NULL);
  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
 
  label = gtk_label_new (prompt);
  
  action_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
  
  GtkEntryBuffer *txt_buffer = gtk_entry_buffer_new(default_comment, strlen(default_comment));

  txt_entry = gtk_entry_new_with_buffer(txt_buffer);

  // Ensure that the dialog box is destroyed when the user responds
  
  g_signal_connect_swapped (dialog,
			    "response",
			    G_CALLBACK (gtk_widget_destroy),
			    dialog);
  
  // Add the label, and show everything we’ve added
  
  gtk_container_add (GTK_CONTAINER (content_area), label);

  gtk_container_add(GTK_CONTAINER(action_area), txt_entry);

  gtk_widget_show_all (dialog);
  gtk_dialog_run((GtkDialog*)dialog);


  sprintf(g_comment_string, "%s", gtk_entry_buffer_get_text(txt_buffer));
  if (g_logfile != NULL) {
    char username[100];
    getlogin_r(username, 100);
    char buf[200];
    sprintf(buf, "%s: Comment by %s:%s\n",  comment_prefix, username, g_comment_string);
    printToLogfile(buf);
  } 
}

void stopRun(FCP_DAQ_Info_t *info) {

  info->FCP_DAQrun->AcqRun = 0;
  info->FCP_DAQrun->EndRun = 1;
  int iBoard;
  const int buflen = 200;
  char buf[buflen];
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    uint32_t ret = CAEN_DGTZ_SWStopAcquisition(info->handle_ADC[iBoard]);
    if (ret != 0) {
      sprintf(buf, "Error stopping data aqcuisition for handle %d: %d\n", info->handle_ADC[iBoard], ret);
      printToLogfile(buf);
    }
  }
  if (info->FCP_DAQrun->fout_allch) { 
    fclose(info->FCP_DAQrun->fout_allch);
    info->FCP_DAQrun->fout_allch = NULL;
  }

  if (info->DAQ_thread_ID != 0) {
    g_source_remove(info->DAQ_thread_ID);
    info->DAQ_thread_ID = 0;   
  }

  // send end run signals to arduino and BPC
  sprintf(buf, "stop run %d", g_runHeader->runNumber);
  writeToSocket(buf);
  if (merge_BPC_data == 1) {
    sprintf(buf, "EoR %d",  g_runHeader->runNumber);
    
    int rc = put_fullstring("CMD", buf);
    if (rc < 0) {
      printf("Error writing to ControlHost: %d\n", rc);
    }
    char tag[TAGSIZE+1] = {"NULL"};
    int nb;
    int ntries = 0, maxtries = 500;
    while ((ntries < maxtries) && (strcmp(tag, "ACK") != 0)) {
      rc =check_head(tag, &nb);
      if (rc < 0) {
	printToLogfile("Error communicating with ControlHost when looking for EoR ACK");
	break;
      }
      if (rc > 0) {
	if (strcmp(tag, "ACK") != 0) {
	  char msgbuf[200];
	  sprintf(msgbuf, "Warning: unexpected tag when looking for EoR ACK: %s", tag);	 
	  printToLogfile(msgbuf);
	  get_string(buf, buflen-1);
	}
      }  
      usleep(10000);
      ntries++;
    }
    //      wait_head(tag, &nb);
    //      printf("ntries = %d\n", ntries);
    if (ntries >= maxtries || rc < 0) {
      printf("No ACK received from BPC\n");
      printToLogfile("No ACK received from BPC\n");
    } else {
      get_string(buf, buflen-1);
      char msgbuf[200];
      sprintf(msgbuf, "ACK from BPC received: %s", buf);
      printToLogfile(msgbuf);
      printf("ACK from BPC received: %s\n", buf);
    }
  }

  requestComment("Please explain why the run was ended", "End run: ", "Normal end run", info);
}

/*! \fn      void GuiStop(FCP_DAQ_Run_t *FCP_DAQrun, GtkWidget *widget)
 *   \brief   stop run 
 *
 *   \param   FCP_DAQrun:   Pointer to the FCP_DAQ_Run_t data structure
 *   \param   widget:  Gui wigdet for start button  
 */

void GuiStop(GtkButton* button, gpointer gp) {
  //printf("Stop button pushed!\n");
 
  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;

  info->FCP_DAQrun->InRun = 0;
  info->FCP_DAQrun->EndRun = 1;
  info->FCP_DAQrun->PauseRun = 0;

  //  gtk_label_set_text((GtkLabel*)info->run_status_label,"No run in progress");
  char buf[200];
  sprintf(buf, "Run %d ended: %d triggers recorded", g_runHeader->runNumber, info->nTriggersInRun);
  gtk_label_set_text((GtkLabel*)info->run_status_label, buf);

  gtk_widget_set_sensitive (info->stop, FALSE);
  gtk_widget_set_sensitive (info->pause, FALSE);
  gtk_widget_set_sensitive (info->start, TRUE);
  gtk_widget_set_sensitive (info->exit, TRUE);
  gtk_widget_set_sensitive (info->scintChoice0, TRUE);
  gtk_widget_set_sensitive (info->scintChoice1, TRUE);
  gtk_widget_set_sensitive (info->ShCalPed0, TRUE);
  gtk_widget_set_sensitive (info->ShCalPed1, TRUE);
  gtk_widget_set_sensitive (info->assemblyInBeam0, TRUE);
  gtk_widget_set_sensitive (info->assemblyInBeam1, TRUE);
  gtk_widget_set_sensitive (info->includeTailVeto, TRUE);
  gtk_button_set_label(GTK_BUTTON(info->start), "Start run");

  
 /* don't actually stop in the middle of a spill */
  if (strstr(info->beam_status, "spill") == NULL) {
    stopRun(info);
  }
  
}

void pauseRun(FCP_DAQ_Info_t* info, const char* comment = NULL) {
  //printf("Doing the pause\n");
  info->FCP_DAQrun->AcqRun = 0;
  int iBoard;
  char buf[200];
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    uint32_t ret = CAEN_DGTZ_SWStopAcquisition(info->handle_ADC[iBoard]);
    if (ret != 0) {
      sprintf(buf, "Error stoppping data aqcuisition for handle %d: %d\n", info->handle_ADC[iBoard], ret);
      printToLogfile(buf);
    }
  }

  if (info->DAQ_thread_ID != 0) {
    g_source_remove(info->DAQ_thread_ID);
    info->DAQ_thread_ID = 0;
  }

  if (info->FCP_DAQrun->autoPause == 0) {
    requestComment("Please enter the reason for pausing the run", "Pause run: ","", info);
  }

  info->FCP_DAQrun->autoPause = 0;
}

/*! \fn      void GuiPause(FCP_DAQ_Run_t *FCP_DAQrun, GtkWidget *widget)
 *   \brief   Pause the run 
 *
 *   \param   FCP_DAQrun:   Pointer to the FCP_DAQ_Run_t data structure
 *   \param   widget:  Gui wigdet for start button  
 */

void GuiPause(GtkButton* button, gpointer gp) {
  //printf("Pause button pushed!\n");

  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;

  gtk_widget_set_sensitive (info->stop, TRUE);
  gtk_widget_set_sensitive (info->pause, FALSE);
  gtk_widget_set_sensitive (info->start, TRUE);

  //  info->FCP_DAQrun->AcqRun = 0;
  info->FCP_DAQrun->PauseRun = 1;
  gtk_button_set_label(GTK_BUTTON(info->start), "Resume run");

  char buf[200];
  sprintf(buf, "pause run %d", g_runHeader->runNumber);
  writeToSocket(buf);

  /* don't actually pause in the middle of a spill */
  if (strstr(info->beam_status, "spill") == NULL) {
    pauseRun(info);
  }

}

void GuiExit(GtkButton* button, gpointer gp) {
  //printf("Exit button pushed!\n");

  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;

  // check if there are any errors that might have triggered the exit; 
  // if none, ask user if they really want to exit
  if (info->FCP_DAQrun->nErrors == 0) {
    GtkWidget *dialog, *label, *content_area;
    GtkDialogFlags flags = (GtkDialogFlags) (GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT);
    dialog = gtk_dialog_new_with_buttons (NULL,
					  (GtkWindow*)info->window,
					  flags,
					  "Yes",
					  GTK_RESPONSE_ACCEPT,
					  "No",
					  GTK_RESPONSE_REJECT,
					  NULL);
    
    content_area = gtk_dialog_get_content_area (GTK_DIALOG ((GtkDialog*)dialog));
    label = gtk_label_new ("Are you sure you want to exit FCP_DAQ?");
    
    // Add the label, and show everything we’ve added
    
    gtk_container_add (GTK_CONTAINER (content_area), label);
    gtk_widget_show_all (dialog);
    
    int result = gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy(dialog);
    if (result == GTK_RESPONSE_ACCEPT) {
      quitProgram(info);
    }
  } else {
    quitProgram(info);
  }    
}

void quitProgram(FCP_DAQ_Info_t* info) {

  char buf[200];
  if (info != NULL) {
    info->FCP_DAQrun->AcqRun = 0;
    info->FCP_DAQrun->EndRun = 1;
    /* stop the acquisition */
    int iBoard;
    for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
      uint32_t ret = CAEN_DGTZ_SWStopAcquisition(info->handle_ADC[iBoard]);
      if (ret != 0) {
	sprintf(buf, "Error stopping data aqcuisition for handle %d: %d\n", info->handle_ADC[iBoard], ret);
	printToLogfile(buf);
      }
    }
    /* close the output file*/
    if (info->FCP_DAQrun->fout_allch) 
      fclose(info->FCP_DAQrun->fout_allch);
    
    if (sockfd) 
      shutdown(sockfd,0);

    drop_connection();
    
    /* close the device and free the buffers */
    for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
      CAEN_DGTZ_ClearData(info->handle_ADC[iBoard]);      
      if (info->buffer_ADC[iBoard] !=  NULL) {
	CAEN_DGTZ_FreeReadoutBuffer(&info->buffer_ADC[iBoard]);
      }
      CAEN_DGTZ_CloseDigitizer(info->handle_ADC[iBoard]);
    }
  }

  if (info->FCP_DAQrun->nErrors >= 0) {
    for (int iErr = 0; iErr < info->FCP_DAQrun->nErrors; iErr++) {
      FCP_DAQerror err = info->FCP_DAQrun->errors[iErr];
      printf("Quitting FCP_DAQ with error %s\n", err.msg);
      if (g_logfile != NULL) {
	sprintf(buf, "Program ended with error %s", err.msg);
	printToLogfile(buf);
      }
    }
    if (g_logfile != NULL) {
      fclose(g_logfile);
    }
    exit(info->FCP_DAQrun->errors[0].id);
  } else {
    if (g_logfile != NULL) {
      printToLogfile("Program ended normally");
      fclose(g_logfile);
    }
    exit(ERR_NONE);
  }

  // shut down related processes
  //  char cmdstr[200];
  //sprintf(cmdstr, "%s/scripts/kill_processes.sh", RUNDIR);
  //system(cmdstr);

}

int openOutputFile(FCP_DAQ_Info_t* info) {

  if (!info->FCP_DAQrun->fout_allch) {
  
    //                sprintf(fname, "wave%d.dat", ch);

    /* old naming convention
    time_t rawtime;
    time(&rawtime);
    char timestamp[200];
    sprintf(timestamp, "%s",  ctime(&rawtime));
    // remove newline
    int len = strlen(timestamp);
    if( timestamp[len-1] == '\n' )
      timestamp[len-1] = 0;
    sprintf(fname, "%s/FCP_data_%s.dat", OUTPUT_TOPLEVEL_DIRECTORY, timestamp);
    //                if ((FCP_DAQrun->fout[ch] = fopen(fname, "wb")) == NULL)
    //    return -1;
    char *p = fname;
    for (; *p; ++p)
      {
	if (*p == ' ')
	  *p = '_';
      }
    */

    sprintf(bytestream_fname, "%s/bytestream/run%d.dat", OUTPUT_TOPLEVEL_DIRECTORY, g_runHeader->runNumber);
    //printf("Opening %s\n", root_fname);
    if ((info->FCP_DAQrun->fout_allch = fopen(bytestream_fname, "wb")) == NULL) {
      printf("Error opening output file %s\n", bytestream_fname);
      char buf[200];
      sprintf(buf, "Error opening output file %s\n", bytestream_fname);
      printToLogfile(buf);
      return -1;
    }

    if (writeRunHeader(info) != 0) {
      printf("Error writing run header to file %s\n", bytestream_fname);
      char buf[200];
      sprintf(buf, "Error writing run header to file %s", bytestream_fname);
      printToLogfile(buf);
      return -1;
    }

  }
  return 0;
}

int openLogFile() {

  if (!g_logfile) {
    char fname[200];
    //                sprintf(fname, "wave%d.log", ch);
    time_t rawtime;
    time(&rawtime);
    char timestamp[200];
    sprintf(timestamp, "%s",  ctime(&rawtime));
    // remove newline
    int len = strlen(timestamp);
    if( timestamp[len-1] == '\n' )
      timestamp[len-1] = 0;
    sprintf(fname, "%s/logfiles/FCP_DAQ_%s.log", OUTPUT_TOPLEVEL_DIRECTORY, timestamp);
    char *p = fname;
    for (; *p; ++p)
      {
	if (*p == ' ')
	  *p = '_';
      }
    //printf("Opening %s\n", fname);
    if ((g_logfile = fopen(fname, "w")) == NULL) {
      printf("Error opening output file %s\n", fname);
      return -1;
    }
  }
  return 0;
}

int writeRunHeader(FCP_DAQ_Info_t* info) {

  if (!info->FCP_DAQrun->fout_allch) {
    printf("Error: attempting to write run header but file is not open");
    printToLogfile( "Error: attempting to write run header but file is not open");
    openErrorWindow(info, "Error writing run header", 1);
    return -1;
  }

  g_runHeader->dataFormatVersion = FCP_DAQ_Data_Format_Version;
  g_runHeader->channelDataSize = 2*info->FCP_DAQcfg->RecordLength + sizeof(FCP_DAQ_ChannelHeader_t);
  g_runHeader->NumEvents = info->FCP_DAQcfg->NumEvents; 
  g_runHeader->RecordLength = info->FCP_DAQcfg->RecordLength;
  g_runHeader->PostTrigger = info->FCP_DAQcfg->PostTrigger;
  g_runHeader->FPIOtype = info->FCP_DAQcfg->FPIOtype;
  g_runHeader->NumBoards = NUM_ADC_BOARDS;
  int iBoard;
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    g_runHeader->SerialNumber[iBoard] = info->BoardInfo[iBoard].SerialNumber;
    g_runHeader->ExtTriggerMode[iBoard] = info->FCP_DAQcfg->ExtTriggerMode[iBoard];
    g_runHeader->EnableMask[iBoard] = info->FCP_DAQcfg->EnableMask[iBoard];
    int iChan;
    for (iChan = 0; iChan < NUM_ADC_CHANNELS; iChan++) {
      g_runHeader->DCoffset[iBoard][iChan] = info->FCP_DAQcfg->DCoffset[iBoard][iChan];
      g_runHeader->Vrange[iBoard][iChan] = info->FCP_DAQcfg->Vrange[iBoard][iChan];
      g_runHeader->ChannelTriggerMode[iBoard][iChan] = info->FCP_DAQcfg->ChannelTriggerMode[iBoard][iChan];
      g_runHeader->PulsePolarity[iBoard][iChan] = info->FCP_DAQcfg->PulsePolarity[iBoard][iChan];
    }
  }
  g_runHeader->GWn = info->FCP_DAQcfg->GWn;
  int iGW;
  for (iGW = 0; iGW < NUM_GW; iGW++) {
    g_runHeader->GWaddr[iGW] = info->FCP_DAQcfg->GWaddr[iGW];
    g_runHeader->GWdata[iGW] = info->FCP_DAQcfg->GWdata[iGW];
    g_runHeader->GWmask[iGW] = info->FCP_DAQcfg->GWmask[iGW];
  }
  
 
  g_runHeader->nHV_set = NHV_set;
  g_runHeader->nHV_read = NHV_read;
  g_runHeader->nCryostats = NCRYOSTATS;

  g_runHeader->scintUsed = info->FCP_DAQcfg->scintUsed;
  g_runHeader->sourceInBeam = info->FCP_DAQcfg->sourceInBeam;
  g_runHeader->includeTailVeto = info->FCP_DAQcfg->includeTailVeto;

  g_runHeader->FCal_rod_front_source_digitizer_SN = FCAL_ROD_FRONT_SOURCE_DIGITIZER_SN;
  g_runHeader->FCal_rod_front_source_digitizer_handle = FCAL_ROD_FRONT_SOURCE_DIGITIZER_HANDLE;
  g_runHeader->FCal_rod_front_source_channel = FCAL_ROD_FRONT_SOURCE_CHANNEL;
  g_runHeader->FCal_rod_mid_source_digitizer_SN  = FCAL_ROD_MID_SOURCE_DIGITIZER_SN;
  g_runHeader->FCal_rod_mid_source_digitizer_handle  = FCAL_ROD_MID_SOURCE_DIGITIZER_HANDLE;
  g_runHeader->FCal_rod_mid_source_channel = FCAL_ROD_MID_SOURCE_CHANNEL;
  g_runHeader->FCal_rod_rear_source_digitizer_SN  = FCAL_ROD_REAR_SOURCE_DIGITIZER_SN;
  g_runHeader->FCal_rod_rear_source_digitizer_handle  = FCAL_ROD_REAR_SOURCE_DIGITIZER_HANDLE;
  g_runHeader->FCal_rod_rear_source_channel = FCAL_ROD_REAR_SOURCE_CHANNEL;
  g_runHeader->FCal_rod_front_nosource_digitizer_SN  = FCAL_ROD_FRONT_NOSOURCE_DIGITIZER_SN;
  g_runHeader->FCal_rod_front_nosource_digitizer_handle  = FCAL_ROD_FRONT_NOSOURCE_DIGITIZER_HANDLE;
  g_runHeader->FCal_rod_front_nosource_channel = FCAL_ROD_FRONT_NOSOURCE_CHANNEL;
  g_runHeader->FCal_rod_mid_nosource_digitizer_SN  = FCAL_ROD_MID_NOSOURCE_DIGITIZER_SN;
  g_runHeader->FCal_rod_mid_nosource_digitizer_handle  = FCAL_ROD_MID_NOSOURCE_DIGITIZER_HANDLE;
  g_runHeader->FCal_rod_mid_nosource_channel = FCAL_ROD_MID_NOSOURCE_CHANNEL;
  g_runHeader->FCal_rod_rear_nosource_digitizer_SN  = FCAL_ROD_REAR_NOSOURCE_DIGITIZER_SN;
  g_runHeader->FCal_rod_rear_nosource_digitizer_handle  = FCAL_ROD_REAR_NOSOURCE_DIGITIZER_HANDLE;
  g_runHeader->FCal_rod_rear_nosource_channel = FCAL_ROD_REAR_NOSOURCE_CHANNEL;

  g_runHeader->ShCal_front_source_quad_1_digitizer_SN  = SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_SN;
  g_runHeader->ShCal_front_source_quad_1_digitizer_handle  = SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_HANDLE;
  g_runHeader->ShCal_front_source_quad_1_channel = SHCAL_FRONT_SOURCE_QUAD_1_CHANNEL;
  g_runHeader->ShCal_front_source_quad_2_digitizer_SN  = SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_SN;
  g_runHeader->ShCal_front_source_quad_2_digitizer_handle  = SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE;
  g_runHeader->ShCal_front_source_quad_2_channel = SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL;
  g_runHeader->ShCal_front_source_quad_3_digitizer_SN  = SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_SN;
  g_runHeader->ShCal_front_source_quad_3_digitizer_handle  = SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_HANDLE;
  g_runHeader->ShCal_front_source_quad_3_channel = SHCAL_FRONT_SOURCE_QUAD_3_CHANNEL;
  g_runHeader->ShCal_front_source_quad_4_digitizer_SN  = SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_SN;
  g_runHeader->ShCal_front_source_quad_4_digitizer_handle  = SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_HANDLE;
  g_runHeader->ShCal_front_source_quad_4_channel = SHCAL_FRONT_SOURCE_QUAD_4_CHANNEL;
  g_runHeader->ShCal_rear_source_quad_1_digitizer_SN  = SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_SN;
  g_runHeader->ShCal_rear_source_quad_1_digitizer_handle  = SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_HANDLE;
  g_runHeader->ShCal_rear_source_quad_1_channel = SHCAL_REAR_SOURCE_QUAD_1_CHANNEL;
  g_runHeader->ShCal_rear_source_quad_2_digitizer_SN  = SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_SN;
  g_runHeader->ShCal_rear_source_quad_2_digitizer_handle  = SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_HANDLE;
  g_runHeader->ShCal_rear_source_quad_2_channel = SHCAL_REAR_SOURCE_QUAD_2_CHANNEL;
  g_runHeader->ShCal_rear_source_quad_3_digitizer_SN  = SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_SN;
  g_runHeader->ShCal_rear_source_quad_3_digitizer_handle  = SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_HANDLE;
  g_runHeader->ShCal_rear_source_quad_3_channel = SHCAL_REAR_SOURCE_QUAD_3_CHANNEL;
  g_runHeader->ShCal_rear_source_quad_4_digitizer_SN  = SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_SN;
  g_runHeader->ShCal_rear_source_quad_4_digitizer_handle  = SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_HANDLE;
  g_runHeader->ShCal_rear_source_quad_4_channel = SHCAL_REAR_SOURCE_QUAD_4_CHANNEL;
  g_runHeader->ShCal_front_nosource_quad_1_digitizer_SN  = SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_SN;
  g_runHeader->ShCal_front_nosource_quad_1_digitizer_handle  = SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_HANDLE;
  g_runHeader->ShCal_front_nosource_quad_1_channel = SHCAL_FRONT_NOSOURCE_QUAD_1_CHANNEL;
  g_runHeader->ShCal_front_nosource_quad_2_digitizer_SN  = SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_SN;
  g_runHeader->ShCal_front_nosource_quad_2_digitizer_handle  = SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_HANDLE;
  g_runHeader->ShCal_front_nosource_quad_2_channel = SHCAL_FRONT_NOSOURCE_QUAD_2_CHANNEL;
  g_runHeader->ShCal_front_nosource_quad_3_digitizer_SN  = SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_SN;
  g_runHeader->ShCal_front_nosource_quad_3_digitizer_handle  = SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_HANDLE;
  g_runHeader->ShCal_front_nosource_quad_3_channel = SHCAL_FRONT_NOSOURCE_QUAD_3_CHANNEL;
  g_runHeader->ShCal_front_nosource_quad_4_digitizer_SN  = SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_SN;
  g_runHeader->ShCal_front_nosource_quad_4_digitizer_handle  = SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_HANDLE;
  g_runHeader->ShCal_front_nosource_quad_4_channel = SHCAL_FRONT_NOSOURCE_QUAD_4_CHANNEL;
  g_runHeader->ShCal_rear_nosource_quad_1_digitizer_SN  = SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_SN;
  g_runHeader->ShCal_rear_nosource_quad_1_digitizer_handle  = SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_HANDLE;
  g_runHeader->ShCal_rear_nosource_quad_1_channel = SHCAL_REAR_NOSOURCE_QUAD_1_CHANNEL;
  g_runHeader->ShCal_rear_nosource_quad_2_digitizer_SN  = SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_SN;
  g_runHeader->ShCal_rear_nosource_quad_2_digitizer_handle  = SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_HANDLE;
  g_runHeader->ShCal_rear_nosource_quad_2_channel = SHCAL_REAR_NOSOURCE_QUAD_2_CHANNEL;
  g_runHeader->ShCal_rear_nosource_quad_3_digitizer_SN  = SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_SN;
  g_runHeader->ShCal_rear_nosource_quad_3_digitizer_handle  = SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_HANDLE;
  g_runHeader->ShCal_rear_nosource_quad_3_channel = SHCAL_REAR_NOSOURCE_QUAD_3_CHANNEL;
  g_runHeader->ShCal_rear_nosource_quad_4_digitizer_SN  = SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_SN;
  g_runHeader->ShCal_rear_nosource_quad_4_digitizer_handle  = SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_HANDLE;
  g_runHeader->ShCal_rear_nosource_quad_4_channel = SHCAL_REAR_NOSOURCE_QUAD_4_CHANNEL;

  g_runHeader->S1_counter_digitizer_SN = S1_COUNTER_DIGITIZER_SN;
  g_runHeader->S1_counter_digitizer_handle = S1_COUNTER_DIGITIZER_HANDLE;
  g_runHeader->S1_counter_channel = S1_COUNTER_CHANNEL;
  g_runHeader->S2_counter_digitizer_SN = S2_COUNTER_DIGITIZER_SN;
  g_runHeader->S2_counter_digitizer_handle = S2_COUNTER_DIGITIZER_HANDLE;
  g_runHeader->S2_counter_channel = S2_COUNTER_CHANNEL;
  g_runHeader->S3_counter_digitizer_SN = S3_COUNTER_DIGITIZER_SN;
  g_runHeader->S3_counter_digitizer_handle = S3_COUNTER_DIGITIZER_HANDLE;
  g_runHeader->S3_counter_channel = S3_COUNTER_CHANNEL;
  g_runHeader->S4_counter_digitizer_SN = S4_COUNTER_DIGITIZER_SN;
  g_runHeader->S4_counter_digitizer_handle = S4_COUNTER_DIGITIZER_HANDLE;
  g_runHeader->S4_counter_channel = S4_COUNTER_CHANNEL;
  g_runHeader->Leakage_counter_digitizer_SN = LEAKAGE_COUNTER_DIGITIZER_SN;
  g_runHeader->Leakage_counter_digitizer_handle = LEAKAGE_COUNTER_DIGITIZER_HANDLE;
  g_runHeader->Leakage_counter_channel = LEAKAGE_COUNTER_CHANNEL;
  g_runHeader->Halo_counter_digitizer_SN = HALO_COUNTER_DIGITIZER_SN;
  g_runHeader->Halo_counter_digitizer_handle = HALO_COUNTER_DIGITIZER_HANDLE;
  g_runHeader->Halo_counter_channel = HALO_COUNTER_CHANNEL;

  g_runHeader->Digitizer_1_trigger_digitizer_SN = DIGITIZER_1_TRIGGER_DIGITIZER_SN;
  g_runHeader->Digitizer_1_trigger_digitizer_handle = DIGITIZER_1_TRIGGER_DIGITIZER_HANDLE;
  g_runHeader->Digitizer_1_trigger_channel = DIGITIZER_1_TRIGGER_CHANNEL;
  g_runHeader->Digitizer_2_trigger_digitizer_SN = DIGITIZER_2_TRIGGER_DIGITIZER_SN;
  g_runHeader->Digitizer_2_trigger_digitizer_handle = DIGITIZER_2_TRIGGER_DIGITIZER_HANDLE;
  g_runHeader->Digitizer_2_trigger_channel = DIGITIZER_2_TRIGGER_CHANNEL;
  g_runHeader->Digitizer_3_trigger_digitizer_SN = DIGITIZER_3_TRIGGER_DIGITIZER_SN;
  g_runHeader->Digitizer_3_trigger_digitizer_handle = DIGITIZER_3_TRIGGER_DIGITIZER_HANDLE;
  g_runHeader->Digitizer_3_trigger_channel = DIGITIZER_3_TRIGGER_CHANNEL;
  g_runHeader->Digitizer_4_trigger_digitizer_SN = DIGITIZER_4_TRIGGER_DIGITIZER_SN;
  g_runHeader->Digitizer_4_trigger_digitizer_handle = DIGITIZER_4_TRIGGER_DIGITIZER_HANDLE;
  g_runHeader->Digitizer_4_trigger_channel = DIGITIZER_4_TRIGGER_CHANNEL;

  if (info->FCP_DAQrun->fout_allch != 0 ){
    if (fwrite(g_runHeader, sizeof(*g_runHeader), 1, info->FCP_DAQrun->fout_allch) != 1 ) {
      printf("Error: unable to write run header");
      printToLogfile("Error: unable to write to run header");
      openErrorWindow(info, "Error writing run header", 1);
      return -1;
    }
  } else {
    printf("Error: unable to write run header -- output file not open\n");
    printToLogfile("Error: unable to write to run header -- output file not open");
  }

  // Write out root tree
  if (t_event == 0) {
    if (merge_BPC_data == 1) {
      t_event = new TTree("merged_digitizer_BPC", "merged_digitizer_BPC");
    } else {
      t_event = new TTree("Digitizer_event", "Digitizer_event");
    }
  }

  char arr_descrip[200];
  if (b_dataFormatVersion == 0) {
    b_dataFormatVersion = t_event->Branch("dataFormatVersion", &g_runHeader->dataFormatVersion);
  } else {
    //    b_dataFormatVersion->SetAddress(&g_runHeader->dataFormatVersion);
  }
  if (b_channelDataSize == 0) {
    b_channelDataSize = t_event->Branch("channelDataSize", &g_runHeader->channelDataSize);
  } else {
    // b_channelDataSize->SetAddress(&g_runHeader->channelDataSize);
  }
  //  if (b_NumEvents == 0) {
  //  b_NumEvents = t_event->Branch("NumEvents", &g_runHeader->NumEvents);
  //} else {
  //  //b_NumEvents->SetAddress(&g_runHeader->NumEvents);
  // }
  if (b_RecordLength == 0) {
    b_RecordLength = t_event->Branch("RecordLength", &g_runHeader->RecordLength);  
  } else {
    // b_RecordLength->SetAddress(&g_runHeader->RecordLength); 
  }
  if (b_PostTrigger == 0) {
    b_PostTrigger = t_event->Branch("PostTrigger", &g_runHeader->PostTrigger);
  } else {
    // b_PostTrigger->SetAddress(&g_runHeader->PostTrigger);
  }
  if (b_FPIOtype == 0) {
    b_FPIOtype = t_event->Branch("FPIOtype", &g_runHeader->FPIOtype);
  } else {
    // b_FPIOtype->SetAddress( &g_runHeader->FPIOtype);
  }
  if (b_NumDigitizers == 0) {
    b_NumDigitizers = t_event->Branch("NumDigitizers", &g_runHeader->NumBoards);
  } else {
    // b_NumBoards->SetAddress(&g_runHeader->NumBoards);
  }
  if (b_SerialNumber == 0) {
    sprintf(arr_descrip, "SerialNumber[%d]/i", g_runHeader->NumBoards);
    b_SerialNumber = t_event->Branch("SerialNumber", g_runHeader->SerialNumber, arr_descrip);
  } else {
    //b_SerialNumber->SetAddress(g_runHeader->SerialNumber);
  }
  if (b_ExtTriggerMode == 0) {
    sprintf(arr_descrip, "ExtTriggerMode[%d]/i", g_runHeader->NumBoards);
    b_ExtTriggerMode = t_event->Branch("ExtTriggerMode", g_runHeader->ExtTriggerMode, arr_descrip);
  } else {
    //b_ExtTriggerMode->SetAddress(g_runHeader->ExtTriggerMode);
  }
  if (b_EnableMask == 0) {
    sprintf(arr_descrip, "EnableMask[%d]/i", g_runHeader->NumBoards);
    b_EnableMask = t_event->Branch("EnableMask", g_runHeader->EnableMask, arr_descrip);
  } else {
    //b_EnableMask->SetAddress(g_runHeader->EnableMask);
  }
  if (b_DCoffset == 0) {
    sprintf(arr_descrip, "DCoffset[%d][%d]/i", g_runHeader->NumBoards, NUM_ADC_CHANNELS);
    b_DCoffset = t_event->Branch("DCoffset", g_runHeader->DCoffset, arr_descrip);
  } else {
    // b_DCoffset->SetAddress(g_runHeader->DCoffset);
  }
  if (b_ChannelTriggerMode == 0) {
    sprintf(arr_descrip, "ChannelTriggerMode[%d][%d]/i", g_runHeader->NumBoards, NUM_ADC_CHANNELS);
    b_ChannelTriggerMode = t_event->Branch("ChannelTriggerMode", g_runHeader->ChannelTriggerMode, arr_descrip);
  } else {
    //b_ChannelTriggerMode->SetAddress(g_runHeader->ChannelTriggerMode);
  }
  if (b_PulsePolarity == 0) {    
    sprintf(arr_descrip, "PulsePolarity[%d][%d]/i", g_runHeader->NumBoards, NUM_ADC_CHANNELS);
    b_PulsePolarity = t_event->Branch("PulsePolarity", g_runHeader->PulsePolarity, arr_descrip);
  } else {
    //b_PulsePolarity->SetAddress(g_runHeader->PulsePolarity);
  }

  if (b_GWn == 0) {
    b_GWn = t_event->Branch("GWn", &g_runHeader->GWn);
  } else {
    //b_GWn->SetAddress(&g_runHeader->GWn);
  }
  if (b_GWaddr == 0) {
    sprintf(arr_descrip, "GWaddr[%d]/i", g_runHeader->GWn);
    b_GWaddr = t_event->Branch("GWaddr", g_runHeader->GWaddr, arr_descrip);
  } else {
    //b_GWaddr->SetAddress(g_runHeader->GWaddr);
  }
  if (b_GWdata == 0) {
    sprintf(arr_descrip, "GWdata[%d]/i", g_runHeader->GWn);
    b_GWdata = t_event->Branch("GWdata", g_runHeader->GWdata, arr_descrip);
  } else {
    //b_GWdata->SetAddress(g_runHeader->GWdata);
  }
  if (b_GWmask == 0) {    
    sprintf(arr_descrip, "GWmask[%d]/i", g_runHeader->GWn);
    b_GWmask = t_event->Branch("GWmask", g_runHeader->GWmask, arr_descrip);
  } else {
    //b_GWmask->SetAddress(g_runHeader->GWmask);
  }

  if (b_nCryostats == 0) {
    b_nCryostats = t_event->Branch("nCryostats", &g_runHeader->nCryostats);
  } else {
    //b_nCryostats->SetAddress(&g_runHeader->nCryostats);
  }

  if (b_scintUsed == 0) {
    b_scintUsed = t_event->Branch("scintUsed", &g_runHeader->scintUsed);
  }
  if (b_tailVeto == 0) {
    b_tailVeto = t_event->Branch("includeTailVeto", &g_runHeader->includeTailVeto);
  }
 if (b_sourceInBeam == 0) {
    b_sourceInBeam = t_event->Branch("sourceInBeam", &g_runHeader->sourceInBeam);
  }

  return 0;
}

int openErrorWindow(FCP_DAQ_Info_t* info, const char *message, int severity) {
  info->FCP_DAQrun->autoPause = 1;
  GuiPause((GtkButton*)info->pause, info);
  GtkWidget *dialog, *label[20], *content_area;
  GtkDialogFlags flags = (GtkDialogFlags) (GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT);
  if (severity == 0) {
    dialog = gtk_dialog_new_with_buttons (NULL,
					  (GtkWindow*)info->window,
					  flags,
					  "End run",
					  1,
					  "Resume run",
					  2,
					  NULL);
  } else {
    dialog = gtk_dialog_new_with_buttons (NULL,
					  (GtkWindow*)info->window,
					  flags,
					  "Exit",
					  1,
					  NULL);
  }

  content_area = gtk_dialog_get_content_area (GTK_DIALOG ((GtkDialog*)dialog));
  label[0] = gtk_label_new (message);
  
  // Add the label, and show everything we’ve added
  
  gtk_container_add (GTK_CONTAINER (content_area), label[0]);
  
  int iErr;
  for (iErr = 0; iErr < info->FCP_DAQrun->nErrors; iErr++) {
    FCP_DAQerror err = info->FCP_DAQrun->errors[iErr];
    label[iErr+1] = gtk_label_new (err.msg);
    gtk_container_add (GTK_CONTAINER (content_area), label[iErr+1]);
  }
  if (severity > 0) {
    label[info->FCP_DAQrun->nErrors+1] = gtk_label_new("One or more fatal errors; will exit FCP_DAQ");
    gtk_container_add (GTK_CONTAINER (content_area), label[info->FCP_DAQrun->nErrors+1]);
  };    
  gtk_widget_show_all (dialog);
  
  int result = gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy(dialog);
  //  printf("Result was %d\n", result);
  if (result == 1) {
    if (severity == 0) {
      GuiStop((GtkButton*)info->stop, info);
    } else {
      GuiExit((GtkButton*)info->exit, info);
    }
  }
  if (result == 2) {
    if (info->FCP_DAQrun->nErrors > 0) {
      for (int iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
	info->FCP_DAQrun->ackBoardFailBits[iBoard] = ( 	info->FCP_DAQrun->ackBoardFailBits[iBoard] | info->FCP_DAQrun->boardFailBits[iBoard] );
      }
    }
    GuiStart((GtkButton*)info->stop, info);
  }

  return 0;
}

int updateConditions(FCP_DAQ_Info_t* info, bool startOfRun) {

  // check the digitizers for errors
  int iBoard;
  char buf[20000];

  if (sim_mode == 0) {
    for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
      uint32_t status;
      status = CheckDigitizerFailureStatus(info, info->handle_ADC[iBoard]);
      //    if (iBoard == 1) status = 0xfff;
      info->FCP_DAQrun->boardFailBits[iBoard] = status;
      uint32_t newError = (status ^ info->FCP_DAQrun->ackBoardFailBits[iBoard]);

      if (newError & (1 << 4)) {
	addError(info->FCP_DAQrun, iBoard, ERR_PLL_LOCK);
	printToLogfile("Digitizer error detected: PLL not locked.\n");

      }
      if (newError & (1 << 5)) {
	addError(info->FCP_DAQrun, iBoard, ERR_OVERTEMP);
	printToLogfile("Digitizer over temperature detected.\n"); 
      }
      if (newError & (1 << 6)) {
	addError(info->FCP_DAQrun, iBoard, ERR_ADC);
	printToLogfile("Digitizer ADC power is down.\n");
      }
      // record ADC temperatures
      int32_t ch;
      uint32_t maxtemp = 0;
      sprintf(buf,"Digitizer %s ADC temperatures:\n", digitizerName[iBoard]);
      for (ch = 0; ch < NUM_ADC_CHANNELS; ch++) {
	uint32_t temp;
	uint32_t ret = CAEN_DGTZ_ReadTemperature(info->handle_ADC[iBoard], ch, &temp);
	sprintf(buf, "%s Digitizer %s channel %02d: ", buf, digitizerName[iBoard], ch);
	if (ret == CAEN_DGTZ_Success) {
	  sprintf(buf, "%s %u C\n", buf, temp);
	  if (temp > maxtemp) maxtemp = temp;
	}  else {
	  sprintf(buf, "%s Digitizer %s CAENDigitizer ERR %d\n", buf, digitizerName[iBoard], ret);
	}
      }
      if (maxtemp > 62) {
	// too hot, set fan to max!
	if (info->FCP_DAQrun->fanSpeed[iBoard] == 0) {
	  int32_t ret = WriteRegisterBitmask(info->handle_ADC[iBoard], 0x8168, 0x18, 0xff);
	  if (ret == CAEN_DGTZ_Success) {
	    printToLogfile("Too hot! Setting fan speed to high!");
	    info->FCP_DAQrun->fanSpeed[iBoard] = 0;
	  } else {
	    printToLogfile("Error setting fan speed\n");
	  }
	} else if (maxtemp < 60 && info->FCP_DAQrun->fanSpeed[iBoard] == 0) {
	  int32_t ret = WriteRegisterBitmask(info->handle_ADC[iBoard], 0x8168, 0x10, 0xff);
	  if (ret == CAEN_DGTZ_Success) {
	    printToLogfile("Cool enough.. Setting fan speed to automatic");
	    info->FCP_DAQrun->fanSpeed[iBoard] = 1;
	  } else {
	    printToLogfile("Error setting fan speed\n");
	  }
  
	}
      }
      printToLogfile(buf);
    }

    if (info->FCP_DAQrun->nErrors > 0) {
      openErrorWindow(info, "Digitizer error(s) detected!", 1);
    }

  }
  
  // for HV we can read from a file
  // first, need to copy from the Windows laptop
  // delete any old files that might still be there, 
  // to make sure information is fresh
  std::remove("/home/varnes/FCalPulse/conditions/FCalPulse_HV_Settings.txt");
  char cmdstr[200];
  sprintf(cmdstr, "%s/scripts/copy_HV_file.sh", RUNDIR);
  if (system(cmdstr) != 0) {
    printf("Error copying HV conditions file\n");
    printToLogfile("Error copying HV conditions file\n");
    char buf[200];
    sprintf(buf, "Value of errno: %d\n", errno);
    printToLogfile(buf);
  }

  float HV_set_prev[NHV_set];
  if (!startOfRun) {
    for (int i = 0; i < NHV_set; i++) {
      HV_set_prev[i] = info->FCP_DAQrun->HV_set[i];
    }
  }

  std::ifstream HVfile("/home/varnes/FCalPulse/conditions/FCalPulse_HV_Settings.txt");
  if (!HVfile.is_open()) {
    printf("Error opening HV conditions file\n");
    printToLogfile("Error opening HV conditions file\n");
    if (info->FCP_DAQrun->InRun == 0) {
      gtk_widget_set_sensitive (info->ShCalPed0, FALSE);
      gtk_widget_set_sensitive (info->ShCalPed1, FALSE);
    }
  } else {

    while (!HVfile.eof()) {
      char ts[200];
      info->FCP_DAQrun->HV_read[3] = 0;
      HVfile >> ts >> info->FCP_DAQrun->HV_set[0] >> info->FCP_DAQrun->HV_read[0]
	     >> info->FCP_DAQrun->HV_set[2] >>  info->FCP_DAQrun->HV_read[1]
	     >> info->FCP_DAQrun->HV_set[1] >>  info->FCP_DAQrun->HV_read[2];

      //     printf("Read %d %d %d\n",  (int)info->FCP_DAQrun->HV_set[0],  (int)info->FCP_DAQrun->HV_set[1],  (int)info->FCP_DAQrun->HV_set[2]);
    }
    HVfile.close();
    
    //since we were able to read in the HV values, set the ShCal pedestals automatically
    if (info->FCP_DAQrun->HV_read[0] < 0) {
      gtk_toggle_button_set_active((GtkToggleButton*)info->ShCalPed1, TRUE);
    } else {
      gtk_toggle_button_set_active((GtkToggleButton*)info->ShCalPed0, TRUE);
    }
    gtk_widget_set_sensitive (info->ShCalPed0, FALSE);
    gtk_widget_set_sensitive (info->ShCalPed1, FALSE);
    

    for (int iHV = 0; iHV < NHV_set; iHV++) {
      g_spillHeader->HV_set[iHV] = info->FCP_DAQrun->HV_set[iHV];
      if (!startOfRun) {
	if ((int)info->FCP_DAQrun->HV_set[iHV] != (int)HV_set_prev[iHV]) {
	  printf("Error:  HV settings changed during run.  Was %d, now %d\n", (int)HV_set_prev[iHV], (int)info->FCP_DAQrun->HV_set[iHV] );
	  char buf[200];
	  sprintf(buf, "Error:  HV settings changed during run.  Was %d, now %d", (int)HV_set_prev[iHV], (int)info->FCP_DAQrun->HV_set[iHV]); 
	  printToLogfile(buf);
	  openErrorWindow(info, "HV settings changed during run");
	}
      }
      //  printf("HV_set[%d] = %f\n", iHV,  info->FCP_DAQrun->HV_set[iHV]);
    }
    for (int iHV = 0; iHV < NHV_read; iHV++) {
      g_spillHeader->HV_read[iHV] = info->FCP_DAQrun->HV_read[iHV];
      //  printf("HV_read[%d] = %f\n", iHV,  info->FCP_DAQrun->HV_read[iHV]);
    }
  }

  info->FCP_DAQrun->BeamStatus = 0xdeadbeef;

  return 0;
}


int writeSpillHeader(FCP_DAQ_Info_t* info) {

  if (!info->FCP_DAQrun->fout_allch) {
    printf("Error: attempting to write spill header but file is not open\n");
    printToLogfile("Error: attempting to write spill header but file is not open");
    openErrorWindow(info, "Error writing spill header", 1);
    return -1;
  }

  // check if file size is larger than limit.  If so, close this file and open
  // a new one
  off_t len = (off_t)ftell(info->FCP_DAQrun->fout_allch);
  if (len > info->MaxFileSize) {
    fclose(info->FCP_DAQrun->fout_allch);
    info->FCP_DAQrun->fout_allch = 0;
    openOutputFile(info);
  }

  // initialize number of event to 0
  //  g_spillHeader->NumEvents = 0;

  time_t rawtime;
  time(&rawtime);
  g_spillHeader->spillStartTime = rawtime;
  
  int iBoard;
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    g_spillHeader->boardFailBits[iBoard] = info->FCP_DAQrun->boardFailBits[iBoard];
  }

  int iHV;
  
  for (iHV = 0; iHV < NHV_set; iHV++) {
    g_spillHeader->HV_set[iHV] = info->FCP_DAQrun->HV_set[iHV];
  }
  for (iHV = 0; iHV < NHV_read; iHV++) {
    g_spillHeader->HV_read[iHV] = info->FCP_DAQrun->HV_read[iHV];
  }

  g_spillHeader->BeamStatus = info->FCP_DAQrun->BeamStatus;

  // Root tree version.  Would be one file per spill
  if (f_root == 0 ) {
 
    //                sprintf(fname, "wave%d.dat", ch);
    /* old naming convention
    time_t rawtime;
    time(&rawtime);
    char timestamp[200];
    sprintf(timestamp, "%s",  ctime(&rawtime));
    // remove newline
    int len = strlen(timestamp);
    if( timestamp[len-1] == '\n' )
      timestamp[len-1] = 0;
 
    sprintf(fname, "%s/FCP_data_%s.root", OUTPUT_TOPLEVEL_DIRECTORY, timestamp);
    //                if ((FCP_DAQrun->fout[ch] = fopen(fname, "wb")) == NULL)
    //    return -1;
    char *p = fname;
    for (; *p; ++p)
      {
	if (*p == ' ')
	  *p = '_';
      }
    //printf("Opening a new root file\n");
    */
 
    //  get spill number by parsing beam_status string
    const char s[2] = " ";
    char beam_status_copy[200];
    sprintf(beam_status_copy, "%s", info->beam_status);
    char *spillNumStr = strtok(beam_status_copy, s);
    spillNumStr = strtok(NULL, s);
    if (spillNumStr == NULL) {
      printf("Error getting spill number from beam_status %s\n", info->beam_status);
      printToLogfile("Error getting spill number");
    } else {
      //    printf("Spill number is %s\n", spillNumStr);
      info->FCP_DAQrun->spillNumber = atoi(spillNumStr);  
    };
    g_spillHeader->spillNumber = info->FCP_DAQrun->spillNumber;

    sprintf(root_simplefname, "%s/all/run%dspill%d.root", OUTPUT_TOPLEVEL_DIRECTORY, g_runHeader->runNumber, g_spillHeader->spillNumber);
    char Sr90status[3];
    // NOTE logic is flipped here.  We alraedy started taking data this way so just leave it.  Can fix everything offline later
    // Fix for 2022 (starting with DAQ version 10)!
    if (g_runHeader->sourceInBeam == 0) {
      sprintf(Sr90status, "%s", "out");
    } else if (g_runHeader->sourceInBeam == 1) {
      sprintf(Sr90status, "%s", "in");
    } else {
      sprintf(Sr90status, "%s", "unk");
    }

    sprintf(root_fname, "%s/Sr90%s/HV%d/run%dspill%d.root", OUTPUT_TOPLEVEL_DIRECTORY, Sr90status,  (int)g_spillHeader->HV_set[1], g_runHeader->runNumber, g_spillHeader->spillNumber );    
    char buf[200];
    sprintf(buf, "Opening new root file %s", root_fname);
    printToLogfile(buf);
    // check to see if file already exists.  If so, throw an error
    std::ifstream test_file(root_simplefname);
    if (test_file.good()) {
      printf("Error: trying to open a data file that already exists: %s!\n", root_simplefname);
      addError(info->FCP_DAQrun, 0, ERR_DATA_WRITE);
      openErrorWindow(info, "Trying to open a data file that already exists", 1);
    } else { 
      char mkdircmd[200];
      sprintf(mkdircmd, "mkdir %s/Sr90%s/HV%d >& /dev/null", OUTPUT_TOPLEVEL_DIRECTORY, Sr90status,  (int)g_spillHeader->HV_set[1]);
      //      printf("Trying %s\n", mkdircmd);
      system(mkdircmd);
      sprintf(mkdircmd, "chmod 777 %s/Sr90%s/HV%d >& /dev/null", OUTPUT_TOPLEVEL_DIRECTORY, Sr90status,  (int)g_spillHeader->HV_set[1]);
      f_root = new TFile(root_fname,"NEW");
      if (f_root == 0) {
	printf("Error opening root output file\n");
	printToLogfile("Error opening root output file");
	openErrorWindow(info, "Error opening root output file", 1);
      }
       
      if (t_event != 0) {
	t_event->SetDirectory(f_root);
	t_event->Reset() ;
      } else {
	printf("What am I doing here?\n");
	t_event = new TTree("Digitizer_event", "Digitizer_event");
	//     t_event->SetDirectory(f_root);
      } 
    } 
    
  } else {
    printf("Error: root file already open at start of spill\n");
    printToLogfile("Error: root file already open at start of spill");
    openErrorWindow(info, "Error: root file already open at start of spill", 1);    
  }

  //t_run = new TTree("FCP_DAQ_run", "Tree for run-level information");
  //t_spill = new TTree("FCP_DAQ_spill", "Tree for spill-level information");  
  if (b_runNumber == 0) {
    b_runNumber = t_event->Branch("runNumber", &g_runHeader->runNumber);
  } else {
    // b_spillNumber->SetAddress( &g_spillHeader->spillNumber);
  }

  if (b_spillNumber == 0) {
    b_spillNumber = t_event->Branch("spillNumber", &g_spillHeader->spillNumber);
  } else {
    // b_spillNumber->SetAddress( &g_spillHeader->spillNumber);
  }
  TDatime ttime;
  ttime.Set(g_spillHeader->spillStartTime);
  if (b_spillStartTime == 0) {    
    b_spillStartTime = t_event->Branch("spillStartTime", &ttime);
  } else {
    //b_spillStartTime->SetAddress( &g_spillHeader->spillStartTime);
  }
  if (b_BeamStatus == 0) {
    b_BeamStatus = t_event->Branch("BeamStatus", &g_spillHeader->BeamStatus);
  } else {
    //b_BeamStatus->SetAddress( &g_spillHeader->BeamStatus);
  }
  char arr_descrip[200]; 
  if (b_digitizerFailBits == 0) {
    sprintf(arr_descrip, "boardFailBits[%d]/i", NUM_ADC_BOARDS);
    b_digitizerFailBits = t_event->Branch("digitizerFailBits", g_spillHeader->boardFailBits, arr_descrip);
  } else {
    //b_boardFailBits->SetAddress(g_spillHeader->boardFailBits);
  }
  if (b_SCal_HV_set == 0) {
    b_SCal_HV_set = t_event->Branch("SCal_HV_set", &g_spillHeader->HV_set[0]);
  } 
  if (b_SCal_HV_read == 0) {
    b_SCal_HV_read = t_event->Branch("SCal_HV_read", &g_spillHeader->HV_read[0]);
  } 
  if (b_FCal_inner_HV_set == 0) {
    b_FCal_inner_HV_set = t_event->Branch("FCal_inner_HV_set", &g_spillHeader->HV_set[1]);
  } 
  if (b_FCal_tubeA_HV_read == 0) {
    b_FCal_tubeA_HV_read = t_event->Branch("FCal_tubeA_HV_read", &g_spillHeader->HV_read[1]);
  }
  if (b_FCal_outer_HV_set == 0) {
    b_FCal_outer_HV_set = t_event->Branch("FCal_outer_HV_set", &g_spillHeader->HV_set[2]);
  } 
  if (b_FCal_tubeB_HV_read == 0) {
    b_FCal_tubeB_HV_read = t_event->Branch("FCal_tubeB_HV_read", &g_spillHeader->HV_read[2]);
  }
  if (b_FCal_tubeC_HV_read == 0) {
    b_FCal_tubeC_HV_read = t_event->Branch("FCal_tubeC_HV_read", &g_spillHeader->HV_read[3]);
  }

  return 0;
}

static void
activate (GtkApplication *app,
          gpointer        gp)
{
  GtkWidget *window;
  GtkWidget *dead_button, *start_button, *stop_button, *pause_button, *exit_button, *scintChoice0_button, *scintChoice1_button, *assemblyInBeam0_button, *assemblyInBeam1_button, *includeTailVeto_button, *ShCalPed0_button, *ShCalPed1_button;
  GtkWidget *nevents_bar[NUM_ADC_BOARDS], *BPC_nevents_bar;
  GtkWidget *grid;
  GtkWidget *space, *space2, *space3, *space4, *nevents_bar_label[NUM_ADC_BOARDS], *nevents_bar_label_min[NUM_ADC_BOARDS], *nevents_bar_label_max[NUM_ADC_BOARDS], *BPC_nevents_bar_label, *BPC_nevents_bar_label_min, *BPC_nevents_bar_label_max;
  GtkWidget *beam_status_label, *scint_choice_label, *run_status_label, *disk_space_label, *assembly_in_beam_label, *ShCalPed_label;
  GtkWidget *hline, *hline2, *hline3, *hline4, *hline5;
  
  int iBoard;
  
  FCP_DAQ_Info_t* info = (FCP_DAQ_Info_t*)gp;
  
  SetupConfiguration(info->FCP_DAQcfg);
  
  // open socket for receiving beam status information                     
  openSocket(info);

  CAEN_DGTZ_ErrorCode digi_init_err = initializeDigitizers(info);

  window = gtk_application_window_new (app);
  gtk_window_set_deletable(GTK_WINDOW(window), FALSE);
  if (sim_mode == 1) {
    gtk_window_set_title (GTK_WINDOW (window), "FCP_DAQ simulation mode.  No data will be taken!");
  } else {
    gtk_window_set_title (GTK_WINDOW (window), "FCP_DAQ");
  }
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);
  gtk_window_set_position((GtkWindow*)window, GTK_WIN_POS_CENTER);
  info->window = window;
  
  grid = gtk_grid_new();
  gtk_container_add (GTK_CONTAINER (window), grid);
  
  dead_button = gtk_button_new();
  pause_button =  gtk_button_new_with_label ("Pause run");
  start_button = gtk_button_new_with_label ("Start run");
  //  pause_button =  gtk_button_new_with_label ("Pause run");
  stop_button = gtk_button_new_with_label ("End run");
  exit_button = gtk_button_new_with_label ("Exit");
  scintChoice0_button = gtk_radio_button_new_with_label(0, "Small");
  
  scintChoice1_button = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (scintChoice0_button), "Large");
  // by default small scintillators are chosen on the GUI, so write this to
  // the Arduino.  Due to a wiring inversion, "large scintillators" has this effect
  //  writeToSocket("large scintillators");

  ShCalPed0_button = gtk_radio_button_new_with_label(0, "Positive");
  ShCalPed1_button = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (ShCalPed0_button), "Negative");
  
  assemblyInBeam0_button = gtk_radio_button_new_with_label(0, "Upper assembly (without source)");
  assemblyInBeam1_button = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (assemblyInBeam0_button), "Lower assembly (with source)");
  
  includeTailVeto_button = gtk_check_button_new_with_label("Include tail veto in trigger");
  gtk_toggle_button_set_active((GtkToggleButton*)includeTailVeto_button, TRUE);
  // by default the tail veto is not checked on the GUI, so write this to the
  // Arduino
  // writeToSocket("no halo trigger veto");
  
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    nevents_bar[iBoard] = gtk_level_bar_new_for_interval(0., info->FCP_DAQcfg->NumEvents);
    gtk_widget_set_size_request(nevents_bar[iBoard], 450, 25);
    info->nevents_bar[iBoard]=nevents_bar[iBoard];
    gtk_level_bar_set_value((GtkLevelBar*)nevents_bar[iBoard], 0.);
  }
  
  BPC_nevents_bar = gtk_level_bar_new_for_interval(0., info->FCP_DAQcfg->NumEvents);
  gtk_widget_set_size_request(BPC_nevents_bar, 450, 25);
  info->BPC_nevents_bar=BPC_nevents_bar;
  gtk_level_bar_set_value((GtkLevelBar*)BPC_nevents_bar, 0.);
  
  info->start=start_button;
  info->pause=pause_button;
  info->stop=stop_button;
  info->exit=exit_button;
  info->scintChoice0=scintChoice0_button;
  info->scintChoice1=scintChoice1_button;
  info->ShCalPed0=ShCalPed0_button;
  info->ShCalPed1=ShCalPed1_button;
  info->assemblyInBeam0=assemblyInBeam0_button;
  info->assemblyInBeam1=assemblyInBeam1_button;
  info->includeTailVeto=includeTailVeto_button;
  
  g_signal_connect (start_button, "clicked", G_CALLBACK (GuiStart), gp);
  //  g_signal_connect_swapped (button, "clicked", G_CALLBACK (gtk_widget_destroy), window);
  g_signal_connect (pause_button, "clicked", G_CALLBACK (GuiPause), gp);
  g_signal_connect (stop_button, "clicked", G_CALLBACK (GuiStop), gp);
  g_signal_connect (exit_button, "clicked", G_CALLBACK (GuiExit), gp);
  g_signal_connect (scintChoice0_button, "clicked", G_CALLBACK (GuiScintChoice), gp);
  g_signal_connect (scintChoice1_button, "clicked", G_CALLBACK (GuiScintChoice), gp);
  g_signal_connect (ShCalPed0_button, "clicked", G_CALLBACK (GuiScintChoice), gp);
  g_signal_connect (ShCalPed1_button, "clicked", G_CALLBACK (GuiScintChoice), gp);
  g_signal_connect (assemblyInBeam0_button, "clicked", G_CALLBACK (GuiAssemblyInBeam), gp);
  g_signal_connect (assemblyInBeam1_button, "clicked", G_CALLBACK (GuiAssemblyInBeam), gp);
  g_signal_connect (includeTailVeto_button, "clicked", G_CALLBACK (GuiIncludeTailVeto), gp);
  space = gtk_label_new("");
  space2 = gtk_label_new("");
  space3 = gtk_label_new("");
  space4 = gtk_label_new("");
  
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    char label[50];
    sprintf(label, "Events in last spill for digitizer %s:  N/A", digitizerName[iBoard]);
    nevents_bar_label[iBoard] = gtk_label_new(label);
    info->nevents_bar_label[iBoard] = nevents_bar_label[iBoard];
    gtk_label_set_justify(GTK_LABEL(nevents_bar_label[iBoard]), GTK_JUSTIFY_CENTER);
    nevents_bar_label_min[iBoard] = gtk_label_new("0");
    char txt[100];
    sprintf(txt, "%d", info->FCP_DAQcfg->NumEvents);
    nevents_bar_label_max[iBoard] = gtk_label_new(txt);
    
    gtk_label_set_xalign((GtkLabel*)nevents_bar_label_min[iBoard], 0.0);
    gtk_label_set_xalign((GtkLabel*)nevents_bar_label_max[iBoard], 1.0);
    
  }
  
  char label[50];
  sprintf(label, "BPC events in last spill: N/A");
  BPC_nevents_bar_label = gtk_label_new(label);
  gtk_label_set_justify(GTK_LABEL(BPC_nevents_bar_label), GTK_JUSTIFY_CENTER);
  info->BPC_nevents_bar_label = BPC_nevents_bar_label;
  BPC_nevents_bar_label_min = gtk_label_new("0");
  char txt[100];
  sprintf(txt, "%d", info->FCP_DAQcfg->NumEvents);
  BPC_nevents_bar_label_max = gtk_label_new(txt);
  
  gtk_label_set_xalign((GtkLabel*)BPC_nevents_bar_label_min, 0.0);
  gtk_label_set_xalign((GtkLabel*)BPC_nevents_bar_label_max, 1.0);
  
  beam_status_label = gtk_label_new("Beam status: Unknown");
  run_status_label = gtk_label_new("No run in progress");
  assembly_in_beam_label = gtk_label_new("Sr90 in beam: Unknown");
  float fs =  freeDiskSpace();
  sprintf(txt, "Disk space available: %6.2f Gb", fs);
  disk_space_label = gtk_label_new(txt);
  scint_choice_label = gtk_label_new("Trigger scintillators to use:");
  
  ShCalPed_label = gtk_label_new("What is the ShCal HV polarity?");
  
  gtk_label_set_xalign((GtkLabel*)beam_status_label, 0.5);
  info->beam_status_label=beam_status_label;
  gtk_label_set_xalign((GtkLabel*)run_status_label, 0.5);
  info->run_status_label=run_status_label;

  info->assembly_in_beam_label = assembly_in_beam_label;
  info->disk_space_label = disk_space_label;

  hline = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
  hline2 = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
  hline3 = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
  hline4 = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
  hline5 = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
  
  int row = 0;
  
  gtk_grid_attach (GTK_GRID (grid), run_status_label, 0, row, 4, 1);
  row++;
  row++;
  gtk_grid_attach (GTK_GRID (grid), beam_status_label, 0, row, 4, 1);
  row++;
  row++;
  gtk_grid_attach (GTK_GRID (grid), assembly_in_beam_label, 0, row, 4, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), disk_space_label, 0, row, 4, 1);
  row++;
  row++;
  gtk_grid_attach (GTK_GRID (grid), start_button, 0, row, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), dead_button, 0, row, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), pause_button, 1, row, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), stop_button, 2, row, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), exit_button, 3, row, 1, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), space, 0, row, 4, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), hline, 0, row, 4, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), space2, 0, row, 4, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), scint_choice_label, 0, row, 2, 1);
  gtk_grid_attach (GTK_GRID (grid), scintChoice0_button, 2, row, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), scintChoice1_button, 3, row, 1, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), hline4, 0, row, 4, 1);
  gtk_grid_attach (GTK_GRID (grid), ShCalPed_label, 0, row, 2, 1);
  gtk_grid_attach (GTK_GRID (grid), ShCalPed0_button, 2, row, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), ShCalPed1_button, 3, row, 1, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), hline5, 0, row, 4, 1);
  //  row++;
  //gtk_grid_attach (GTK_GRID (grid), assembly_in_beam_label, 0, row, 2, 1);
  //gtk_grid_attach (GTK_GRID (grid), assemblyInBeam0_button, 2, row, 1, 1);
  //gtk_grid_attach (GTK_GRID (grid), assemblyInBeam1_button, 3, row, 1, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), hline2, 0, row, 4, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), space3, 0, row, 4, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), includeTailVeto_button, 0, row, 4, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), hline3, 0, row, 4, 1);
  row++;
  gtk_grid_attach (GTK_GRID (grid), space4, 0, row, 4, 1);
  row++;
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    int iDisp;
    if (iBoard == 0) iDisp = DIGITIZER_1_HANDLE;
    if (iBoard == 1) iDisp = DIGITIZER_2_HANDLE;
    if (iBoard == 2) iDisp = DIGITIZER_3_HANDLE;
    if (iBoard == 3) iDisp = DIGITIZER_4_HANDLE;

    gtk_grid_attach (GTK_GRID (grid), nevents_bar_label[iDisp], 0, row, 4, 1);
    row++;
    gtk_grid_attach (GTK_GRID (grid), nevents_bar[iDisp], 0, row, 4, 1);
    row++;
    gtk_grid_attach (GTK_GRID (grid), nevents_bar_label_min[iDisp], 0, row, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), nevents_bar_label_max[iDisp], 3, row, 1, 1);
    row++;
  }
  
  if (merge_BPC_data == 1) {
    gtk_grid_attach (GTK_GRID (grid), BPC_nevents_bar_label, 0, row, 4, 1);
    row++;
    gtk_grid_attach (GTK_GRID (grid), BPC_nevents_bar, 0, row, 4, 1);
    row++;
    gtk_grid_attach (GTK_GRID (grid), BPC_nevents_bar_label_min, 0, row, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), BPC_nevents_bar_label_max, 3, row, 1, 1);
    row++;
  }
  
  //  info->mainWindowRows = row;
  
  gtk_widget_set_sensitive(stop_button, FALSE);
  gtk_widget_set_sensitive(pause_button, FALSE);
  gtk_widget_show_all (window);
  //gtk_widget_show_all (barwindow);

  if (digi_init_err != CAEN_DGTZ_Success) {
    if (sim_mode == 0) openErrorWindow(info, "Error initializing digitizers", 1);
  }
  //initializeDigitizers(info);

  //  readFromSocket(info);
  //writeToSocket("large scintillators");
  //writeToSocket("no halo trigger veto");
  if (info->beam_status_thread_ID == 0) {
    info->beam_status_thread_ID = g_timeout_add(10, readFromSocket, gp);
  }

    //create thread for analysis
  if (do_analysis != 0) {
    pthread_create(&info->FCP_DAQrun->AnalysisThread, NULL, analysisLoop, (void *)&info);
  }

  g_object_unref (app);

  if (do_analysis != 0) {
    for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
      free(info->analysisBuffer_ADC[iBoard]);
    }
  }
}

uint64_t PrevRateTime = 0;
int Nb[NUM_ADC_BOARDS]={NUM_ADC_BOARDS*0}, Ne[NUM_ADC_BOARDS] = {NUM_ADC_BOARDS*0};

/* Here we monitor the beam status (i.e. spill in progress or not) and take
   appropriate action upon transitions */

gboolean mainDAQLoop( FCP_DAQ_Info_t *info) {

  //uint32_t ret = 0;
  int iBoard;

  // only take data during spill
  if (strstr(info->beam_status, "spill") == NULL) {
  
    //    printf("AcqRun = %d\n", info->FCP_DAQrun->AcqRun);
    if (info->FCP_DAQrun->AcqRun == 1) {

      // send message to disable triggers to allow readout to complete
      char buf[200];
      sprintf(buf, "readout started");
      writeToSocket(buf);  
      /*
      for (int iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) { 
      	gtk_label_set_text((GtkLabel*)info->nevents_bar_label[iBoard], "Readout in progress...\n");
	gtk_level_bar_set_value((GtkLevelBar*)info->nevents_bar[iBoard], 0.);
	gtk_widget_show(info->nevents_bar_label[iBoard]);
	//      	printf("GOT HERE\n");
      }
      */
      //      TStopwatch tsw;
      //tsw.Start();

      //time_t rawtime;
      //struct tm * timeinfo;
      
      //time ( &rawtime );
      //timeinfo = localtime ( &rawtime );
      //printf ( "Spill ended at: %s", asctime (timeinfo) );
      char msg[200];
      sprintf(msg, "End of spill %d", info->FCP_DAQrun->spillNumber);
      printToLogfile(msg);
      //printf("Stopping DAQ because beam_status = %s\n", info->beam_status);
      // do not stop acquisition after every spill; trust that no out-of-spill       // triggers will come
      // for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
      //ret |= CAEN_DGTZ_SWStopAcquisition(info->handle_ADC[iBoard]);
      //if (ret != 0 && sim_mode == 0) {
      // //	  gtk_widget_set_sensitive (info->exit, TRUE);
      //  sprintf(buf, "Error stopping data aqcuisition for handle %d: %d\n", info->handle_ADC[iBoard], ret);
      //	  printToLogfile(buf);

      //  return FALSE;
      //	}
      //}

      printToLogfile("Digitizer readout begin");
      for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
	if (do_analysis != 0 ) {
	  if (info->FCP_DAQrun->DataCopyInProgress == 1) {
	    sleep(1);  // wait for data copy in analysisLoop() to complete
	    // if it can't finish in a second, go on without it 
	  }
	  if (info->FCP_DAQrun->DataCopyInProgress == 1) {
	    printf("Warning: analysisLoop() seems to be having trouble copying data.  Proceeding with digitizer readout\n");
	    printToLogfile("Warning: analysisLoop() seems to be having trouble copying data.  Proceeding with digitizer readout");
	  }
	}
	//	if (sim_mode == 0) {
	if (readoutData(info, iBoard) != 0 ) {
	  return FALSE;
	} 
	  //}
      }
      printToLogfile("Digitizer readout ends");

      //  tsw.Stop();
      //  printf ("Took %f seconds to readout digitizers\n", tsw.RealTime());
      // tsw.Continue();

      // data all read out, so set flags for analysis thread
      info->FCP_DAQrun->NewDataToAnalyze = 1;
      if (writeData(info) != 0) {
	printf("Error writing channel data\n");
	printToLogfile("Error writing channel data");
	openErrorWindow(info, "Error writing channel data", 1);
	//GuiStop((GtkButton*)info->stop, info);
	return FALSE;
      }

      info->nTriggersInRun += g_spillHeader->NumEvents;

      if ( info->FCP_DAQrun->InRun ) {
	sprintf(buf, "<span foreground=\"#ff0044\">Run %d in progress: %d triggers recorded</span>", g_runHeader->runNumber, info->nTriggersInRun);
	//gtk_label_set_text((GtkLabel*)info->run_status_label, buf);
	gtk_label_set_markup (GTK_LABEL (info->run_status_label), buf);
      }

      if (writeEndOfSpillRecord(info) != 0) {
	printf("Error writing end of spill record\n");
	printToLogfile("Error writing end of spill record");
	return FALSE;
      }
   
      if (updateConditions(info) != 0) {
	printf("Error updating conditions information\n");
	printToLogfile("Error updating conditions information");
	return FALSE;
      }
      //      analysisLoop(info);
      //printf("Done with analysisLoop\n");
      info->FCP_DAQrun->AcqRun = 0;

      //tsw.Stop();
      //printf ("Took %f seconds to do end-of-spill tasks\n", tsw.RealTime());

      bool runResumed = false;
      while (!runResumed) {
	// update beam status info
	readFromSocket(info);	
	if (strstr(info->beam_status, "spill") == NULL) {
	  char buf[200];
	  sprintf(buf, "readout ended");
	  writeToSocket(buf);
	  runResumed = true;
	}
	usleep(10000);
      }
    }

    // clear "StartPushedDuringSpill" flag to ensure that data collected
    // begins at the following spill
    if (info->FCP_DAQrun->StartPushedDuringSpill == 1) {
      info->FCP_DAQrun->StartPushedDuringSpill = 0;
      startRun(info);
    }

    if (info->FCP_DAQrun->PauseRun == 1) {
      pauseRun(info);
      return FALSE;
    }

    if (info->FCP_DAQrun->EndRun == 1) {
      // printf("Calling stopRun\n");
      stopRun(info);
      return FALSE;
    }
  } 

  // char buf[200];
  if (strstr(info->beam_status, "spill") != NULL) {

      
    if ((info->FCP_DAQrun->AcqRun == 0) && (info->FCP_DAQrun->StartPushedDuringSpill == 0) && (info->FCP_DAQrun->PauseRun != 1)) {
      //      printf("Firing up the DAQ\n");

      //time how long this takes
      //TStopwatch tsw;
      //tsw.Start();

      // now get the digitizers ready
      for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
	// reset time counter and also emtpy memory
	ResetTimeStamp(info->handle_ADC[iBoard]);

	// check if memory is full -- shouldn't be at this point!
	int ich;
	
	for (ich = 0; ich < 8; ich++) {
	  uint32_t addr = 0x1088;
	  addr += ich << 8;
	  uint32_t chanstatus;
	  CAEN_DGTZ_ReadRegister(info->handle_ADC[iBoard], addr, &chanstatus);
	  if (chanstatus & 0x1) {
	    printf("Memory full for channel %d\n", ich);
	    char buf[200];
	    sprintf(buf, "Memory full for channel %d\n", ich);
	    printToLogfile(buf);
	  } 
	}
      }
    
      for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
	//CAEN_DGTZ_ClearData(info->handle_ADC[iBoard]);
	//	uint32_t ret = CAEN_DGTZ_SWStartAcquisition(info->handle_ADC[iBoard]);
	//	if (ret != 0) {
	//  sprintf(buf, "Error starting data aqcuisition for handle %d: %d\n", info->handle_ADC[iBoard], ret);
	//  printToLogfile(buf);
	//	}
      }	

      // check that there's no "leftover" data from a previous spill in the
      // BPC buffer
      char tag[TAGSIZE+1] = {"NULL"};
      int nb;
      int rc = check_head(tag, &nb);
      if (rc > 0) {
	get_data(BPC_data_buf, nb);
	if (strcmp(tag, "BPCDATA") == 0) {
	  printToLogfile("Warning: BPC data from previous spill found");
	} else {
	  char msgbuf[200];
	  sprintf(msgbuf, "Warning: BPC message from previous spill found with tag %s", tag);
	  printToLogfile(msgbuf);
	}
      }
      if (rc < 0) {
	printToLogfile("Error communicating with ControlHost when checking for data from previous spill");
      }

      info->FCP_DAQrun->AcqRun = 1;
      //prepare for next spill by 
      //writing spill header information

      if (writeSpillHeader(info) != 0) {
	printf("Error writing spill header\n");
	printToLogfile("Error writing spill header");
	return FALSE;
      }
      //tsw.Stop();
      //printf ("Took %f seconds to prepare for spill\n", tsw.RealTime());
    }
  }
  return TRUE;
}

int readoutData(FCP_DAQ_Info_t* info, int iBoard) {

  int32_t ret = 0;
  ERROR_CODES ErrCode= ERR_NONE;
	  
  /* Read data from the boards */
  
  CheckDigitizerFailureStatus(info, info->handle_ADC[iBoard]);
  // printf("Checking if readout buffer is available:  first byte is %x\n", *(char*)(info->buffer_ADC[iBoard])[0]);
  //printf("Buffer size is %d\n", info->BufferSize_ADC[iBoard]);
  ret = CAEN_DGTZ_ReadData(info->handle_ADC[iBoard], CAEN_DGTZ_SLAVE_TERMINATED_READOUT_MBLT, info->buffer_ADC[iBoard], &info->BufferSize_ADC[iBoard]);
  if (ret) {
      
    ErrCode = ERR_READOUT;
    printf("Error reading out digitizer %d with handle %d: %d\n", iBoard, info->handle_ADC[iBoard], ret);
    printf("Buffer size is %d\n", info->BufferSize_ADC[iBoard]);
    char buf[200];
    sprintf(buf, "Error reading out digitizer %d with handle %d: %d\n", iBoard, info->handle_ADC[iBoard], ret);
    printToLogfile(buf);
    addError(info->FCP_DAQrun, 0, ERR_READOUT);
    CheckDigitizerFailureStatus(info, info->handle_ADC[iBoard]);
    openErrorWindow(info, buf, 1);
    return ErrCode;
  }
	
  //  printf("BufferSize[%d] = %d\n", iBoard, info->BufferSize_ADC[iBoard]);
  char buf[200];
  sprintf(buf, "BufferSize[%d] = %d", iBoard, info->BufferSize_ADC[iBoard]);
  printToLogfile(buf);
  if (info->BufferSize_ADC[iBoard] != 0) {
    ret = CAEN_DGTZ_GetNumEvents(info->handle_ADC[iBoard], info->buffer_ADC[iBoard], info->BufferSize_ADC[iBoard], &g_spillHeader->NumEvents);
    //    printf("Read %d events\n", NumEvents);
    sprintf(buf, "Read %d events", g_spillHeader->NumEvents);
    printToLogfile(buf);
  }

  ret = CAEN_DGTZ_ClearData(info->handle_ADC[iBoard]);

  return 0;
}

int writeData(FCP_DAQ_Info_t* info) {

  if (info->FCP_DAQrun->fout_allch == 0) {
    printf("Error: file not open when attempting to write event data\n");
    printToLogfile("Error: file not open when attempting to write event data");
    openErrorWindow(info, "File not open when attempting to write event data", 1);
    return -1;
  }

  // write out total data size for this spill, so analysis software knows when
  // to stop reading
  
  
  uint32_t PrevNumEvents = 0, NumEventsExp;
  uint32_t ret;

  printToLogfile("Formatting data...");

  NumEventsExp=info->FCP_DAQcfg->NumEvents;

  int iBoard;
  int nNumTrigErrors = 0;
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    
    if (info->BufferSize_ADC[iBoard] != 0) {
      // printf("Buffer size was nonzero...");
      ret = CAEN_DGTZ_GetNumEvents(iBoard, info->buffer_ADC[iBoard], info->BufferSize_ADC[iBoard], &g_spillHeader->NumEvents);
      // printf("and there were %d events\n", g_spillHeader->NumEvents);
      if (ret != 0) {
	printf("Error %d getting number of events\n", ret);
	printToLogfile("Error getting number of events");
	return ret;
      }
    } else {
      g_spillHeader->NumEvents = 0;
    }
  
    char label[100];
    sprintf(label, "Events in last spill for digitizer %s: %d\n", digitizerName[iBoard], g_spillHeader->NumEvents);
    gtk_label_set_text((GtkLabel*)info->nevents_bar_label[iBoard], label);
    gtk_level_bar_set_value((GtkLevelBar*)info->nevents_bar[iBoard], g_spillHeader->NumEvents); 
    sprintf(label, "Disk space available: %6.2f Gb", freeDiskSpace());
    gtk_label_set_text((GtkLabel*)info->disk_space_label, label);
    //    printf("AND NOW HERE\n");
    if (iBoard == 0) {
      PrevNumEvents = g_spillHeader->NumEvents;
    } else {
      if (g_spillHeader->NumEvents != PrevNumEvents) {
	nNumTrigErrors++;
	printf("Number of events current = %d past = %d\n", g_spillHeader->NumEvents, PrevNumEvents);
	if (PrevNumEvents < g_spillHeader->NumEvents) {
	  g_spillHeader->NumEvents = PrevNumEvents;
	}
	PrevNumEvents = g_spillHeader->NumEvents;
	// set number of events to the minimum seen by any board to prevent overdflows

      } else {
	PrevNumEvents = g_spillHeader->NumEvents;
      }
    }  
  
    if (g_spillHeader->NumEvents > NumEventsExp) {
      printf("Error: only expected %d events but read %d; events after event %d will not be written\n", NumEventsExp, g_spillHeader->NumEvents, NumEventsExp);
      char buf[200];
      sprintf(buf, "Error: only expected %d events but read %d; events after event %d will not be written", NumEventsExp, g_spillHeader->NumEvents, NumEventsExp);
      printToLogfile(buf);
      openErrorWindow(info, buf);
    }
    //    if (NumEvents < NumEventsExp) {
    //  printf("Warning: expected %d events but only read %d; perhaps trigger rate is too low?\n", NumEventsExp, NumEvents);
    //}
  }

  if (nNumTrigErrors > 0) {
    printf("Error: different numbers of events in different digitizers; will not record data for this spill\n");
    printToLogfile("Error: different numbers of events in different digitizers; will not record data for this spill"); 
    openErrorWindow(info, "Different numbers of events in different digitizers; will not record data for this spill");
  }

  BPC_Data* BPC_data_sample = 0;
  bool good_BPC_data = false;

  // get BPC data, if operating in that mode
  if (merge_BPC_data == 1 && g_spillHeader->NumEvents > 0) {
    printToLogfile("Getting BPC data...");
    int ntries = 0, maxtries = 500;
    char tag[TAGSIZE+1] = {"NULL"};

    int nb;
    
    int rc = 0;
    while ( (ntries < maxtries) && ( strcmp(tag, "BPCDATA") != 0 )) {
       usleep(10000);
       rc = check_head(tag, &nb);
       if (rc > 0 ) {
	 if  ( strcmp(tag, "BPCDATA") == 0 ) {
	   //will read the data below in this case
	   char msgbuf[200];
	   sprintf(msgbuf, "Found %d bytes of BPC data with tag %s when looking for data", nb, tag);
	   printToLogfile(msgbuf);
	 } else {
	   char msgbuf[200];
	   sprintf(msgbuf, "Unexpected tag when looking for BPC data: %s", tag);
	   printToLogfile(msgbuf);
	   get_data(BPC_data_buf_junk, nb);

	 }
       }
       if (rc < 1) {
	 printToLogfile("Error communicating with ControlHost when looking for BPC data");
	 break;
       }
       ntries++;
     }
	
     //wait_head(tag, &nb);
    if (ntries >= maxtries) {
      printf("Error receiving BPC data\n");
      printToLogfile("Error receiving BPC data");
    } else {
      get_data(BPC_data_buf, nb);
    
      // now check data validity and add BPC data branches
      // note that we assume that the digitizers and BPC saw the same set 
      // of trigger signals.  This can be checked offline and corrected 
      // if needed
      BPC_data_sample = (BPC_Data*)&BPC_data_buf[0];
      if (BPC_data_sample->tag != 0xFACE) { // ||
	  //	  BPC_data_sample->runNumber != g_runHeader->runNumber ||
	  //BPC_data_sample->spillNumber != g_spillHeader->spillNumber) {
	printf("Error: BPC header data not in correct format\n");
	printf("tag = %x\n", BPC_data_sample->tag);
	printf("runNumber = %i\n", BPC_data_sample->runNumber);
	printf("spillNumber = %i\n", BPC_data_sample->spillNumber);
	printToLogfile("Error: BPC data not in correct format");
      } else {
	BPC_data_sample++;
	good_BPC_data = true;
      }
    
    }
  } 

 
  printToLogfile("Starting root output");
  // Root tree output
  char arr_descrip[200];

  if (b_NumEventsMax == 0) {
    b_NumEventsMax = t_event->Branch("NumEventsMax", &g_runHeader->NumEvents);
  } else {
    // b_NumEvents->SetAddress(&NumEvents);
  }
  
  if (b_NumEventsInSpill == 0) {
    b_NumEventsInSpill = t_event->Branch("NumEventsInSpill", &g_spillHeader->NumEvents);
  } else {
    // b_NumEvents->SetAddress(&NumEvents);
  }
  int nchannels = NUM_ADC_BOARDS*info->FCP_DAQcfg->Nch;
  if (b_nchannels == 0) {
    b_nchannels = t_event->Branch("nchannels", &g_eventHeader->nchannels);
  } else {
    // b_nchannels->SetAddress(b_nchannels);
  }

  if (b_eventCounter == 0) { 
    b_eventCounter = t_event->Branch("eventCounter", &g_eventHeader->eventCounter);
  } else {
    //    b_eventCounter->SetAddress(&eventCounter);
  }
  if (b_triggerTimeTag == 0) {
    sprintf(arr_descrip, "triggerTimeTag[%d]/i", NUM_ADC_BOARDS);
    b_triggerTimeTag = t_event->Branch("triggerTimeTag", &g_eventHeader->triggerTimeTag, arr_descrip);
  } else {
    //b_triggerTimeTag->SetAddress(&triggerTimeTag);
  }
  // now a 2d vector (channel, samples) to hold the actual ADC data
  //  std::vector<std::vector<uint16_t>> ADC_data;
  g_ADC_data.resize(nchannels);
  // hard-coded for some testing...
  g_ADC_data.resize(32);

  //  for (int ich = 0; ich < nchannels; ich++) {
  //   ADC_data[ich].resize(info->FCP_DAQcfg.recordLength);
  //  }
  if (teststand_mode == 0) {
    // for (int iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    //  for (int iChan = 0; iChan < NUM_ADC_CHANNELS; iChan++) {
    //	sprintf(arr_descrip, "Digitizer_%d_channel_%d_data", iBoard, iChan);
    //b_ADC_data[iBoard][iChan] = t_event->Branch(arr_descrip, &g_ADC_data[(iBoard << 3) + iChan]);
    // }
    //}
    if (b_FCal_rod_front_source_data == 0) {
      b_FCal_rod_front_source_data = t_event->Branch("FCal_rod_front_source_data", &g_ADC_data[(FCAL_ROD_FRONT_SOURCE_DIGITIZER_HANDLE << 3) + FCAL_ROD_FRONT_SOURCE_CHANNEL]);
    }
    if (b_FCal_rod_mid_source_data == 0) {
      b_FCal_rod_mid_source_data = t_event->Branch("FCal_rod_mid_source_data", &g_ADC_data[(FCAL_ROD_MID_SOURCE_DIGITIZER_HANDLE << 3) + FCAL_ROD_MID_SOURCE_CHANNEL]);
    }
    if (b_FCal_rod_rear_source_data == 0) {
      b_FCal_rod_rear_source_data = t_event->Branch("FCal_rod_rear_source_data", &g_ADC_data[(FCAL_ROD_REAR_SOURCE_DIGITIZER_HANDLE << 3) + FCAL_ROD_REAR_SOURCE_CHANNEL]);
    }
    if (b_ShCal_front_source_quad_1_data == 0) {
      b_ShCal_front_source_quad_1_data = t_event->Branch("ShCal_front_source_quad_1_data",  &g_ADC_data[(SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_HANDLE << 3) + SHCAL_FRONT_SOURCE_QUAD_1_CHANNEL]);
    }
    if (b_ShCal_front_source_quad_2_data == 0) {
      b_ShCal_front_source_quad_2_data = t_event->Branch("ShCal_front_source_quad_2_data",  &g_ADC_data[(SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE << 3) + SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL]);
    }
    if (b_ShCal_front_source_quad_3_data == 0) {
      b_ShCal_front_source_quad_3_data = t_event->Branch("ShCal_front_source_quad_3_data",  &g_ADC_data[(SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_HANDLE << 3) + SHCAL_FRONT_SOURCE_QUAD_3_CHANNEL]);
    }
    if (b_ShCal_front_source_quad_4_data == 0) {
      b_ShCal_front_source_quad_4_data = t_event->Branch("ShCal_front_source_quad_4_data",  &g_ADC_data[(SHCAL_FRONT_SOURCE_QUAD_4_DIGITIZER_HANDLE << 3) + SHCAL_FRONT_SOURCE_QUAD_4_CHANNEL]);
    }
    if (b_ShCal_rear_source_quad_1_data == 0) {
      b_ShCal_rear_source_quad_1_data = t_event->Branch("ShCal_rear_source_quad_1_data",  &g_ADC_data[(SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_HANDLE << 3) + SHCAL_REAR_SOURCE_QUAD_1_CHANNEL]);
    }
    if (b_ShCal_rear_source_quad_2_data == 0) {
      b_ShCal_rear_source_quad_2_data = t_event->Branch("ShCal_rear_source_quad_2_data",  &g_ADC_data[(SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_HANDLE << 3) + SHCAL_REAR_SOURCE_QUAD_2_CHANNEL]);
    }
    if (b_ShCal_rear_source_quad_3_data == 0) {
      b_ShCal_rear_source_quad_3_data = t_event->Branch("ShCal_rear_source_quad_3_data",  &g_ADC_data[(SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_HANDLE << 3) + SHCAL_REAR_SOURCE_QUAD_3_CHANNEL]);
    }
    if (b_ShCal_rear_source_quad_4_data == 0) {
      b_ShCal_rear_source_quad_4_data = t_event->Branch("ShCal_rear_source_quad_4_data",  &g_ADC_data[(SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_HANDLE << 3) + SHCAL_REAR_SOURCE_QUAD_4_CHANNEL]);
    }
    
    if (b_FCal_rod_front_nosource_data == 0) {
      b_FCal_rod_front_nosource_data = t_event->Branch("FCal_rod_front_nosource_data", &g_ADC_data[(FCAL_ROD_FRONT_NOSOURCE_DIGITIZER_HANDLE << 3) + FCAL_ROD_FRONT_NOSOURCE_CHANNEL]);
    }
    if (b_FCal_rod_mid_nosource_data == 0) {
      b_FCal_rod_mid_nosource_data = t_event->Branch("FCal_rod_mid_nosource_data", &g_ADC_data[(FCAL_ROD_MID_NOSOURCE_DIGITIZER_HANDLE << 3) + FCAL_ROD_MID_NOSOURCE_CHANNEL]);
    }
    if (b_FCal_rod_rear_nosource_data == 0) {
      b_FCal_rod_rear_nosource_data = t_event->Branch("FCal_rod_rear_nosource_data", &g_ADC_data[(FCAL_ROD_REAR_NOSOURCE_DIGITIZER_HANDLE << 3) + FCAL_ROD_REAR_NOSOURCE_CHANNEL]);
    }
    if (b_ShCal_front_nosource_quad_1_data == 0) {
      b_ShCal_front_nosource_quad_1_data = t_event->Branch("ShCal_front_nosource_quad_1_data",  &g_ADC_data[(SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_HANDLE << 3) + SHCAL_FRONT_NOSOURCE_QUAD_1_CHANNEL]);
    }
    if (b_ShCal_front_nosource_quad_2_data == 0) {
      b_ShCal_front_nosource_quad_2_data = t_event->Branch("ShCal_front_nosource_quad_2_data",  &g_ADC_data[(SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_HANDLE << 3) + SHCAL_FRONT_NOSOURCE_QUAD_2_CHANNEL]);
    }
    if (b_ShCal_front_nosource_quad_3_data == 0) {
      b_ShCal_front_nosource_quad_3_data = t_event->Branch("ShCal_front_nosource_quad_3_data",  &g_ADC_data[(SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_HANDLE << 3) + SHCAL_FRONT_NOSOURCE_QUAD_3_CHANNEL]);
    }
    if (b_ShCal_front_nosource_quad_4_data == 0) {
      b_ShCal_front_nosource_quad_4_data = t_event->Branch("ShCal_front_nosource_quad_4_data",  &g_ADC_data[(SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_HANDLE << 3) + SHCAL_FRONT_NOSOURCE_QUAD_4_CHANNEL]);
    }
    if (b_ShCal_rear_nosource_quad_1_data == 0) {
      b_ShCal_rear_nosource_quad_1_data = t_event->Branch("ShCal_rear_nosource_quad_1_data",  &g_ADC_data[(SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_HANDLE << 3) + SHCAL_REAR_NOSOURCE_QUAD_1_CHANNEL]);
    }
    if (b_ShCal_rear_nosource_quad_2_data == 0) {
      b_ShCal_rear_nosource_quad_2_data = t_event->Branch("ShCal_rear_nosource_quad_2_data",  &g_ADC_data[(SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_HANDLE << 3) + SHCAL_REAR_NOSOURCE_QUAD_2_CHANNEL]);
    }
    if (b_ShCal_rear_nosource_quad_3_data == 0) {
      b_ShCal_rear_nosource_quad_3_data = t_event->Branch("ShCal_rear_nosource_quad_3_data",  &g_ADC_data[(SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_HANDLE << 3) + SHCAL_REAR_NOSOURCE_QUAD_3_CHANNEL]);
    }
    if (b_ShCal_rear_nosource_quad_4_data == 0) {
      b_ShCal_rear_nosource_quad_4_data = t_event->Branch("ShCal_rear_nosource_quad_4_data",  &g_ADC_data[(SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_HANDLE << 3) + SHCAL_REAR_NOSOURCE_QUAD_4_CHANNEL]);
    }
    
    if (b_S1_counter_data == 0) {
      b_S1_counter_data = t_event->Branch("S1_counter_data", &g_ADC_data[(S1_COUNTER_DIGITIZER_HANDLE << 3) + S1_COUNTER_CHANNEL]);
    }
    if (b_S2_counter_data == 0) {
      b_S2_counter_data = t_event->Branch("S2_counter_data", &g_ADC_data[(S2_COUNTER_DIGITIZER_HANDLE << 3) + S2_COUNTER_CHANNEL]);
    }
    if (b_S3_counter_data == 0) {
      b_S3_counter_data = t_event->Branch("S3_counter_data", &g_ADC_data[(S3_COUNTER_DIGITIZER_HANDLE << 3) + S3_COUNTER_CHANNEL]);
    }
    if (b_S4_counter_data == 0) {
      b_S4_counter_data = t_event->Branch("S4_counter_data", &g_ADC_data[(S4_COUNTER_DIGITIZER_HANDLE << 3) + S4_COUNTER_CHANNEL]);
    }
    if (b_Leakage_counter_data == 0) {
      b_Leakage_counter_data = t_event->Branch("Leakage_counter_data", &g_ADC_data[(LEAKAGE_COUNTER_DIGITIZER_HANDLE << 3) + LEAKAGE_COUNTER_CHANNEL]);
    }
    if (b_Halo_counter_data == 0) {
      b_Halo_counter_data = t_event->Branch("Halo_counter_data", &g_ADC_data[(HALO_COUNTER_DIGITIZER_HANDLE << 3) + HALO_COUNTER_CHANNEL]);
    }
    if (b_Digitizer_1_trigger_data == 0) {
      b_Digitizer_1_trigger_data = t_event->Branch("Digitizer_1_trigger_data", &g_ADC_data[(DIGITIZER_1_TRIGGER_DIGITIZER_HANDLE << 3) + DIGITIZER_1_TRIGGER_CHANNEL]);
    }
    if (b_Digitizer_2_trigger_data == 0) {
      b_Digitizer_2_trigger_data = t_event->Branch("Digitizer_2_trigger_data", &g_ADC_data[(DIGITIZER_2_TRIGGER_DIGITIZER_HANDLE << 3) + DIGITIZER_2_TRIGGER_CHANNEL]);
    }
    if (b_Digitizer_3_trigger_data == 0) {
      b_Digitizer_3_trigger_data = t_event->Branch("Digitizer_3_trigger_data", &g_ADC_data[(DIGITIZER_3_TRIGGER_DIGITIZER_HANDLE << 3) + DIGITIZER_3_TRIGGER_CHANNEL]);
    }
    if (b_Digitizer_4_trigger_data == 0) {
      b_Digitizer_4_trigger_data = t_event->Branch("Digitizer_4_trigger_data", &g_ADC_data[(DIGITIZER_4_TRIGGER_DIGITIZER_HANDLE << 3) + DIGITIZER_4_TRIGGER_CHANNEL]);
    }
    
    if (b_FCal_rod_front_source_digitizer_SN == 0) {
      b_FCal_rod_front_source_digitizer_SN = t_event->Branch("FCal_rod_front_source_digitizer_SN", &g_runHeader->FCal_rod_front_source_digitizer_SN);
    }
    if (b_FCal_rod_front_source_digitizer_handle == 0) {
      b_FCal_rod_front_source_digitizer_handle = t_event->Branch("FCal_rod_front_source_digitizer_handle", &g_runHeader->FCal_rod_front_source_digitizer_handle);
    }
    if (b_FCal_rod_front_source_channel == 0) {
      b_FCal_rod_front_source_channel = t_event->Branch("FCal_rod_front_source_channel", &g_runHeader->FCal_rod_front_source_channel);
    }
    if (b_FCal_rod_front_source_vrange == 0) {
      b_FCal_rod_front_source_vrange = t_event->Branch("FCal_rod_front_source_vrange", &g_runHeader->Vrange[FCAL_ROD_FRONT_SOURCE_DIGITIZER_HANDLE][FCAL_ROD_FRONT_SOURCE_CHANNEL]);
    }
    if (b_FCal_rod_mid_source_digitizer_SN == 0) {
      b_FCal_rod_mid_source_digitizer_SN = t_event->Branch("FCal_rod_mid_source_digitizer_SN", &g_runHeader->FCal_rod_mid_source_digitizer_SN);
    }
    if (b_FCal_rod_mid_source_digitizer_handle == 0) {
      b_FCal_rod_mid_source_digitizer_handle = t_event->Branch("FCal_rod_mid_source_digitizer_handle", &g_runHeader->FCal_rod_mid_source_digitizer_handle);
    }
    if (b_FCal_rod_mid_source_channel == 0) {
      b_FCal_rod_mid_source_channel = t_event->Branch("FCal_rod_mid_source_channel", &g_runHeader->FCal_rod_mid_source_channel);
    }
    if (b_FCal_rod_mid_source_vrange == 0) {
      b_FCal_rod_mid_source_vrange = t_event->Branch("FCal_rod_mid_source_vrange", &g_runHeader->Vrange[FCAL_ROD_MID_SOURCE_DIGITIZER_HANDLE][FCAL_ROD_MID_SOURCE_CHANNEL]);
    }
    if (b_FCal_rod_rear_source_digitizer_SN == 0) {
      b_FCal_rod_rear_source_digitizer_SN = t_event->Branch("FCal_rod_rear_source_digitizer_SN", &g_runHeader->FCal_rod_rear_source_digitizer_SN);
    }
    if (b_FCal_rod_rear_source_digitizer_handle == 0) {
      b_FCal_rod_rear_source_digitizer_handle = t_event->Branch("FCal_rod_rear_source_digitizer_handle", &g_runHeader->FCal_rod_rear_source_digitizer_handle);
    }
    if (b_FCal_rod_rear_source_channel == 0) {
      b_FCal_rod_rear_source_channel = t_event->Branch("FCal_rod_rear_source_channel", &g_runHeader->FCal_rod_rear_source_channel);
    }
    if (b_FCal_rod_rear_source_vrange == 0) {
      b_FCal_rod_rear_source_vrange = t_event->Branch("FCal_rod_rear_source_vrange", &g_runHeader->Vrange[FCAL_ROD_FRONT_SOURCE_DIGITIZER_HANDLE][FCAL_ROD_FRONT_SOURCE_CHANNEL]);
    }    

    if (b_ShCal_front_source_quad_1_digitizer_SN== 0) {
      b_ShCal_front_source_quad_1_digitizer_SN =t_event->Branch("ShCal_front_source_quad_1_digitizer_SN", &g_runHeader->ShCal_front_source_quad_1_digitizer_SN);
    }
    if (b_ShCal_front_source_quad_1_digitizer_handle== 0) {
      b_ShCal_front_source_quad_1_digitizer_handle =t_event->Branch("ShCal_front_source_quad_1_digitizer_handle", &g_runHeader->ShCal_front_source_quad_1_digitizer_handle);
    }
    if (b_ShCal_front_source_quad_1_channel== 0) {
      b_ShCal_front_source_quad_1_channel =t_event->Branch("ShCal_front_source_quad_1_channel", &g_runHeader->ShCal_front_source_quad_1_channel);
    }
    if (b_ShCal_front_source_quad_1_vrange== 0) {
      b_ShCal_front_source_quad_1_vrange =t_event->Branch("ShCal_front_source_quad_1_vrange", &g_runHeader->Vrange[SHCAL_FRONT_SOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_1_CHANNEL]);
    }
    if (b_ShCal_front_source_quad_2_digitizer_SN== 0) {
      b_ShCal_front_source_quad_2_digitizer_SN =t_event->Branch("ShCal_front_source_quad_2_digitizer_SN", &g_runHeader->ShCal_front_source_quad_2_digitizer_SN);
    }
    if (b_ShCal_front_source_quad_2_digitizer_handle== 0) {
      b_ShCal_front_source_quad_2_digitizer_handle =t_event->Branch("ShCal_front_source_quad_2_digitizer_handle", &g_runHeader->ShCal_front_source_quad_2_digitizer_handle);
    }
    if (b_ShCal_front_source_quad_2_channel== 0) {
      b_ShCal_front_source_quad_2_channel =t_event->Branch("ShCal_front_source_quad_2_channel", &g_runHeader->ShCal_front_source_quad_2_channel);
    }
    if (b_ShCal_front_source_quad_2_vrange== 0) {
      b_ShCal_front_source_quad_2_vrange =t_event->Branch("ShCal_front_source_quad_2_vrange", &g_runHeader->Vrange[SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL]);
    }
    if (b_ShCal_front_source_quad_3_digitizer_SN== 0) {
      b_ShCal_front_source_quad_3_digitizer_SN =t_event->Branch("ShCal_front_source_quad_3_digitizer_SN", &g_runHeader->ShCal_front_source_quad_3_digitizer_SN);
    }
    if (b_ShCal_front_source_quad_3_digitizer_handle== 0) {
      b_ShCal_front_source_quad_3_digitizer_handle =t_event->Branch("ShCal_front_source_quad_3_digitizer_handle", &g_runHeader->ShCal_front_source_quad_3_digitizer_handle);
    }
    if (b_ShCal_front_source_quad_3_channel== 0) {
      b_ShCal_front_source_quad_3_channel =t_event->Branch("ShCal_front_source_quad_3_channel", &g_runHeader->ShCal_front_source_quad_3_channel);
    }
    if (b_ShCal_front_source_quad_3_vrange== 0) {
      b_ShCal_front_source_quad_3_vrange =t_event->Branch("ShCal_front_source_quad_3_vrange", &g_runHeader->Vrange[SHCAL_FRONT_SOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_3_CHANNEL]);
    }
    if (b_ShCal_front_source_quad_4_digitizer_SN == 0) {
      b_ShCal_front_source_quad_4_digitizer_SN = t_event->Branch("ShCal_front_source_quad_4_digitizer_SN", &g_runHeader->ShCal_front_source_quad_4_digitizer_SN);
    }
    if (b_ShCal_front_source_quad_4_digitizer_handle == 0) {
      b_ShCal_front_source_quad_4_digitizer_handle = t_event->Branch("ShCal_front_source_quad_4_digitizer_handle", &g_runHeader->ShCal_front_source_quad_4_digitizer_handle);
    }
    if (b_ShCal_front_source_quad_4_channel== 0) {
      b_ShCal_front_source_quad_4_channel =t_event->Branch("ShCal_front_source_quad_4_channel", &g_runHeader->ShCal_front_source_quad_4_channel);
    }
    if (b_ShCal_front_source_quad_4_vrange== 0) {
      b_ShCal_front_source_quad_4_vrange =t_event->Branch("ShCal_front_source_quad_4_vrange", &g_runHeader->Vrange[SHCAL_FRONT_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_SOURCE_QUAD_2_CHANNEL]);
    }
    
    if (b_ShCal_rear_source_quad_1_digitizer_SN== 0) {
      b_ShCal_rear_source_quad_1_digitizer_SN =t_event->Branch("ShCal_rear_source_quad_1_digitizer_SN", &g_runHeader->ShCal_rear_source_quad_1_digitizer_SN);
    }
    if (b_ShCal_rear_source_quad_1_digitizer_handle== 0) {
      b_ShCal_rear_source_quad_1_digitizer_handle =t_event->Branch("ShCal_rear_source_quad_1_digitizer_handle", &g_runHeader->ShCal_rear_source_quad_1_digitizer_handle);
    }
    if (b_ShCal_rear_source_quad_1_channel== 0) {
      b_ShCal_rear_source_quad_1_channel =t_event->Branch("ShCal_rear_source_quad_1_channel", &g_runHeader->ShCal_rear_source_quad_1_channel);
    }
    if (b_ShCal_rear_source_quad_1_vrange== 0) {
      b_ShCal_rear_source_quad_1_vrange =t_event->Branch("ShCal_rear_source_quad_1_vrange", &g_runHeader->Vrange[SHCAL_REAR_SOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_1_CHANNEL]);
    }
    if (b_ShCal_rear_source_quad_2_digitizer_SN== 0) {
      b_ShCal_rear_source_quad_2_digitizer_SN =t_event->Branch("ShCal_rear_source_quad_2_digitizer_SN", &g_runHeader->ShCal_rear_source_quad_2_digitizer_SN);
    }
    if (b_ShCal_rear_source_quad_2_digitizer_handle== 0) {
      b_ShCal_rear_source_quad_2_digitizer_handle =t_event->Branch("ShCal_rear_source_quad_2_digitizer_handle", &g_runHeader->ShCal_rear_source_quad_2_digitizer_handle);
    }
    if (b_ShCal_rear_source_quad_2_channel== 0) {
      b_ShCal_rear_source_quad_2_channel =t_event->Branch("ShCal_rear_source_quad_2_channel", &g_runHeader->ShCal_rear_source_quad_2_channel);
    }
    if (b_ShCal_rear_source_quad_2_vrange== 0) {
      b_ShCal_rear_source_quad_2_vrange =t_event->Branch("ShCal_rear_source_quad_2_vrange", &g_runHeader->Vrange[SHCAL_REAR_SOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_2_CHANNEL]);
    }
    if (b_ShCal_rear_source_quad_3_digitizer_SN== 0) {
      b_ShCal_rear_source_quad_3_digitizer_SN =t_event->Branch("ShCal_rear_source_quad_3_digitizer_SN", &g_runHeader->ShCal_rear_source_quad_3_digitizer_SN);
    }
    if (b_ShCal_rear_source_quad_3_digitizer_handle== 0) {
      b_ShCal_rear_source_quad_3_digitizer_handle =t_event->Branch("ShCal_rear_source_quad_3_digitizer_handle", &g_runHeader->ShCal_rear_source_quad_3_digitizer_handle);
    }
    if (b_ShCal_rear_source_quad_3_channel== 0) {
      b_ShCal_rear_source_quad_3_channel =t_event->Branch("ShCal_rear_source_quad_3_channel", &g_runHeader->ShCal_rear_source_quad_3_channel);
    }
    if (b_ShCal_rear_source_quad_3_vrange== 0) {
      b_ShCal_rear_source_quad_3_vrange =t_event->Branch("ShCal_rear_source_quad_3_vrange", &g_runHeader->Vrange[SHCAL_REAR_SOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_3_CHANNEL]);
    }
    if (b_ShCal_rear_source_quad_4_digitizer_SN== 0) {
      b_ShCal_rear_source_quad_4_digitizer_SN =t_event->Branch("ShCal_rear_source_quad_4_digitizer_SN", &g_runHeader->ShCal_rear_source_quad_4_digitizer_SN);
    }
    if (b_ShCal_rear_source_quad_4_digitizer_handle== 0) {
      b_ShCal_rear_source_quad_4_digitizer_handle =t_event->Branch("ShCal_rear_source_quad_4_digitizer_handle", &g_runHeader->ShCal_rear_source_quad_4_digitizer_handle);
    }
    if (b_ShCal_rear_source_quad_4_channel== 0) {
      b_ShCal_rear_source_quad_4_channel =t_event->Branch("ShCal_rear_source_quad_4_channel", &g_runHeader->ShCal_rear_source_quad_4_channel);
    }
    if (b_ShCal_rear_source_quad_4_vrange== 0) {
      b_ShCal_rear_source_quad_4_vrange =t_event->Branch("ShCal_rear_source_quad_4_vrange", &g_runHeader->Vrange[SHCAL_REAR_SOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_REAR_SOURCE_QUAD_4_CHANNEL]);
    }

    if (b_FCal_rod_front_nosource_digitizer_SN == 0) {
      b_FCal_rod_front_nosource_digitizer_SN = t_event->Branch("FCal_rod_front_nosource_digitizer_SN", &g_runHeader->FCal_rod_front_nosource_digitizer_SN);
    }
    if (b_FCal_rod_front_nosource_digitizer_handle == 0) {
      b_FCal_rod_front_nosource_digitizer_handle = t_event->Branch("FCal_rod_front_nosource_digitizer_handle", &g_runHeader->FCal_rod_front_nosource_digitizer_handle);
    }
    if (b_FCal_rod_front_nosource_channel == 0) {
      b_FCal_rod_front_nosource_channel = t_event->Branch("FCal_rod_front_nosource_channel", &g_runHeader->FCal_rod_front_nosource_channel);
    }
    if (b_FCal_rod_front_nosource_vrange == 0) {
      b_FCal_rod_front_nosource_vrange = t_event->Branch("FCal_rod_front_nosource_vrange", &g_runHeader->Vrange[FCAL_ROD_FRONT_NOSOURCE_DIGITIZER_HANDLE][FCAL_ROD_FRONT_NOSOURCE_CHANNEL]);
    }
    if (b_FCal_rod_mid_nosource_digitizer_SN == 0) {
      b_FCal_rod_mid_nosource_digitizer_SN = t_event->Branch("FCal_rod_mid_nosource_digitizer_SN", &g_runHeader->FCal_rod_mid_nosource_digitizer_SN);
    }
    if (b_FCal_rod_mid_nosource_digitizer_handle == 0) {
      b_FCal_rod_mid_nosource_digitizer_handle = t_event->Branch("FCal_rod_mid_nosource_digitizer_handle", &g_runHeader->FCal_rod_mid_nosource_digitizer_handle);
    }
    if (b_FCal_rod_mid_nosource_channel == 0) {
      b_FCal_rod_mid_nosource_channel = t_event->Branch("FCal_rod_mid_nosource_channel", &g_runHeader->FCal_rod_mid_nosource_channel);
    }
    if (b_FCal_rod_mid_nosource_vrange == 0) {
      b_FCal_rod_mid_nosource_vrange = t_event->Branch("FCal_rod_mid_nosource_vrange", &g_runHeader->Vrange[FCAL_ROD_MID_NOSOURCE_DIGITIZER_HANDLE][FCAL_ROD_MID_NOSOURCE_CHANNEL]);
    }
    if (b_FCal_rod_rear_nosource_digitizer_SN == 0) {
      b_FCal_rod_rear_nosource_digitizer_SN = t_event->Branch("FCal_rod_rear_nosource_digitizer_SN", &g_runHeader->FCal_rod_rear_nosource_digitizer_SN);
    }
    if (b_FCal_rod_rear_nosource_digitizer_handle == 0) {
      b_FCal_rod_rear_nosource_digitizer_handle = t_event->Branch("FCal_rod_rear_nosource_digitizer_handle", &g_runHeader->FCal_rod_rear_nosource_digitizer_handle);
    }
    if (b_FCal_rod_rear_nosource_channel == 0) {
      b_FCal_rod_rear_nosource_channel = t_event->Branch("FCal_rod_rear_nosource_channel", &g_runHeader->FCal_rod_rear_nosource_channel);
    }
    if (b_FCal_rod_rear_nosource_vrange == 0) {
      b_FCal_rod_rear_nosource_vrange = t_event->Branch("FCal_rod_rear_nosource_vrange", &g_runHeader->Vrange[FCAL_ROD_REAR_NOSOURCE_DIGITIZER_HANDLE][FCAL_ROD_REAR_NOSOURCE_CHANNEL]);
    }

    if (b_ShCal_front_nosource_quad_1_digitizer_SN== 0) {
      b_ShCal_front_nosource_quad_1_digitizer_SN =t_event->Branch("ShCal_front_nosource_quad_1_digitizer_SN", &g_runHeader->ShCal_front_nosource_quad_1_digitizer_SN);
    }
    if (b_ShCal_front_nosource_quad_1_digitizer_handle== 0) {
      b_ShCal_front_nosource_quad_1_digitizer_handle =t_event->Branch("ShCal_front_nosource_quad_1_digitizer_handle", &g_runHeader->ShCal_front_nosource_quad_1_digitizer_handle);
    }
    if (b_ShCal_front_nosource_quad_1_channel== 0) {
      b_ShCal_front_nosource_quad_1_channel =t_event->Branch("ShCal_front_nosource_quad_1_channel", &g_runHeader->ShCal_front_nosource_quad_1_channel);
    }
    if (b_ShCal_front_nosource_quad_1_vrange== 0) {
      b_ShCal_front_nosource_quad_1_vrange =t_event->Branch("ShCal_front_nosource_quad_1_vrange", &g_runHeader->Vrange[SHCAL_FRONT_NOSOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_1_CHANNEL]);
    }
    if (b_ShCal_front_nosource_quad_2_digitizer_SN== 0) {
      b_ShCal_front_nosource_quad_2_digitizer_SN =t_event->Branch("ShCal_front_nosource_quad_2_digitizer_SN", &g_runHeader->ShCal_front_nosource_quad_2_digitizer_SN);
    }
    if (b_ShCal_front_nosource_quad_2_digitizer_handle== 0) {
      b_ShCal_front_nosource_quad_2_digitizer_handle =t_event->Branch("ShCal_front_nosource_quad_2_digitizer_handle", &g_runHeader->ShCal_front_nosource_quad_2_digitizer_handle);
    }
    if (b_ShCal_front_nosource_quad_2_channel== 0) {
      b_ShCal_front_nosource_quad_2_channel =t_event->Branch("ShCal_front_nosource_quad_2_channel", &g_runHeader->ShCal_front_nosource_quad_2_channel);
    }
    if (b_ShCal_front_nosource_quad_2_vrange== 0) {
      b_ShCal_front_nosource_quad_2_vrange =t_event->Branch("ShCal_front_nosource_quad_2_vrange", &g_runHeader->Vrange[SHCAL_FRONT_NOSOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_2_CHANNEL]);
    }
    if (b_ShCal_front_nosource_quad_3_digitizer_SN== 0) {
      b_ShCal_front_nosource_quad_3_digitizer_SN =t_event->Branch("ShCal_front_nosource_quad_3_digitizer_SN", &g_runHeader->ShCal_front_nosource_quad_3_digitizer_SN);
    }
    if (b_ShCal_front_nosource_quad_3_digitizer_handle== 0) {
      b_ShCal_front_nosource_quad_3_digitizer_handle =t_event->Branch("ShCal_front_nosource_quad_3_digitizer_handle", &g_runHeader->ShCal_front_nosource_quad_3_digitizer_handle);
    }
    if (b_ShCal_front_nosource_quad_3_channel== 0) {
      b_ShCal_front_nosource_quad_3_channel =t_event->Branch("ShCal_front_nosource_quad_3_channel", &g_runHeader->ShCal_front_nosource_quad_3_channel);
    }
    if (b_ShCal_front_nosource_quad_3_vrange== 0) {
      b_ShCal_front_nosource_quad_3_vrange =t_event->Branch("ShCal_front_nosource_quad_3_vrange", &g_runHeader->Vrange[SHCAL_FRONT_NOSOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_3_CHANNEL]);
    }
    if (b_ShCal_front_nosource_quad_4_digitizer_SN == 0) {
      b_ShCal_front_nosource_quad_4_digitizer_SN = t_event->Branch("ShCal_front_nosource_quad_4_digitizer_SN", &g_runHeader->ShCal_front_nosource_quad_4_digitizer_SN);
    }
    if (b_ShCal_front_nosource_quad_4_digitizer_handle == 0) {
      b_ShCal_front_nosource_quad_4_digitizer_handle = t_event->Branch("ShCal_front_nosource_quad_4_digitizer_handle", &g_runHeader->ShCal_front_nosource_quad_4_digitizer_handle);
    }
    if (b_ShCal_front_nosource_quad_4_channel== 0) {
      b_ShCal_front_nosource_quad_4_channel =t_event->Branch("ShCal_front_nosource_quad_4_channel", &g_runHeader->ShCal_front_nosource_quad_4_channel);
    }
    if (b_ShCal_front_nosource_quad_4_vrange== 0) {
      b_ShCal_front_nosource_quad_4_vrange =t_event->Branch("ShCal_front_nosource_quad_4_vrange", &g_runHeader->Vrange[SHCAL_FRONT_NOSOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_FRONT_NOSOURCE_QUAD_4_CHANNEL]);
    }

    if (b_ShCal_rear_nosource_quad_1_digitizer_SN== 0) {
      b_ShCal_rear_nosource_quad_1_digitizer_SN =t_event->Branch("ShCal_rear_nosource_quad_1_digitizer_SN", &g_runHeader->ShCal_rear_nosource_quad_1_digitizer_SN);
    }
    if (b_ShCal_rear_nosource_quad_1_digitizer_handle== 0) {
      b_ShCal_rear_nosource_quad_1_digitizer_handle =t_event->Branch("ShCal_rear_nosource_quad_1_digitizer_handle", &g_runHeader->ShCal_rear_nosource_quad_1_digitizer_handle);
    }
    if (b_ShCal_rear_nosource_quad_1_channel== 0) {
      b_ShCal_rear_nosource_quad_1_channel =t_event->Branch("ShCal_rear_nosource_quad_1_channel", &g_runHeader->ShCal_rear_nosource_quad_1_channel);
    }
    if (b_ShCal_rear_nosource_quad_1_vrange== 0) {
      b_ShCal_rear_nosource_quad_1_vrange =t_event->Branch("ShCal_rear_nosource_quad_1_vrange", &g_runHeader->Vrange[SHCAL_REAR_NOSOURCE_QUAD_1_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_1_CHANNEL]);
    }
    if (b_ShCal_rear_nosource_quad_2_digitizer_SN== 0) {
      b_ShCal_rear_nosource_quad_2_digitizer_SN =t_event->Branch("ShCal_rear_nosource_quad_2_digitizer_SN", &g_runHeader->ShCal_rear_nosource_quad_2_digitizer_SN);
    }
    if (b_ShCal_rear_nosource_quad_2_digitizer_handle== 0) {
      b_ShCal_rear_nosource_quad_2_digitizer_handle =t_event->Branch("ShCal_rear_nosource_quad_2_digitizer_handle", &g_runHeader->ShCal_rear_nosource_quad_2_digitizer_handle);
    }
    if (b_ShCal_rear_nosource_quad_2_channel== 0) {
      b_ShCal_rear_nosource_quad_2_channel =t_event->Branch("ShCal_rear_nosource_quad_2_channel", &g_runHeader->ShCal_rear_nosource_quad_2_channel);
    }
    if (b_ShCal_rear_nosource_quad_2_vrange== 0) {
      b_ShCal_rear_nosource_quad_2_vrange =t_event->Branch("ShCal_rear_nosource_quad_2_vrange", &g_runHeader->Vrange[SHCAL_REAR_NOSOURCE_QUAD_2_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_2_CHANNEL]);
    }
    if (b_ShCal_rear_nosource_quad_3_digitizer_SN== 0) {
      b_ShCal_rear_nosource_quad_3_digitizer_SN =t_event->Branch("ShCal_rear_nosource_quad_3_digitizer_SN", &g_runHeader->ShCal_rear_nosource_quad_3_digitizer_SN);
    }
    if (b_ShCal_rear_nosource_quad_3_digitizer_handle== 0) {
      b_ShCal_rear_nosource_quad_3_digitizer_handle =t_event->Branch("ShCal_rear_nosource_quad_3_digitizer_handle", &g_runHeader->ShCal_rear_nosource_quad_3_digitizer_handle);
    }
    if (b_ShCal_rear_nosource_quad_3_channel== 0) {
      b_ShCal_rear_nosource_quad_3_channel =t_event->Branch("ShCal_rear_nosource_quad_3_channel", &g_runHeader->ShCal_rear_nosource_quad_3_channel);
    }
    if (b_ShCal_rear_nosource_quad_3_vrange== 0) {
      b_ShCal_rear_nosource_quad_3_vrange =t_event->Branch("ShCal_rear_nosource_quad_3_vrange", &g_runHeader->Vrange[SHCAL_REAR_NOSOURCE_QUAD_3_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_3_CHANNEL]);
    }
    if (b_ShCal_rear_nosource_quad_4_digitizer_SN== 0) {
      b_ShCal_rear_nosource_quad_4_digitizer_SN =t_event->Branch("ShCal_rear_nosource_quad_4_digitizer_SN", &g_runHeader->ShCal_rear_nosource_quad_4_digitizer_SN);
    }
    if (b_ShCal_rear_nosource_quad_4_digitizer_handle== 0) {
      b_ShCal_rear_nosource_quad_4_digitizer_handle =t_event->Branch("ShCal_rear_nosource_quad_4_digitizer_handle", &g_runHeader->ShCal_rear_nosource_quad_4_digitizer_handle);
    }
    if (b_ShCal_rear_nosource_quad_4_channel== 0) {
      b_ShCal_rear_nosource_quad_4_channel =t_event->Branch("ShCal_rear_nosource_quad_4_channel", &g_runHeader->ShCal_rear_nosource_quad_4_channel);
    }
    if (b_ShCal_rear_nosource_quad_4_vrange== 0) {
      b_ShCal_rear_nosource_quad_4_vrange =t_event->Branch("ShCal_rear_nosource_quad_4_vrange", &g_runHeader->Vrange[SHCAL_REAR_NOSOURCE_QUAD_4_DIGITIZER_HANDLE][SHCAL_REAR_NOSOURCE_QUAD_4_CHANNEL]);
    }

    if (b_S1_counter_digitizer_SN == 0) {
      b_S1_counter_digitizer_SN = t_event->Branch("S1_counter_digitizer_SN", &g_runHeader->S1_counter_digitizer_SN);
    }
    if (b_S1_counter_digitizer_handle == 0) {
      b_S1_counter_digitizer_handle = t_event->Branch("S1_counter_digitizer_handle", &g_runHeader->S1_counter_digitizer_handle);
    }
    if (b_S1_counter_channel == 0) {
      b_S1_counter_channel = t_event->Branch("S1_counter_channel", &g_runHeader->S1_counter_channel);
    }
    if (b_S1_counter_vrange == 0) {
      b_S1_counter_vrange = t_event->Branch("S1_counter_vrange", &g_runHeader->Vrange[S1_COUNTER_DIGITIZER_HANDLE][S1_COUNTER_CHANNEL]);
    }
    if (b_S2_counter_digitizer_SN == 0) {
      b_S2_counter_digitizer_SN = t_event->Branch("S2_counter_digitizer_SN", &g_runHeader->S2_counter_digitizer_SN);
    }
    if (b_S2_counter_digitizer_handle == 0) {
      b_S2_counter_digitizer_handle = t_event->Branch("S2_counter_digitizer_handle", &g_runHeader->S2_counter_digitizer_handle);
    }
    if (b_S2_counter_channel == 0) {
      b_S2_counter_channel = t_event->Branch("S2_counter_channel", &g_runHeader->S2_counter_channel);
    }
    if (b_S2_counter_vrange == 0) {
      b_S2_counter_vrange = t_event->Branch("S2_counter_vrange", &g_runHeader->Vrange[S2_COUNTER_DIGITIZER_HANDLE][S2_COUNTER_CHANNEL]);
    }
    if (b_S3_counter_digitizer_SN == 0) {
      b_S3_counter_digitizer_SN = t_event->Branch("S3_counter_digitizer_SN", &g_runHeader->S3_counter_digitizer_SN);
    }
    if (b_S3_counter_digitizer_handle == 0) {
      b_S3_counter_digitizer_handle = t_event->Branch("S3_counter_digitizer_handle", &g_runHeader->S3_counter_digitizer_handle);
    }
    if (b_S3_counter_channel == 0) {
      b_S3_counter_channel = t_event->Branch("S3_counter_channel", &g_runHeader->S3_counter_channel);
    }
    if (b_S3_counter_vrange == 0) {
      b_S3_counter_vrange = t_event->Branch("S3_counter_vrange", &g_runHeader->Vrange[S3_COUNTER_DIGITIZER_HANDLE][S3_COUNTER_CHANNEL]);
    }
    if (b_S4_counter_digitizer_SN == 0) {
      b_S4_counter_digitizer_SN = t_event->Branch("S4_counter_digitizer_SN", &g_runHeader->S4_counter_digitizer_SN);
    }
    if (b_Leakage_counter_digitizer_handle == 0) {
      b_Leakage_counter_digitizer_handle = t_event->Branch("Leakage_counter_digitizer_handle", &g_runHeader->Leakage_counter_digitizer_handle);
    }
    if (b_S4_counter_channel == 0) {
      b_S4_counter_channel = t_event->Branch("S4_counter_channel", &g_runHeader->S4_counter_channel);
    }
    if (b_S4_counter_vrange == 0) {
      b_S4_counter_vrange = t_event->Branch("S4_counter_vrange", &g_runHeader->Vrange[S4_COUNTER_DIGITIZER_HANDLE][S4_COUNTER_CHANNEL]);
    }
    if (b_Leakage_counter_digitizer_SN == 0) {
      b_Leakage_counter_digitizer_SN = t_event->Branch("Leakage_counter_digitizer_SN", &g_runHeader->Leakage_counter_digitizer_SN);
    }
    if (b_Leakage_counter_digitizer_handle == 0) {
      b_Leakage_counter_digitizer_handle = t_event->Branch("Leakage_counter_digitizer_handle", &g_runHeader->Leakage_counter_digitizer_handle);
    }
    if (b_Leakage_counter_channel == 0) {
      b_Leakage_counter_channel = t_event->Branch("Leakage_counter_channel", &g_runHeader->Leakage_counter_channel);
    }
    if (b_Leakage_counter_vrange == 0) {
      b_Leakage_counter_vrange = t_event->Branch("Leakage_counter_vrange", &g_runHeader->Vrange[LEAKAGE_COUNTER_DIGITIZER_HANDLE][LEAKAGE_COUNTER_CHANNEL]);
    }
    if (b_Halo_counter_digitizer_SN == 0) {
      b_Halo_counter_digitizer_SN = t_event->Branch("Halo_counter_digitizer_SN", &g_runHeader->Halo_counter_digitizer_SN);
    }
    if (b_Halo_counter_digitizer_handle == 0) {
      b_Halo_counter_digitizer_handle = t_event->Branch("Halo_counter_digitizer_handle", &g_runHeader->Halo_counter_digitizer_handle);
    }
    if (b_Halo_counter_channel == 0) {
      b_Halo_counter_channel = t_event->Branch("Halo_counter_channel", &g_runHeader->Halo_counter_channel);
    }
    if (b_Halo_counter_vrange == 0) {
      b_Halo_counter_vrange = t_event->Branch("Halo_counter_vrange", &g_runHeader->Vrange[HALO_COUNTER_DIGITIZER_HANDLE][HALO_COUNTER_CHANNEL]);
    }
    if (b_Digitizer_1_trigger_digitizer_SN == 0) {
      b_Digitizer_1_trigger_digitizer_SN = t_event->Branch("Digitizer_1_trigger_digitizer_SN", &g_runHeader->Digitizer_1_trigger_digitizer_SN);
    }
    if (b_Digitizer_1_trigger_digitizer_handle == 0) {
      b_Digitizer_1_trigger_digitizer_handle = t_event->Branch("Digitizer_1_trigger_digitizer_handle", &g_runHeader->Digitizer_1_trigger_digitizer_handle);
    }
    if (b_Digitizer_1_trigger_channel == 0) {
      b_Digitizer_1_trigger_channel = t_event->Branch("Digitizer_1_trigger_channel", &g_runHeader->Digitizer_1_trigger_channel);
    }
    if (b_Digitizer_1_trigger_vrange == 0) {
      b_Digitizer_1_trigger_vrange = t_event->Branch("Digitizer_1_trigger_vrange", &g_runHeader->Vrange[DIGITIZER_1_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_1_TRIGGER_CHANNEL]);
    }
    if (b_Digitizer_2_trigger_digitizer_SN == 0) {
      b_Digitizer_2_trigger_digitizer_SN = t_event->Branch("Digitizer_2_trigger_digitizer_SN", &g_runHeader->Digitizer_2_trigger_digitizer_SN);
    }
    if (b_Digitizer_2_trigger_digitizer_handle == 0) {
      b_Digitizer_2_trigger_digitizer_handle = t_event->Branch("Digitizer_2_trigger_digitizer_handle", &g_runHeader->Digitizer_2_trigger_digitizer_handle);
    }
    if (b_Digitizer_2_trigger_channel == 0) {
      b_Digitizer_2_trigger_channel = t_event->Branch("Digitizer_2_trigger_channel", &g_runHeader->Digitizer_2_trigger_channel);
    }
    if (b_Digitizer_2_trigger_vrange == 0) {
      b_Digitizer_2_trigger_vrange = t_event->Branch("Digitizer_2_trigger_vrange", &g_runHeader->Vrange[DIGITIZER_2_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_2_TRIGGER_CHANNEL]);
    }
    if (b_Digitizer_3_trigger_digitizer_SN == 0) {
      b_Digitizer_3_trigger_digitizer_SN = t_event->Branch("Digitizer_3_trigger_digitizer_SN", &g_runHeader->Digitizer_3_trigger_digitizer_SN);
    }
    if (b_Digitizer_3_trigger_digitizer_handle == 0) {
      b_Digitizer_3_trigger_digitizer_handle = t_event->Branch("Digitizer_3_trigger_digitizer_handle", &g_runHeader->Digitizer_3_trigger_digitizer_handle);
    }
    if (b_Digitizer_3_trigger_channel == 0) {
      b_Digitizer_3_trigger_channel = t_event->Branch("Digitizer_3_trigger_channel", &g_runHeader->Digitizer_3_trigger_channel);
    }
    if (b_Digitizer_3_trigger_vrange == 0) {
      b_Digitizer_3_trigger_vrange = t_event->Branch("Digitizer_3_trigger_vrange", &g_runHeader->Vrange[DIGITIZER_3_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_3_TRIGGER_CHANNEL]);
    }
    if (b_Digitizer_4_trigger_digitizer_SN == 0) {
      b_Digitizer_4_trigger_digitizer_SN = t_event->Branch("Digitizer_4_trigger_digitizer_SN", &g_runHeader->Digitizer_4_trigger_digitizer_SN);
    }
    if (b_Digitizer_4_trigger_digitizer_handle == 0) {
      b_Digitizer_4_trigger_digitizer_handle = t_event->Branch("Digitizer_4_trigger_digitizer_handle", &g_runHeader->Digitizer_4_trigger_digitizer_handle);
    }
    if (b_Digitizer_4_trigger_channel == 0) {
      b_Digitizer_4_trigger_channel = t_event->Branch("Digitizer_4_trigger_channel", &g_runHeader->Digitizer_4_trigger_channel);
    }
    if (b_Digitizer_4_trigger_vrange == 0) {
      b_Digitizer_4_trigger_vrange = t_event->Branch("Digitizer_4_trigger_vrange", &g_runHeader->Vrange[DIGITIZER_4_TRIGGER_DIGITIZER_HANDLE][DIGITIZER_4_TRIGGER_CHANNEL]);
    }

  } else {
    if (b_DIGITIZER_1_channel_0_data == 0) {
      b_DIGITIZER_1_channel_0_data = t_event->Branch("AZ_DIGITIZER_channel_0_data", &g_ADC_data[(DIGITIZER_1_HANDLE << 3) + 0]);
    }
    if (b_DIGITIZER_1_channel_1_data == 0) {
      b_DIGITIZER_1_channel_1_data = t_event->Branch("AZ_DIGITIZER_channel_1_data", &g_ADC_data[(DIGITIZER_1_HANDLE << 3) + 1]);
    }
    if (b_DIGITIZER_1_channel_2_data == 0) {
      b_DIGITIZER_1_channel_2_data = t_event->Branch("AZ_DIGITIZER_channel_2_data", &g_ADC_data[(DIGITIZER_1_HANDLE << 3) + 2]);
    }
    if (b_DIGITIZER_1_channel_3_data == 0) {
      b_DIGITIZER_1_channel_3_data = t_event->Branch("AZ_DIGITIZER_channel_3_data", &g_ADC_data[(DIGITIZER_1_HANDLE << 3) + 3]);
    }
    if (b_DIGITIZER_1_channel_4_data == 0) {
      b_DIGITIZER_1_channel_4_data = t_event->Branch("AZ_DIGITIZER_channel_4_data", &g_ADC_data[(DIGITIZER_1_HANDLE << 3) + 4]);
    }
    if (b_DIGITIZER_1_channel_5_data == 0) {
      b_DIGITIZER_1_channel_5_data = t_event->Branch("AZ_DIGITIZER_channel_5_data", &g_ADC_data[(DIGITIZER_1_HANDLE << 3) + 5]);
    }
    if (b_DIGITIZER_1_channel_6_data == 0) {
      b_DIGITIZER_1_channel_6_data = t_event->Branch("AZ_DIGITIZER_channel_6_data", &g_ADC_data[(DIGITIZER_1_HANDLE << 3) + 6]);
    }
    if (b_DIGITIZER_1_channel_7_data == 0) {
      b_DIGITIZER_1_channel_7_data = t_event->Branch("AZ_DIGITIZER_channel_7_data", &g_ADC_data[(DIGITIZER_1_HANDLE << 3) + 7]);
    }
    
    if (b_DIGITIZER_2_channel_0_data == 0) {
      b_DIGITIZER_2_channel_0_data = t_event->Branch("TRIUMF_DIGITIZER_channel_0_data", &g_ADC_data[(DIGITIZER_2_HANDLE << 3) + 0]);
    }
    if (b_DIGITIZER_2_channel_1_data == 0) {
      b_DIGITIZER_2_channel_1_data = t_event->Branch("TRIUMF_DIGITIZER_channel_1_data", &g_ADC_data[(DIGITIZER_2_HANDLE << 3) + 1]);
    }
    if (b_DIGITIZER_2_channel_2_data == 0) {
      b_DIGITIZER_2_channel_2_data = t_event->Branch("TRIUMF_DIGITIZER_channel_2_data", &g_ADC_data[(DIGITIZER_2_HANDLE << 3) + 2]);
    }
    if (b_DIGITIZER_2_channel_3_data == 0) {
      b_DIGITIZER_2_channel_3_data = t_event->Branch("TRIUMF_DIGITIZER_channel_3_data", &g_ADC_data[(DIGITIZER_2_HANDLE << 3) + 3]);
    }
    if (b_DIGITIZER_2_channel_4_data == 0) {
      b_DIGITIZER_2_channel_4_data = t_event->Branch("TRIUMF_DIGITIZER_channel_4_data", &g_ADC_data[(DIGITIZER_2_HANDLE << 3) + 4]);
    }
    if (b_DIGITIZER_2_channel_5_data == 0) {
      b_DIGITIZER_2_channel_5_data = t_event->Branch("TRIUMF_DIGITIZER_channel_5_data", &g_ADC_data[(DIGITIZER_2_HANDLE << 3) + 5]);
    }
    if (b_DIGITIZER_2_channel_6_data == 0) {
      b_DIGITIZER_2_channel_6_data = t_event->Branch("TRIUMF_DIGITIZER_channel_6_data", &g_ADC_data[(DIGITIZER_2_HANDLE << 3) + 6]);
    }
    if (b_DIGITIZER_2_channel_7_data == 0) {
      b_DIGITIZER_2_channel_7_data = t_event->Branch("TRIUMF_DIGITIZER_channel_7_data", &g_ADC_data[(DIGITIZER_2_HANDLE << 3) + 7]);
    }

    if (b_DIGITIZER_1_channel_0_vrange == 0) {
      b_DIGITIZER_1_channel_0_vrange = t_event->Branch("AZ_DIGITIZER_channel_0_vrange", &g_runHeader->Vrange[DIGITIZER_1_HANDLE][0]);
    }
    if (b_DIGITIZER_1_channel_1_vrange == 0) {
      b_DIGITIZER_1_channel_1_vrange = t_event->Branch("AZ_DIGITIZER_channel_1_vrange", &g_runHeader->Vrange[DIGITIZER_1_HANDLE][1]);
    }
    if (b_DIGITIZER_1_channel_2_vrange == 0) {
      b_DIGITIZER_1_channel_2_vrange = t_event->Branch("AZ_DIGITIZER_channel_2_vrange", &g_runHeader->Vrange[DIGITIZER_1_HANDLE][2]);
    }
    if (b_DIGITIZER_1_channel_3_vrange == 0) {
      b_DIGITIZER_1_channel_3_vrange = t_event->Branch("AZ_DIGITIZER_channel_3_vrange", &g_runHeader->Vrange[DIGITIZER_1_HANDLE][3]);
    }
    if (b_DIGITIZER_1_channel_4_vrange == 0) {
      b_DIGITIZER_1_channel_4_vrange = t_event->Branch("AZ_DIGITIZER_channel_4_vrange", &g_runHeader->Vrange[DIGITIZER_1_HANDLE][4]);
    }
    if (b_DIGITIZER_1_channel_5_vrange == 0) {
      b_DIGITIZER_1_channel_5_vrange = t_event->Branch("AZ_DIGITIZER_channel_5_vrange", &g_runHeader->Vrange[DIGITIZER_1_HANDLE][5]);
    }
    if (b_DIGITIZER_1_channel_6_vrange == 0) {
      b_DIGITIZER_1_channel_6_vrange = t_event->Branch("AZ_DIGITIZER_channel_6_vrange", &g_runHeader->Vrange[DIGITIZER_1_HANDLE][6]);
    }
    if (b_DIGITIZER_1_channel_7_vrange == 0) {
      b_DIGITIZER_1_channel_7_vrange = t_event->Branch("AZ_DIGITIZER_channel_7_vrange", &g_runHeader->Vrange[DIGITIZER_1_HANDLE][7]);
    }

    if (b_DIGITIZER_2_channel_0_vrange == 0) {
      b_DIGITIZER_2_channel_0_vrange = t_event->Branch("TRIUMF_DIGITIZER_channel_0_vrange", &g_runHeader->Vrange[DIGITIZER_2_HANDLE][0]);
    }
    if (b_DIGITIZER_2_channel_1_vrange == 0) {
      b_DIGITIZER_2_channel_1_vrange = t_event->Branch("TRIUMF_DIGITIZER_channel_1_vrange", &g_runHeader->Vrange[DIGITIZER_2_HANDLE][1]);
    }
    if (b_DIGITIZER_2_channel_2_vrange == 0) {
      b_DIGITIZER_2_channel_2_vrange = t_event->Branch("TRIUMF_DIGITIZER_channel_2_vrange", &g_runHeader->Vrange[DIGITIZER_2_HANDLE][2]);
    }
    if (b_DIGITIZER_2_channel_3_vrange == 0) {
      b_DIGITIZER_2_channel_3_vrange = t_event->Branch("TRIUMF_DIGITIZER_channel_3_vrange", &g_runHeader->Vrange[DIGITIZER_2_HANDLE][3]);
    }
    if (b_DIGITIZER_2_channel_4_vrange == 0) {
      b_DIGITIZER_2_channel_4_vrange = t_event->Branch("TRIUMF_DIGITIZER_channel_4_vrange", &g_runHeader->Vrange[DIGITIZER_2_HANDLE][4]);
    }
    if (b_DIGITIZER_2_channel_5_vrange == 0) {
      b_DIGITIZER_2_channel_5_vrange = t_event->Branch("TRIUMF_DIGITIZER_channel_5_vrange", &g_runHeader->Vrange[DIGITIZER_2_HANDLE][5]);
    }
    if (b_DIGITIZER_2_channel_6_vrange == 0) {
      b_DIGITIZER_2_channel_6_vrange = t_event->Branch("TRIUMF_DIGITIZER_channel_6_vrange", &g_runHeader->Vrange[DIGITIZER_2_HANDLE][6]);
    }
    if (b_DIGITIZER_2_channel_7_vrange == 0) {
      b_DIGITIZER_2_channel_7_vrange = t_event->Branch("TRIUMF_DIGITIZER_channel_7_vrange", &g_runHeader->Vrange[DIGITIZER_2_HANDLE][7]);
    }

  }

  if (merge_BPC_data == 1) {
    if (b_good_BPC_data == 0) {
      b_good_BPC_data = t_event->Branch("good_BPC_data", &good_BPC_data);
    }
    if (b_BPC_tag == 0) {
      b_BPC_tag = t_event->Branch("BPC_tag", &g_BPC_data_sample.tag);
    }	   
    if (b_BPC_runNumber == 0) {
      b_BPC_runNumber = t_event->Branch("BPC_runNumber", &g_BPC_data_sample.runNumber);
    }
    if (b_BPC_spillNumber == 0) {
      b_BPC_spillNumber = t_event->Branch("BPC_spillNumber", &g_BPC_data_sample.spillNumber);
    }
    if (b_BPC_triggerNumber == 0) {
      b_BPC_triggerNumber = t_event->Branch("BPC_triggerNumber", &g_BPC_data_sample.triggerNumber);
    }
    if (b_BPC_accum_time == 0) {
      b_BPC_accum_time = t_event->Branch("BPC_accum_time", &g_BPC_data_sample.accum_time);
    }
    if (b_BPC_wall_time == 0) {
      b_BPC_wall_time = t_event->Branch("BPC_wall_time", &g_BPC_data_sample.wall_time);
    }
    for (int iBPC = 0; iBPC < NUM_BPCS; iBPC++) {
      if (b_BPC_x[iBPC] == 0) {
	char bname[10];
	sprintf(bname, "BPC_x%d", iBPC);
	b_BPC_x[iBPC] = t_event->Branch(bname, &g_BPC_data_sample.xy[iBPC][0]);
      }
      if (b_BPC_y[iBPC] == 0) {
	char bname[10];
	sprintf(bname, "BPC_y%d", iBPC);
	b_BPC_y[iBPC] = t_event->Branch(bname, &g_BPC_data_sample.xy[iBPC][1]);
      }
    }

  }
  //if (b_ADC_data == 0) {
  //  b_ADC_data = t_event->Branch("ADC_data", &g_ADC_data);
  //} else {
    //    b_ADC_data->SetAddress(&ADC_data);
  //}

  uint32_t iEvt;


  //  printf("I think there were %d events\n", g_spillHeader->NumEvents);

  uint16_t good_BPC_samples = 0;

  if (nNumTrigErrors == 0) {
    // write out spill header
    if ( info->FCP_DAQrun->fout_allch != 0) {
      if (fwrite(g_spillHeader, sizeof(*g_spillHeader), 1 , info->FCP_DAQrun->fout_allch) != 1) {
	printf("Error: unable to write spill header");
	printToLogfile("Error: unable to write spill header");
	return -1;
      } 
    } else {
      printf("Error: unable to write spill header -- file not open\n");
      printToLogfile("Error: unable to write spill header -- file not open");
      return -1;
    }
    
    for (iEvt = 0; iEvt < g_spillHeader->NumEvents; iEvt++) {
      
      g_eventHeader->nchannels = 0;
      
      for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
	char* EventPtr = NULL;
	CAEN_DGTZ_EventInfo_t       EventInfo;
	
	ret = CAEN_DGTZ_GetEventInfo(iBoard, info->buffer_ADC[iBoard], info->BufferSize_ADC[iBoard], iEvt, &EventInfo, &EventPtr); 
	if (ret != 0) {
	  printf("Error getting event %d of %d from digitizer %d\n", iEvt, g_spillHeader->NumEvents, iBoard);
	  char buf[200];
	  sprintf(buf, "Error getting event %d of %d from digitizer %d", iEvt, g_spillHeader->NumEvents, iBoard);
	  printToLogfile(buf);
	  return ret;
	}
	ret = CAEN_DGTZ_DecodeEvent(iBoard, EventPtr, (void**)&info->Event16[iBoard]);
	if (ret != 0) {
	  printf("Error decoding event\n");
	  printToLogfile("Error decoding event");
	  return ret;
	}
	int ch;
	g_eventHeader->nchannels+= info->FCP_DAQcfg->Nch;
	for (ch = 0; ch < info->FCP_DAQcfg->Nch; ch++) {
	  int Size = info->Event16[iBoard]->ChSize[ch];
	  if (Size <= 0) {
	    continue;
	  }
	  
	  //  if (iEvt%100 == 0) {
	  //  printf("EventCounter is %x\n", EventInfo.EventCounter);
	  //}
	  g_eventHeader->eventCounter = (0xa0000000 | EventInfo.EventCounter);
	  g_eventHeader->triggerTimeTag[iBoard] = EventInfo.TriggerTimeTag;
	  
	  g_channelHeader->channel = ch+(iBoard << 3);
	  g_channelHeader->eventCounter = (0xa0000000 | EventInfo.EventCounter);
	  
	  //	printf("Writing channel header\n");
	  if (  info->FCP_DAQrun->fout_allch != 0) {
	    if(fwrite(g_channelHeader, sizeof(*g_channelHeader), 1, info->FCP_DAQrun->fout_allch) != 1) {
	      printf("Error writing channel header\n");
	      printToLogfile("Error writing channel header");
	      fclose(info->FCP_DAQrun->fout_allch);
	      info->FCP_DAQrun->fout_allch= NULL;
	      return -1;
	    }
	  } else {
	    printf("Error writing channel header -- file not open\n");
	    printToLogfile("Error writing channel header -- file not open");
	    fclose(info->FCP_DAQrun->fout_allch);
	    info->FCP_DAQrun->fout_allch= NULL;
	    return -1;
	  }
	  g_ADC_data[g_channelHeader->channel].clear();
	  g_ADC_data[g_channelHeader->channel].insert(g_ADC_data[g_channelHeader->channel].end(), &info->Event16[iBoard]->DataChannel[ch][0], &info->Event16[iBoard]->DataChannel[ch][info->FCP_DAQcfg->RecordLength]);
	  int ns;
	  if (info->FCP_DAQrun->fout_allch != 0) {
	    ns = (int)fwrite(info->Event16[iBoard]->DataChannel[ch] , 1 , Size*2, info->FCP_DAQrun->fout_allch) / 2;
	  } else {
	    printf("Error writing channel data -- file not open\n");
	    printToLogfile("Error writing channel data -- file not open");
	    return -1;
	  }
	  
	  if (ns != Size) {
	    // error writing to file
	    printf("Error writing channel data: ns = %d, should be %d\n", ns, Size); 
	    fclose(info->FCP_DAQrun->fout_allch);
	    info->FCP_DAQrun->fout_allch= NULL;
	    return -1;
	  }
	  
	}
      }
    
      
      // add in BPC data if operating in that mode, and BPC data was found
      if ((merge_BPC_data == 1)) {
	
	good_BPC_data = true;
	
	if (BPC_data_sample != 0) {
	  // check data validity
	  if (BPC_data_sample->tag != 0xCAFE) { //  ||
	    //    BPC_data_sample->runNumber != g_runHeader->runNumber ||
	    //  BPC_data_sample->spillNumber != g_spillHeader->spillNumber) {
	    good_BPC_data = false;	 
	  }
	  g_BPC_data_sample = *BPC_data_sample;    
	  if (good_BPC_data) {
	    good_BPC_samples++;
	    //	  printf("BPC trigger %d\n", g_BPC_data_sample.triggerNumber);
	  }
	  BPC_data_sample++;
	  
	} else {
	  // dummy values for BPC data
	  g_BPC_data_sample.tag = 0XFFFF;
	  for (int iBPC = 0; iBPC < NUM_BPCS; iBPC++) {
	    g_BPC_data_sample.xy[iBPC][0] = -999.;
	    g_BPC_data_sample.xy[iBPC][1] = -999.;
	  }
	  
	}

      } 
      
          
      TDirectory* d=  t_event->GetDirectory();
      if (d == 0) {
	printf("Error: root tree directory is null\n");
	printToLogfile("Error:root tree directory is null");
	openErrorWindow(info, "Root tree directory is null", 1);
      } 
      if (!d->IsWritable()) {
	printf("Error: root tree directory is not writeable\n");
	printToLogfile("Error:root tree directory is not writeable");
	openErrorWindow(info, "Root tree directory is not writable", 1);
      }
      t_event->Fill();
    }
     
    if (merge_BPC_data == 1) {
      while (good_BPC_data) {
	if (BPC_data_sample != 0) {
	  // check data validity
	  if (BPC_data_sample->tag != 0xCAFE ) {  // ||
	    //	    BPC_data_sample->runNumber != g_runHeader->runNumber ||
	    //  BPC_data_sample->spillNumber != g_spillHeader->spillNumber) {
	    good_BPC_data = false;	 
	  }
	  if (good_BPC_data) good_BPC_samples++;
	  
	  g_BPC_data_sample = *BPC_data_sample;    
	  BPC_data_sample++;
	}
      }
    }
    //  printf("Number of good BPC frames = %d in %d digitizer events\n", good_BPC_samples, g_spillHeader->NumEvents);

    if (merge_BPC_data && (good_BPC_samples != g_spillHeader->NumEvents)) {
      char buf[200];
      sprintf(buf, "Warning: different numbers of events in BPC (%d) and digitizers (%d)", good_BPC_samples, g_spillHeader->NumEvents);
      printToLogfile(buf);
      printf("%s\n", buf);
    }

    gtk_level_bar_set_value((GtkLevelBar*)info->BPC_nevents_bar, good_BPC_samples);
    
    char label[100];
    sprintf(label, "BPC events in last spill: %d\n", good_BPC_samples);
    gtk_label_set_text(GTK_LABEL(info->BPC_nevents_bar_label),label);   

    t_event->Write();
 
  }

  //time_t rawtime;
  //struct tm * timeinfo;

  //time ( &rawtime );
  //timeinfo = localtime ( &rawtime );
  //printf ( "Done writing data at: %s", asctime (timeinfo) );
  printToLogfile("Done writing data");

  if (merge_BPC_data) {
    //buffers from BPC should now be empty.  Check...
    char tag[20];
    int nb;
    int rc = check_head(tag, &nb);
    if (rc > 0) {
      char buf[200];
      sprintf(buf, "Warning: Extra data from BPC found at end of spill with tag %s (%d bytes)", tag, nb);
      printToLogfile(buf);
      get_data(BPC_data_buf, nb);
    }
    if (rc < 0) {
      printToLogfile("Error communicating with ControlHost when looking for extra data at end of spill");
    }
  }
  return 0;
}

int writeEndOfSpillRecord(FCP_DAQ_Info_t* info) {

  //  printf("Writing the end of spill flag\n");
  if (info->FCP_DAQrun->fout_allch == 0) {
    printf("Error: file not open when attempting to write event data\n");
    printToLogfile("Error: file not open when attempting to write event data");
    openErrorWindow(info, "File not open when attempting to write event data", 1);
    return -1;
  }

  uint32_t endOfSpillFlag = 0x80000000;  // MSB set in channel number is
                                         // flag for end of spill
  // writing a 0 to the field that would otherwise be the size of the channel 
  // should be an unambigious signal of the end of the data
  if (fwrite(&endOfSpillFlag , 1 , sizeof(endOfSpillFlag), info->FCP_DAQrun->fout_allch) != sizeof(endOfSpillFlag)) {
    printf("Error writing end of spill flag\n");
    printToLogfile("Error writing end of spill flag");
    openErrorWindow(info, "Error writing end of spill flag", 1);
   
  }

  // close root file (one file per spill)
  t_event->SetDirectory(0);
  f_root->Close();
  delete f_root;
  f_root = 0;

  char sysCmd[200];
  sprintf(sysCmd, "ln -sr %s %s", root_fname, root_simplefname);
  char buf[200];
  sprintf(buf, "Creating softlink: %s", sysCmd);
  printToLogfile(buf);
  int errCode = system(sysCmd);
  if (errCode != 0) {
    sprintf(buf, "Error creating softlink: %d", errCode);
    printToLogfile(buf);
    int errnum = errno;
    sprintf(buf, "Value of errno: %d: %s\n", errno, strerror(errnum));
  } 

  // copy to pcaz001 for online montoring                                                                                                                                   
  char root_basefname[200];
  sprintf(root_basefname, "run%dspill%d.root",  g_runHeader->runNumber, g_spillHeader->spillNumber);
  sprintf(sysCmd, "nice rsync -ap --timeout=20 %s fcalpulse@pcaz001.cern.ch:monitoring/%s &", root_fname, root_basefname);
  system(sysCmd);

  return 0;
}

/* ########################################################################### */
/* MAIN                                                                        */
/* ########################################################################### */
int main(int argc, char *argv[])
{
  FCP_DAQ_Config_t   FCP_DAQcfg;
  FCP_DAQ_Run_t      FCP_DAQrun;


  int c;
  while ((c = getopt(argc, argv, "s")) != -1) {
    switch(c) {
    case 's':
      sim_mode = 1;
    }
  }

  g_runHeader = new FCP_DAQ_RunHeader_t;
  g_spillHeader = new FCP_DAQ_SpillHeader_t;
  g_eventHeader = new FCP_DAQ_EventHeader_t;
  g_channelHeader = new FCP_DAQ_ChannelHeader_t;
  
//start up related processes
  char cmdstr[200];
  sprintf(cmdstr, "%s/scripts/start_processes.sh", RUNDIR);
  system(cmdstr);

  FCP_DAQ_Info_t info;
  info.FCP_DAQrun = &FCP_DAQrun;
  info.FCP_DAQcfg = &FCP_DAQcfg;

  openLogFile();

  //printf("size of spill header is %zu\n", sizeof(FCP_DAQ_SpillHeader_t));

  char buf[1000];
  printf("\n");
  printf("**************************************************************\n");
  sprintf(buf,  "**************************************************************\n");
  printf("                        FCP_DAQ %d.%d.%d\n", FCP_DAQ_MajorVersion,
	 FCP_DAQ_MinorVersion, FCP_DAQ_Revision);
  sprintf(buf, "%s                         FCP_DAQ %d.%d.%d\n", buf, FCP_DAQ_MajorVersion,  FCP_DAQ_MinorVersion, FCP_DAQ_Revision);
  if (sim_mode > 0) {
    printf("****  RUNNING IN SIMULATED MODE.  NOT FOR ACTUAL DATA-TAKING!!! ****\n");
    sprintf(buf, "%s****  RUNNING IN SIMULATED MODE.  NOT FOR ACTUAL DATA-TAKING!!! ****\n", buf);
  }
  printf("**************************************************************\n");
  sprintf(buf, "%s**************************************************************\n", buf);
  printToLogfile(buf);

  /* *************************************************************************************** */
  /* Setup configuration for the digitizer                                                       */
  /* *************************************************************************************** */
  memset(&FCP_DAQrun, 0, sizeof(FCP_DAQrun));
  memset(&FCP_DAQcfg, 0, sizeof(FCP_DAQcfg));

  // EWV test if I can link in Petr Gorbounev's ControlHost libraries
  int make_disp_link = init_disp_link(ch_host, ch_tag);
  //int make_disp_link = init_disp_link(NULL, ch_tag);
  //  make_disp_link = resubscribe("test");

  if (make_disp_link < 0) {
    printf("Error initializing ControlHost link\n");
    printToLogfile("Error initializing ControlHist link");
  }

  send_me_always();
  my_id("FCP_DAQ");

  // bring up GUI first, so any errors can be handled properly
  GtkApplication *app;
  int status;

  app = gtk_application_new ("FCP.DAQ", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), &info);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  if (status != 0) {
    return status;
  }


}

CAEN_DGTZ_ErrorCode initializeDigitizers(FCP_DAQ_Info_t* info) {
  /* *************************************************************************************** */
  /* Open the digitizer and read its information                                       */
  /* *************************************************************************************** */
  CAEN_DGTZ_ErrorCode ret = CAEN_DGTZ_Success, all_ret;
  int handle_ADC[NUM_ADC_BOARDS] = {NUM_ADC_BOARDS*(-1)};
  ERROR_CODES ErrCode= ERR_NONE;
  uint32_t AllocatedSize;
  int MajorNumber;
  CAEN_DGTZ_BoardInfo_t       BoardInfo[NUM_ADC_BOARDS];

  int iBoard;
  char buf[200];

  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    info->buffer_ADC[iBoard] = NULL;
    info->FCP_DAQrun->fanSpeed[iBoard] = 0;

    ret = CAEN_DGTZ_OpenDigitizer(info->FCP_DAQcfg->LinkType, iBoard, info->FCP_DAQcfg->ConetNode, 0, &handle_ADC[iBoard]);
    all_ret = ret;
    if (ret) {
      printf("Link type is %d\n", info->FCP_DAQcfg->LinkType);
      printf("Link num is %i\n", iBoard);
      printf("ConetNode is %i\n", info->FCP_DAQcfg->ConetNode);
      sprintf(buf, "Link type is %d\n", info->FCP_DAQcfg->LinkType);
      sprintf(buf, "%sLink num is %i\n", buf,iBoard);
      sprintf(buf, "%sConetNode is %i\n", buf,info->FCP_DAQcfg->ConetNode);
      printToLogfile(buf);
      ErrCode = ERR_DGZ_OPEN;
      addError(info->FCP_DAQrun, iBoard, ErrCode);     
      //    if (sim_mode ==0) {
      //	openErrorWindow(info, "Error: can't communicate with digitizer", 1);
      //}
    }
    
    printf("handle = %d\n", handle_ADC[iBoard]);
    sprintf(buf, "handle = %d\n", handle_ADC[iBoard]);
    printToLogfile(buf);
    for (int iPrevBoard = 0; iPrevBoard < iBoard; iPrevBoard++) {
      if (handle_ADC[iPrevBoard] == handle_ADC[iBoard]) {
	printf("Error: same handle for multiple digitizers\n");
	printToLogfile("Error: same handle for multiple digitizers");
	ErrCode = ERR_DGZ_OPEN;
	addError(info->FCP_DAQrun, iBoard, ErrCode);     
	all_ret = all_ret | CAEN_DGTZ_NotYetImplemented;
	//if (sim_mode == 0) {
	//  openErrorWindow(info, "Error: same handle for multiple digitizers", 1);
	//}
      }
    }
    
    ret = CAEN_DGTZ_GetInfo(handle_ADC[iBoard], &BoardInfo[iBoard]);
    all_ret = all_ret | ret;
    if (ret) {
      ErrCode = ERR_DIGITIZER_INFO_READ;
      addError(info->FCP_DAQrun, iBoard, ErrCode);     
      printToLogfile("Error: can't read digitizer information");
      //if (sim_mode == 0) {
      //	openErrorWindow(info, "Error: can't read digitizer information", 1);
      //}
    }
    
    printf("Connected to CAEN Digitizer Model %s\n", BoardInfo[iBoard].ModelName);
    printf("ROC FPGA Release is %s\n", BoardInfo[iBoard].ROC_FirmwareRel);
    printf("AMC FPGA Release is %s\n", BoardInfo[iBoard].AMC_FirmwareRel);
    printf("Serial Number is %d\n", BoardInfo[iBoard].SerialNumber);
    sprintf(buf, "ROC FPGA Release is %s\n", BoardInfo[iBoard].ROC_FirmwareRel);
    sprintf(buf, "%sAMC FPGA Release is %s\n", buf, BoardInfo[iBoard].AMC_FirmwareRel);
    sprintf(buf, "%sSerial Number is %d\n", buf,BoardInfo[iBoard].SerialNumber);
    printToLogfile(buf);
    // Check firmware revision (DPP firmwares cannot be used with FCP_DAQ */
    // EWV is this still needed?
    sscanf(BoardInfo[iBoard].AMC_FirmwareRel, "%d", &MajorNumber);
    if (MajorNumber >= 128) {
      printf("This digitizer has a DPP firmware\n");
      printToLogfile("This digitizer has a DPP firmware");
      ErrCode = ERR_INVALID_DIGITIZER_TYPE;
      addError(info->FCP_DAQrun, iBoard, ErrCode);
      all_ret = all_ret | CAEN_DGTZ_NotYetImplemented;     
      //if (sim_mode == 0) {
      //	openErrorWindow(info, "Error: digitizer has a DPP firmware", 1);
      //     }
    }
    
    // Check serial numbers to get correct digitizer assigment for mapping      
    //   AZ digitizer, SN 1756
    //   TRIUMF digitizer, SN 1502
    //   CERN digitizer, SN 14030
    // So check this and throw an error if another pattern is seen
       
    if (sim_mode == 0) {
      if (BoardInfo[iBoard].SerialNumber == DIGITIZER_1_SN) {
	DIGITIZER_1_HANDLE = iBoard;
	sprintf(digitizerName[iBoard], "1 (SN 1502)");
      } else if (BoardInfo[iBoard].SerialNumber == DIGITIZER_2_SN) {
	DIGITIZER_2_HANDLE = iBoard;
	sprintf(digitizerName[iBoard], "2 (SN 1756)");
      } else if (BoardInfo[iBoard].SerialNumber == DIGITIZER_3_SN) {
        DIGITIZER_3_HANDLE = iBoard;
	sprintf(digitizerName[iBoard], "3 (SN 14028)");
      } else if (BoardInfo[iBoard].SerialNumber == DIGITIZER_4_SN) {
	DIGITIZER_4_HANDLE = iBoard;
	sprintf(digitizerName[iBoard], "4 (SN 14018)");
      } else {
	printf("Error: unexpected digitizer serial number %d\n", BoardInfo[iBoard].SerialNumber);
	sprintf(buf, "Error: unexpected digitizer serial number %d",  BoardInfo[iBoard].SerialNumber);
	addError(info->FCP_DAQrun, iBoard, ERR_INVALID_DIGITIZER_TYPE);
	all_ret = all_ret | CAEN_DGTZ_NotYetImplemented;
	//openErrorWindow(info, buf, 1);
      }
    }
    
    info->handle_ADC[iBoard] = handle_ADC[iBoard];
    info->BoardInfo[iBoard] = BoardInfo[iBoard];
    info->FCP_DAQrun->ackBoardFailBits[iBoard] = 0;
    
  }
  
  info->FCP_DAQrun->autoPause = 0;
  info->MaxFileSize = 2000000000;  // 2GB                                                                             

    // Get Number of Channels, Number of bits, Number of Groups of the digitizer */
    //EWV hard wire this stuff in SetupConfigurationrather than calling a function
    //      ret = GetMoreBoardInfo(handle_ADC[iBoard], BoardInfo[iBoard], &FCP_DAQcfg);
    //  if (ret) {
    //  ErrCode = ERR_INVALID_BOARD_TYPE;
    //  if (sim_mode == 0) {
    //quitProgram(NULL, ErrCode);
    //  }
    //}
      
  SetupConfiguration(info->FCP_DAQcfg);

  /* *************************************************************************************** */
  /* program the digitizer                                                                   */
  /* *************************************************************************************** */

  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) { 
 
    //Check for possible digitizer internal errors                                                                         
    if (sim_mode == 0) {
      ret = (CAEN_DGTZ_ErrorCode)CheckDigitizerFailureStatus(info, info->handle_ADC[iBoard]);
      all_ret = all_ret | ret;
      if (ret) {
        ErrCode = ERR_DIGITIZER_FAILURE;
        sprintf(buf, "Error: one or more digitizer failure bits set for digitizer %s", digitizerName[iBoard]);
        printToLogfile(buf);
        addError(info->FCP_DAQrun, iBoard, ErrCode);
        //if (sim_mode == 0) {
	// openErrorWindow(info, buf, 1);
        //}
      }
    }

    ret = ProgramDigitizer(iBoard, info->handle_ADC[iBoard], *(info->FCP_DAQcfg), info->BoardInfo[iBoard]);
    all_ret = all_ret | ret;
    if (ret) {
      ErrCode = ERR_DGZ_PROGRAM;
      sprintf(buf, "Error programming digitizer %s", digitizerName[iBoard]);
      printToLogfile(buf);
      addError(info->FCP_DAQrun, iBoard, ErrCode);
      //if (sim_mode == 0) {
      // 	openErrorWindow(info, buf, 1);
      //}
    
    }
   
    //check for possible failures after programming the digitizer
    ret = CheckDigitizerFailureStatus(info, info->handle_ADC[iBoard]);
    all_ret = all_ret | ret;
    if (ret) {
      ErrCode = ERR_DIGITIZER_FAILURE;
      sprintf(buf, "Error: one or more digitizer failure bits set for digitizer %s", digitizerName[iBoard]);
      printToLogfile(buf);
      addError(info->FCP_DAQrun, iBoard, ErrCode);
      // if (sim_mode == 0) {
      // 	openErrorWindow(info, buf, 1);
      // }

    }
  }

  usleep(300000);

  // Read again the digitizer infos, just in case some of them were changed by the programming
  // (like, for example, the TSample and the number of channels if DES mode is changed)
  for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
    ret = CAEN_DGTZ_GetInfo(info->handle_ADC[iBoard], &(info->BoardInfo[iBoard]));
    all_ret = all_ret | ret;
    if (ret) {
      ErrCode = ERR_DIGITIZER_INFO_READ;
      sprintf(buf, "Error can't read information for digitizer %d", iBoard);
      printToLogfile(buf);
      addError(info->FCP_DAQrun, iBoard, ErrCode);
      // if (sim_mode == 0) {
      //	openErrorWindow(info, buf, 1);
      //}

    }

    // Allocate memory for the event data and readout buffer
    AllocatedSize = 0;
    //    printf("before allocate ret = %d %p %d\n", ret, info->buffer_ADC[iBoard], AllocatedSize);

    ret = CAEN_DGTZ_MallocReadoutBuffer(info->handle_ADC[iBoard], &(info->buffer_ADC[iBoard]),&AllocatedSize); /* WARNING: This malloc must be done after the digitizer programming */
    all_ret = all_ret | ret;
    if (ret != CAEN_DGTZ_Success) {
      printf("ret = %d %p %d\n", ret, info->buffer_ADC[iBoard], AllocatedSize);
      ErrCode = ERR_MALLOC;
      sprintf(buf, "Error can't allocate memory for data from digitizer %d with handle %d", iBoard, info->handle_ADC[iBoard]);
      printToLogfile(buf);
      addError(info->FCP_DAQrun, iBoard, ErrCode);
      //if (sim_mode == 0) {
      //	openErrorWindow(info, buf, 1);
      //}
    }

    //   printf("after allocate ret = %d %p %d\n", ret, info->buffer_ADC[iBoard], AllocatedSize);


    info->Event16[iBoard] = NULL;
  
    ret = CAEN_DGTZ_AllocateEvent(info->handle_ADC[iBoard], (void**)&info->Event16[iBoard]);
    all_ret = all_ret | ret;
    if (ret != CAEN_DGTZ_Success) {
      ErrCode = ERR_MALLOC;
      sprintf(buf, "Error can't allocate memory for event from digitizer %d", iBoard);
      printToLogfile(buf);
      addError(info->FCP_DAQrun, iBoard, ErrCode);
      //if (sim_mode == 0) {
      //  openErrorWindow(info, buf, 1);
      //}
    }
  
    ret = CAEN_DGTZ_ClearData(info->handle_ADC[iBoard]);     
    all_ret = all_ret | ret;
    if (ret != CAEN_DGTZ_Success) {
      ErrCode = ERR_CLEAR;
      sprintf(buf, "Error can't clear data from digitizer %d", iBoard);
      printToLogfile(buf);
      addError(info->FCP_DAQrun, iBoard, ErrCode);
      //if (sim_mode == 0) {
      //  openErrorWindow(info, buf, 1);
      //}
    }    

    // try reading events. Shouldn't be any...
    uint32_t dummy; 
    ret = CAEN_DGTZ_ReadData(info->handle_ADC[iBoard], CAEN_DGTZ_SLAVE_TERMINATED_READOUT_MBLT, info->buffer_ADC[iBoard], &dummy);
    all_ret = all_ret | ret;
    if (ret != CAEN_DGTZ_Success) {
      ErrCode = ERR_READOUT;
      sprintf(buf, "Error: Initial try of readout failed %d", iBoard);
      printToLogfile(buf);
      addError(info->FCP_DAQrun, iBoard, ErrCode);
      //if (sim_mode == 0) {
      //  openErrorWindow(info, buf, 1);
      //}
    }  

    // some indication that a second programming pass is needed to 
    // take good data after the digitizers are power-cycled.  Seems to be
    // related to setting to use external clock, but I really have no idea why
    ret = ProgramDigitizer(iBoard, info->handle_ADC[iBoard], *(info->FCP_DAQcfg), info->BoardInfo[iBoard]);
    all_ret = all_ret | ret;   

  }


  /* *************************************************************************************** */
  /* Readout Loop                                                                            */
  /* *************************************************************************
************** */

  sprintf(info->beam_status, "Unknown");
  info->DAQ_thread_ID = 0;
  info->beam_status_thread_ID = 0;

  if (do_analysis != 0) {
  // allocate analysis buffer
    size_t maxBufSize_ADC = NUM_ADC_CHANNELS*2*info->FCP_DAQcfg->NumEvents*(info->FCP_DAQcfg->RecordLength+1);  // +1 for channel header
    for (iBoard = 0; iBoard < NUM_ADC_BOARDS; iBoard++) {
      if ((info->analysisBuffer_ADC[iBoard] = (char*)malloc(maxBufSize_ADC)) == NULL) {
	printf("Error: cannot allocate analysis buffer for digitizer %d\n", iBoard);
	sprintf(buf, "Error: cannot allocate analysis buffer for digitizer %d", iBoard);
	printToLogfile(buf);
	sprintf(buf, "Error can't allocate analysis buffer digitizer %d", iBoard);
	printToLogfile(buf);
	addError(info->FCP_DAQrun, iBoard, ErrCode);
	all_ret = all_ret | CAEN_DGTZ_NotYetImplemented;
	//	if (sim_mode == 0) {
	//  openErrorWindow(info, buf, 1);
	//}
      }
    }
  }

  return all_ret;
}

float freeDiskSpace() {

  float freeSpace = -1; 
  struct statvfs vfsbuf;  
  if (statvfs(OUTPUT_TOPLEVEL_DIRECTORY, &vfsbuf) == -1) {
    printToLogfile("statvfs error");
  } else {
    freeSpace = ((float)vfsbuf.f_bavail * vfsbuf.f_bsize)/GIGABYTE;
  }
  return freeSpace;
}
