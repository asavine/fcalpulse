#include <stdlib.h>
#include <stdio.h>
#include <sys/statvfs.h>

#define GIGABYTE 1073741824

float freeDiskSpace() {

  float freeSpace = -1; 
  struct statvfs vfsbuf;  
  if (statvfs(".", &vfsbuf) == -1) {
    printf("statvfs error\n");
  } else {
    freeSpace = ((float)vfsbuf.f_bavail * vfsbuf.f_bsize)/GIGABYTE;
  }
  return freeSpace;
}

int main() {

  int ret = system("ls -l *") ;
  if (ret != 0) {
    printf("Command failed!");
  }
  float fs = freeDiskSpace();
  printf("%6.2f\n", fs);

  return ret;
}

