/******************************************************************************
*
* Description: 
*
-----------------------------------------------------------------------------
* This is a simple program to reset the CAEN 5730 digitizers, in case they get
* into a bad state.  Hopefully this can avoid some trips inside the restricted
* area.
*
* Contact person: Erich Varnes (varnes@physics.arizona.edu)
*  ----------------------------------------------------------------------------
*  Syntax: resetDigitizers
*****************************************************************************/
#include <CAENDigitizer.h>
#include <CAENVMElib.h>
#include "FCP_DAQ.h"
#include <iostream>

int main() {
  uint32_t status = 0;
  for (int handle = 0; handle < NUM_ADC_BOARDS; handle++) {
    CAEN_DGTZ_CloseDigitizer(handle);
  }
  for (int handle = 0; handle < NUM_ADC_BOARDS; handle++) {
    int ConetNode = 0, newhandle;
    CAEN_DGTZ_OpenDigitizer(CAEN_DGTZ_USB, handle, ConetNode, 0, &newhandle);
    std::cout << "Opened digitizer with handle " << newhandle << std::endl;
  }
  for (int handle = 0; handle < NUM_ADC_BOARDS; handle++) {
    CAEN_DGTZ_ClearData(handle);
    CAEN_DGTZ_Reset(handle);
    //set to use internal clock, then external clock.  Maybe this will fix PLL unlock?
    CAEN_DGTZ_WriteRegister(handle, 0x8100, 0x0);
    sleep(1);
    //    CAEN_DGTZ_WriteRegister(handle, 0x8100, 0x40);
    // sleep(1);
    CAEN_DGTZ_ReadRegister(handle, 0x8178, &status);
    std::cout << "Error status for digitizer handle " << handle << " " <<  std::hex << "0x" << status << std::dec << std::endl; 
    if (status == 0) {
      CAEN_DGTZ_BoardInfo_t BoardInfo;
      CAEN_DGTZ_GetInfo(handle, &BoardInfo);
      std::cout << "This is SN " << BoardInfo.SerialNumber << std::endl; 
    }
    //    CAEN_DGTZ_WriteRegister(newhandle, 0x8100, 0x40);
    //sleep(1);
  } 
 
  return 0;
}
