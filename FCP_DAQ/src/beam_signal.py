#!/usr/local/anaconda3/bin/python

import socket
import time
import errno
#import select

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 1839        # Port to listen on (non-privileged ports are > 1023)

BEAM_ON_SEC = 10
BEAM_OFF_SEC = 10

beam_on = 1 #simulated initial state...

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    conn.setblocking(0)

    LastChangeTime = time.time()
    with conn:
        print('Connected by', addr)
        while True:
            CurrentTime = time.time()
            ElapsedTime = CurrentTime - LastChangeTime
            if ((beam_on == 1) and (ElapsedTime >= BEAM_ON_SEC)) :
                beam_on = 0
                data = b'off\0'
                conn.sendall(data)
                LastChangeTime = CurrentTime
                ElapsedTime = 0
            if ((beam_on == 0) and (ElapsedTime >= BEAM_OFF_SEC)) :
                beam_on= 1
                data = b'spill 2549\0'
                conn.sendall(data)
                LastChangeTime = CurrentTime
                ElapsedTime = 0
            try:
                data=conn.recv(1024)
                if data:
                    print('DAQ: ',data.decode("utf-8"))
                # run thru possible DAQ messages
            except socket.error as e:
                if e.errno!=errno.EAGAIN:
                    # other error
                    print("Error on socket recv")
                    exit

#            if not data :
#                print('Nothing!')
          
                
